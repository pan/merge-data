#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <filesystem>

#include <TFile.h>
#include <TTree.h>
#include <TKey.h>

using namespace std;

int GetPixelID(int, int);
void FindCluster(TFile&,TFile&,int);
void WriteCluster(TFile&,string,vector< vector< vector<Long64_t> > >);

int main(int argc, char** argv) {

  if(!(argc >= 3 && argc <= 5)){
    cout << "Incorrect Input. Do:" << endl;
    cout << "./ClusterData 1 <time window in ns> <Tpx3Data.root>       | generate clusters" << endl;
    cout << "./ClusterData 2 <ClusterFile.root>                        | search for time offsets" << endl;
    cout << "./ClusterData 3 <Tpx3Data.root> <TimeCorrection.root>  | apply time correction to data set" << endl;
    return 0;
  }

  int iOpt = stoi(argv[1]);

  if(iOpt == 1) {
    int t_window = stoi(argv[2]);
    TFile in_file(argv[3],"READ");
    TFile out_file(Form("ClusterData_%s",argv[3]),"RECREATE");
    FindCluster(in_file,out_file,t_window);
    out_file.Close();
    in_file.Close();

  }
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

void FindCluster(TFile& in_file,TFile& out_file,int t_window){

  //TObject *obj;
  TKey *key;
  TIter next(in_file.GetListOfKeys());
  
  while ((key = (TKey *) next())) {
    in_file.cd();
    vector< vector< vector<Long64_t> > > ClusterList;    //hierachry: 1) clusterID, 2) number of hits, 3) hit info
    ClusterList.clear();
 
    in_file.Get(key->GetName()); // copy object to memory
    
    TTree *tree = (TTree *)in_file.Get(key->GetName());

    int row, col, tot;
    Long64_t timestamp, ftoa;
    float rotation;
    
    tree->SetBranchAddress("row",&row);
    tree->SetBranchAddress("col",&col);
    tree->SetBranchAddress("tot",&tot);
    tree->SetBranchAddress("rotation",&rotation);
    tree->SetBranchAddress("timestamp",&timestamp);
    tree->SetBranchAddress("ftoa",&ftoa);

    vector< vector<Long64_t> > Tpx3Hits(tree->GetEntries());
    for(int ii=0; ii<tree->GetEntries(); ii++){
      tree->GetEntry(ii);
      
      vector<Long64_t> iHit(6);
      iHit[0] = timestamp;
      iHit[1] = row;
      iHit[2] = col;
      iHit[3] = rotation;
      iHit[4] = tot;
      iHit[5] = ftoa;

      Tpx3Hits[ii] = iHit;
    }

    //sort based on timestamp
    sort(Tpx3Hits.begin(),Tpx3Hits.end());
  
    //creating clusters

    //1) should be within time window
    //2) pixels must be next to each other

    vector< vector<Long64_t> > iCluster, ClusterWindow;
    vector< bool > HitMask(Tpx3Hits.size(),false);
    for(size_t ii=0; ii<Tpx3Hits.size();ii++){
      iCluster.clear();
      if(HitMask[ii]) {continue;}      

      if(iCluster.size() == 0){iCluster.push_back(Tpx3Hits[ii]);}

      //store all hits in specified time window
      ClusterWindow.clear();
      for(size_t jj=ii; jj<Tpx3Hits.size();jj++){
        if((Tpx3Hits[jj][0]*25+Tpx3Hits[jj][5]*1.5625-iCluster[0][0]*25-iCluster[0][5]*1.5625)<=t_window){
          ClusterWindow.push_back(Tpx3Hits[jj]);
        } else {
          break;
        }
      }
      
      vector< bool > ClusterHitMask(ClusterWindow.size(),false);
      for(size_t iClusterHit=0;iClusterHit<iCluster.size();iClusterHit++){
        for(size_t jClusterHit=iClusterHit;jClusterHit<ClusterWindow.size();jClusterHit++){
          if(ClusterHitMask[jClusterHit]) {continue;}
          int pixelID_ii = GetPixelID(iCluster[iClusterHit][2],iCluster[iClusterHit][1]);
          int pixelID_jj = GetPixelID(ClusterWindow[jClusterHit][2],ClusterWindow[jClusterHit][1]);

          if(abs(pixelID_ii-pixelID_jj)==1 || abs(pixelID_ii-pixelID_jj)==256){
            iCluster.push_back(ClusterWindow[jClusterHit]);
            ClusterHitMask[iClusterHit] = true; //
          } 
        }
      }

      //update hit mask
      for(size_t iClusterHit=0;iClusterHit<ClusterWindow.size();iClusterHit++){
        HitMask[ii+iClusterHit] = ClusterHitMask[iClusterHit];
      }

      ClusterList.push_back(iCluster);

    }

    cout << "Finished Processing: " << key->GetName() << endl;
    //write Cluster to file
    WriteCluster(out_file,key->GetName(), ClusterList);

    delete tree;
  }

}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

int GetPixelID(int col, int row){

  return (row * 256 + col);

}

void WriteCluster(TFile& out_file,string quad_name,vector< vector< vector<Long64_t> > > ClusterList){

  out_file.cd();

  Long64_t ClusterID, ClusterSize;
  int row, col, tot;
  Long64_t timestamp, ftoa;
  float rotation;

  TTree *tree = new TTree(quad_name.c_str(),"Quad Cluster Data");
  tree->Branch("ClusterID",&ClusterID,"ClusterID/L");
  tree->Branch("ClusterSize",&ClusterSize,"ClusterSize/L");
  tree->Branch("timestamp",&timestamp,"timestamp/L");
  tree->Branch("col",&col,"col/I");
  tree->Branch("row",&row,"row/I");
  tree->Branch("rotation",&rotation,"rotation/F");
  tree->Branch("ftoa",&ftoa,"ftoa/L");
  tree->Branch("tot",&tot,"tot/I");

  for(size_t ii=0;ii<ClusterList.size();ii++){
    ClusterID = ii;
    ClusterSize = ClusterList[ii].size();

    for(size_t iHit=0;iHit<ClusterList[ii].size();iHit++){

      timestamp = ClusterList[ii][iHit][0];
      row = ClusterList[ii][iHit][1];
      col = ClusterList[ii][iHit][2];
      rotation = ClusterList[ii][iHit][3];
      tot = ClusterList[ii][iHit][4];
      ftoa = ClusterList[ii][iHit][5];

      tree->Fill();
    }

  }

  tree->Write();
  delete tree;
}