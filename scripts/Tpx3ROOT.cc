#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <filesystem>

#include <TFile.h>
#include <TTree.h>

using namespace std;

void ReadTpx3_txt(TFile&,string);
void GetRowCol(int, int&, int&);
vector<Long64_t> GetNumbersInLine(string);
double GetQuadRot(string);

string DataPath;
int main(int argc, char** argv) {

  if(argc != 4){
    cout << "Incorrect Input. Do:" << endl;
    cout << "./Tpx3ROOT <directory> <common file prefix> <common file suffix>" << endl;
    return 0;
  }

  DataPath = argv[1];
  string Prefix   = argv[2];
  string Suffix   = argv[3];

  vector<string> quad_FileNames;
  for (const auto & entry : std::filesystem::directory_iterator(DataPath)){
    string filePath = entry.path();
    string fileName = entry.path().filename();
    if(fileName.find(Prefix) == string::npos || fileName.find(Suffix) == string::npos) continue;

    quad_FileNames.push_back(fileName);
  }

  if(quad_FileNames.size() < 4){
    cout << "WARNING. Only <" << quad_FileNames.size() << "> data files found." << endl;
  }
  if(quad_FileNames.size() > 4){
    cout << "ERROR. Too many data files found. Your criteria found <" << quad_FileNames.size() << "> data files. Cannot exceed 4." << endl;
  }

  TFile out_file(Form("%s_%s.root",Prefix.c_str(),Suffix.c_str()),"RECREATE");

  for(size_t iFile=0;iFile<quad_FileNames.size();iFile++){
    ReadTpx3_txt(out_file,quad_FileNames[iFile]);
  }

  out_file.Close();
  
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

void ReadTpx3_txt(TFile& out_file, string quad_filename){

  out_file.cd();

  int row, col, tot;
  Long64_t timestamp, ftoa;
  float rotation;

  //---------------------------
  string quad_treename = quad_filename;
  quad_treename.erase(quad_treename.size()-4);

  TTree *tree = new TTree(quad_treename.c_str(),"Quad File Data");
  tree->Branch("rotation",&rotation,"rotation/F");
  tree->Branch("col",&col,"col/I");
  tree->Branch("row",&row,"row/I");
  tree->Branch("timestamp",&timestamp,"timestamp/L");
  tree->Branch("ftoa",&ftoa,"ftoa/L");
  tree->Branch("tot",&tot,"tot/I");
  //---------------------------

  std::ifstream myfile;
  myfile.clear();
  myfile.open(DataPath+"/"+quad_filename);

  if(!myfile.is_open()) {
      cout << "Cannot open file!!" << endl;
      cout << "Searching for " << DataPath+"/"+quad_filename << endl;
      return;
  } else {
    
    rotation = GetQuadRot(quad_filename);

    string line;
    while (getline (myfile,line)){
      if (line.find("#")  != string::npos) {continue;} //ignoring all commented lines
      
      vector<Long64_t> dataHit = GetNumbersInLine(line);

      GetRowCol(dataHit[0],col,row);
      timestamp = dataHit[1];
      ftoa      = dataHit[2];
      tot       = dataHit[3];

      tree->Fill();
      //exit(EXIT_FAILURE);
    }
  }

  tree->Write();
  cout << "Finished reading " << quad_filename << endl;
  myfile.close();

}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

void GetRowCol(int PixelID, int& col, int& row){

  row = PixelID / 256 ;
  col = PixelID - row * 256 ;

}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

vector<Long64_t> GetNumbersInLine(string line){
  stringstream ss; 
  ss << line;

  string temp;
  Long64_t found;
  vector<Long64_t> dataHit;
  while (!ss.eof()) {
 
    //extracting word by word from stream
    ss >> temp;
 
    //Checking the given word is Long64_t or not
    if (stringstream(temp) >> found) {
      dataHit.push_back(found);
    }
     
    //To save from space at the end of string
    temp = "";
  }

  return dataHit;
}

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

double GetQuadRot(string quadFileName){

  //units are in degrees
  double rotation = 0;
  if (quadFileName.find("E10")  != string::npos) {rotation = 270.;}
  else if (quadFileName.find("E11")  != string::npos) {rotation = 270.;}
  else if (quadFileName.find("I10")  != string::npos) {rotation = 90.;}
  else if (quadFileName.find("I11")  != string::npos) {rotation = 90.;}

  return rotation;

}