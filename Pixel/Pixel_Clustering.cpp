// TPX3_clustering_windows_v2.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>
#include <chrono>
#include <sstream>
#include <string>
#include <vector>

#include "boost/program_options.hpp" 
#include "TError.h"

#include "AnalysisAction.h"
#include "AditionalFunctions.h"
#include "InputDataFormats.h"

using namespace std;
namespace po = boost::program_options;

int main(int argc, char* argv[])
{
	gErrorIgnoreLevel = kFatal;
	string f_in;
	string f_out;
	double time_window;
	//bool geometric_clustering;

	//Set defaults!
	time_window = 200;
	string time_offset = "0,0,0,0,0,0,0";
	vector<string> toa_bug_correction;
	string toa_bug_correction_files = "";
	string calib_dirs = "";
	string time_walk = "";
	string infile_format_string = "ASCII_KATHRINE";
	input_data_formats infile_format; //= ASCII_T3PA;
	string mask_file = "";
	string matrix_clock = "";
	string clock_rescale = "";
	string pixel_pitch = "";
	string multilayer_config = "";
	string suffix = "";
	string trigger_file = "";
	double trigger_window = 15;

	//for more than one detector -> use initialization file
	po::options_description desc("Allowed options");
	desc.add_options()
		("help,h",		"Produce help message")
		("inputfile,i",		po::value<string>(&f_in),					"Input a comma separated list of files from coincident detectors")
		("outputfile,o",	po::value<string>(&f_out),					"Name of output file with clusters")
		("time_window,t",	po::value<double>(&time_window),			"Length of time slice in ns [default: 200]")
		("time_offset",		po::value<string>(&time_offset),			"Input a commma separated list of time offsets in ns for each detector - needed")
		("calib",			po::value<string>(&calib_dirs),				"Input a commma separated list of calibration directories")
		("toa_bug_corr",	po::value<string>(&toa_bug_correction_files), "Input file for toa bug correction")
		("timewalk",		po::value<string>(&time_walk),				"Input files for the timewalk correction")
		("file_format",		po::value<string>(&infile_format_string),	"Input file format: [ASCII_T3PA (default), ROOT_FILE, ...]")
		("mask",			po::value<string>(&mask_file),				"input a list of pixels to be ignored")
		("matrix_clock",	po::value<string>(&matrix_clock),			"set the matrix clock, if PLL was bypassed")
		("clock_rescale",	po::value<string>(&clock_rescale),			"clock rescale factor: multiplicates the raw ToT with the factor, i.e. for using 5MHz with 10MHz calibration set 2.")
		("pixel_pitch",		po::value<string>(&pixel_pitch),			"pixel pitch in um, default: 55.")
		("trigger_file",    po::value<string>(&trigger_file),           "Only write data based on Trigger file. Time window with respect to trigger is -5us/+15us by default. Include 'trigger window' flag to change this")
		("trigger_window",  po::value<double>(&trigger_window),         "Width of trigger window to write subset of data. A trigger window of 15us = 'from t0-5us to t0+15us with t0 being the trigger time'. This option will not work without 'trigger file'. Units are in 'us'")
		("multilayer",		po::value<string>(&multilayer_config),		"Use a configuration file for a multilayer setup")	
		("suffix",			po::value<string>(&suffix),					"Add a suffix (e.g. \"_1\") if using the configuration file for a multilayer setup")	
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help") || argc == 1)
	{
		std::cout << desc << std::endl << std::endl;
		return 1;
	}
	chrono::high_resolution_clock::time_point prog_start = std::chrono::high_resolution_clock::now();

	vector<string> files;
	SplitCommaSeparatedInput(f_in, files);

	vector<double> time_offsets;
	SplitCommaSeparatedInput(time_offset, time_offsets);
	for (size_t i = 0; i < files.size(); i++) {
		cout.precision(10); cout << files.at(i).c_str() << " " << time_offsets.at(i) << endl;
	}

	if (infile_format_string.compare("ASCII_T3PA") == 0) 
		infile_format = ASCII_T3PA;
	else if (infile_format_string.compare("ROOT_FILE") == 0)
		infile_format = ROOT_FILE;
	else if (infile_format_string.compare("ASCII_KATHRINE") == 0)
		infile_format = ASCII_KATHRINE;
	else { cerr << "UKNOWN INPUT DATA FORMAT" << endl; exit(-1); }

	AnalysisAction* data_analyser = new AnalysisAction();
	data_analyser->SetInputFormat(infile_format);

	if (multilayer_config != "") {
		data_analyser->Initialize(f_in, trigger_file, trigger_window, f_out, multilayer_config, time_window, suffix);
	}
	else { // READ INPUT DATA FROM COMMAND LINE PARAMETERS
		data_analyser->Initialize(files, trigger_file, trigger_window, f_out, time_window, time_offsets);

		//MATRIX CLOCKS
		if (matrix_clock != "") {
			vector<int> vec_matrix_clocks;
			SplitCommaSeparatedInput(matrix_clock, vec_matrix_clocks);
			data_analyser->SetMatrixClocks(vec_matrix_clocks);
		}

		//MATRIX CLOCK RESCALE FACTORS
		if (clock_rescale != "") {
			vector<double> vec_rescale_factors;
			SplitCommaSeparatedInput(clock_rescale, vec_rescale_factors);
			data_analyser->SetMatrixClockRescale(vec_rescale_factors);
		}

		//PIXEL PITCH
		cerr << pixel_pitch << endl;
		if (pixel_pitch != "") {
			cerr << "pixel pitch" << endl;
			vector<int> vec_pixel_pitch;
			SplitCommaSeparatedInput(pixel_pitch, vec_pixel_pitch);
			for (size_t i = 0; i < vec_pixel_pitch.size(); ++i) {
				cerr << vec_pixel_pitch.at(i) << endl;
			}
			data_analyser->SetPixelPitch(vec_pixel_pitch);
		}

		//ENERGY CALIBRATIONS
		if (calib_dirs.compare("") != 0) {
			vector<string> dir;
			SplitCommaSeparatedInput(calib_dirs, dir);
			data_analyser->SetEnergyCalibration(dir);
		}

		//CORRECT FOR 25ns SHIFT in some columns
		if (toa_bug_correction_files.compare("") != 0) {
			vector<string> toa_bug_correction;
			SplitCommaSeparatedInput(toa_bug_correction_files, toa_bug_correction);
			data_analyser->SetToABugCorrectionMap(toa_bug_correction);
		}

		//SET PARAMETERS FOR THE TIMEWALK CORRECTION
		if (time_walk.compare("") != 0) {
			vector<string> time_walk_param_files;
			SplitCommaSeparatedInput(time_walk, time_walk_param_files);
			data_analyser->SetTimewalkCorrectionParams(time_walk_param_files);
		}

		//SET MASK
		if (mask_file.compare("") != 0) {
			vector<string> masks;
			SplitCommaSeparatedInput(mask_file, masks);
			data_analyser->LoadPixelMasks(masks);
		}
	}

	//HERE STARTS THE REAL ACTION: Now action!
	data_analyser->Run();

	cerr << "run finished" << endl;
	data_analyser->Finalize();
	cerr << "finialized" << endl;

	delete data_analyser;

	cout << endl;
	chrono::high_resolution_clock::time_point prog_end = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::seconds>(prog_end - prog_start).count();
	cout << "Elapsed time: " << duration << " sec. " << endl;

	return 0;
}
