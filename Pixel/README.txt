========================================================================
    CONSOLE APPLICATION : TPX3_clustering_windows_v2 Project Overview
========================================================================

AppWizard has created this TPX3_clustering_windows_v2 application for you.

This file contains a summary of what you will find in each of the files that
make up your TPX3_clustering_windows_v2 application.


Author: 
    Benedikt Bergmann
    Johannes Hulsman (only very minor modifications for Mini.PAN modifications)


Pixel_Clustering.cpp
    This is the main application source file.

