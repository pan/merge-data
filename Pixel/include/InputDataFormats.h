#ifndef INPUT_DATA_FORMATS_H
#define INPUT_DATA_FORMATS_H

enum input_data_formats {
	ASCII_T3PA ,
	ROOT_FILE ,
	ASCII_KATHRINE ,
	OTHER
};

#endif