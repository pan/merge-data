#ifndef CoincidenceGroup_H
#define CoincidenceGroup_H

#include "Pixel_data.h"
#include "PrecompilerDefinitions.h"

using namespace std;

class CoincidenceGroup{
	public: 
		CoincidenceGroup();
		~CoincidenceGroup();
		void Fill( pixel_data pix );
		inline int GetSize(){ return coincGroupSize; }
		pixel_data GetEntry(int i);
		inline void Reset(){ coincGroupSize = 0;}

	private:
		pixel_data* pixel;
		int coincGroupSize;
};

#endif