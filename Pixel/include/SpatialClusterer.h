#ifndef SpatialClusterer_H
#define SpatialClusterer_H

#include "Pixel_data.h"
#include "PrecompilerDefinitions.h"

#include<deque>
#include<vector>

using namespace std;

class Cluster{
	public:
		Cluster();
		~Cluster(){}
		Cluster( const Cluster &c ) = default; 
			
		void AddPixel( const pixel_data &pix  )	;					
		void SetLayer( const int& l )								{ m_layer = l; }
		void ClearCluster();
		
		const double& GetMinToA() const								{ return m_min_toa; }
		pixel_data& GetPixel( const int &i )						{ return m_vec_pixels[i]; }
		const vector<pixel_data>& GetCluster() const				{ return m_vec_pixels; }
		size_t GetSize() 		const								{ return m_vec_pixels.size(); }
		const int& GetLayer() 	const									{ return m_layer; }	
		
		const float& GetVolume_keV();
		void GetVolume_keV(float& v);
		const float& GetHeight_keV();
		
		const unsigned long long& GetVolume_ToT();
		void GetVolume_ToT(unsigned long long& v);
		const int& GetHeight_ToT();
		
		const double& GetMaxToA();
		
		void GetSumEnergyWeightedCoordinates(float& x, float& y, float& x_mm, float& y_mm);
		void GetSumToTWeightedCoordinates(float& x, float& y, float& x_mm, float& y_mm);
		void GetBinarySumCoordinates(float& x, float& y, float& x_mm, float& y_mm);
		void GetBinarySquareSumCoordinates(float& x, float& y, float& x_mm, float& y_mm);

			
		static bool SortByMinToA(const Cluster &c1, const Cluster &c2 )		{ return c1.GetMinToA() < c2.GetMinToA(); }
		
	private:
		vector<pixel_data> m_vec_pixels;
		int m_layer;
		double m_min_toa;
		double _max_toa;
		unsigned long long _cluster_volume;
		float _cluster_volume_keV;
		int _cluster_height;
		float _cluster_height_keV;
		float _cluster_mean_x;
		float _cluster_mean_y;
		float _cluster_mean_x_mm;
		float _cluster_mean_y_mm;
		float _cluster_rms_x;
		float _cluster_rms_y;
		float _cluster_volume_centroid_x;
		float _cluster_volume_centroid_y;
};


class SpatialClusterer{
	public:
		SpatialClusterer();
		SpatialClusterer( const SpatialClusterer &c );
		~SpatialClusterer();

		void AddPixel( const pixel_data &pix )						{ m_pixel_list.push_back(pix); }
		int FindClusters();
		bool IsNeighbor( pixel_data &pix1, pixel_data &pix2 );
		
		int GetCoincidenceGroupSize() 		const				{ return m_total_size; }
		
		vector<Cluster>& GetClusterList()							{ return m_cluster_list; }

	private:
		size_t m_idx;
		size_t m_total_size;
		
		Cluster _cluster_temp;

		deque<pixel_data> m_pixel_list;
		vector<Cluster> m_cluster_list;
};

#endif