#pragma once

#include <vector>
#include <sstream>
#include <iostream>
#include <array>

#include "FileReader.h"
#include "Pixel_data.h"
#include "PrecompilerDefinitions.h"

using namespace std;

class KathrineFileReader : public FileReader {
public:
	KathrineFileReader(string &fn);
	~KathrineFileReader();
	KathrineFileReader(const KathrineFileReader& a) = default;

	void Initialize();
	Long64_t GetEntries() 								const		{ return m_nentries; }
	bool LoadNextEntry();

	bool GetLastLine( string& last_line, std::ifstream& in ) ;
	const pixel_data& GetPixelData() 					const		{ return m_data_vector[m_active_vector].at(m_current_position[m_active_vector]-1); }
	const double& GetToA() 								const		{ return m_data_vector[m_active_vector].at(m_current_position[m_active_vector]-1).toa; }
	double GetStartTime()								const 		{ return m_start_time; }


private:
	void TimeOverlapRangeCheck( vector<pixel_data> &v_act, vector<pixel_data> &v_pas ); 
	bool ReadChunkOfData(vector<pixel_data> &vec);
	void AnalyseHeaderLine(string &s);

	short m_active_stream, m_passive_stream, m_active_vector;
	unsigned long m_current_position[2];
	bool m_last_line_read;
	
	string m_filename;
	Long64_t m_nentries;
	Long64_t m_nlines;

	double m_overlap_time, m_start_time_compensation_ns;
	bool m_eof;
	bool m_finish;

	pixel_data m_input_data;
	array<vector<pixel_data>,2> m_data_vector;
	
	std::ifstream m_infile;

	//define the variables for the info tree:
	float m_readout_temp, m_sensor_temp;
	double m_start_time, m_bias, m_relative_start_time, m_relative_end_time;
	int m_threshold, m_lost_hits;
	string m_chipboard_id, m_readout_ip, m_interface, m_start_time_string, m_readout_mode, m_det_mode;
	short m_dacs[18];
};

