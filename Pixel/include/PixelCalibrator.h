#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include <array>
#include <functional>

//#include "TMath.h"
#include "Pixel_data.h"

using namespace std;

struct calib {
    calib() = default;
    calib(float _a, float _b, float _c, float _t) : a(_a), b(_b), c(_c), t(_t) {};
    calib(const calib& c) = default;
  
    float a;
    float b;
    float c;
    float t;
};

class PixelCalibrator {
public:
    PixelCalibrator() : _coeff(){
        _coeff.resize(MATRIX_HEIGHT * MATRIX_WIDTH);
        std::fill(_tw_parameters.begin(), _tw_parameters.end(), 0.f);
        _timewalk_correction = false;
        _bins = 100;
        _max = 100;
        _time_walk_look_up.resize(_bins);
        _bin_width = _max / _bins;
    }
    ~PixelCalibrator() = default;

    void load_a(string f) { load_file(f, [this](float val, int i)   {_coeff[i].a = val; }); }
    void load_b(string f) { load_file(f, [this](float val, int i)   {_coeff[i].b = val; }); }
    void load_c(string f) { load_file(f, [this](float val, int i)   {_coeff[i].c = val; }); }
    void load_t(string f) { load_file(f, [this](float val, int i)   {_coeff[i].t = val; }); }

    float& get_tw_parameter(const int& i)                           { return _tw_parameters[i]; }
    array<float,4>& get_tw_parameters()                             { return _tw_parameters; }
    vector<calib>& get_coeffs()                                     { return _coeff; }

    void load_tw_coeffs(const float& p1, const float& p2, const float& p3, const float& p4) {
        _tw_parameters[0] = p1;
        _tw_parameters[1] = p2;
        _tw_parameters[2] = p3;
        _tw_parameters[3] = p4;
        CreateTimeWalkLoopUp();
        _timewalk_correction = true;
    }

    void calibrate(vector<pixel_data>& vec);

private:
    using setter = std::function<void(float, int)>;
    void load_file(const string& f, setter set);
    float energy_cali(const calib& c, const int& tot);
    
    double timewalk_correction(const float& e_keV);
    void timewalk_correction(double& t, const float& e_keV);
    void CreateTimeWalkLoopUp();

    vector<calib> _coeff;
    std::array<float,4> _tw_parameters;
    bool _timewalk_correction;
    vector<double> _time_walk_look_up;
    int _bins;
    float _bin_width;
    float _max;
};