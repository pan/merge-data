#ifndef AsciiFileReader_H
#define AsciiFileReader_H

#include <vector>
#include <sstream>
#include <iostream>
#include <array>
#include <complex>

#include <boost/config/warning_disable.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/stream.hpp> 
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

#include "FileReader.h"
#include "Pixel_data.h"
#include "PrecompilerDefinitions.h"

const long long ROLLOVER_ADD = 1073741824;

class AsciiFileReader : public FileReader {
public:
	AsciiFileReader(std::string &fn);
	~AsciiFileReader();

	void Initialize();
	Long64_t GetEntries()					const		{ return m_nentries; }
	bool LoadNextEntry();

	const pixel_data& GetPixelData() 		const		{ return m_data_vector[m_active_vector].at(m_current_position[m_active_vector]-1); }
	const double& GetToA() 					const		{ return m_data_vector[m_active_vector].at(m_current_position[m_active_vector]-1).toa; }
	double GetStartTime() 					const		{ return m_start_time; }
	
	void ReadInfoFile( std::string fn );

private:
//protected:
	AsciiFileReader(const AsciiFileReader& a);
	
	//void ReadInputDataFile();
	bool ReadChunkOfData(std::vector<pixel_data> &vec);
	void TimeOverlapRangeCheck( std::vector<pixel_data> &v_act, std::vector<pixel_data> &v_pas );
	void ProcessPixel();
	//void CalibratePixel();

	short m_active_stream, m_passive_stream, m_active_vector;
	unsigned long m_current_position[2];
	
	std::string m_filename;
	Long64_t m_nentries;
	Long64_t m_nlines;

	double m_overlap_time;
	bool m_eof;
	bool m_finish;

	int m_rollover_counter;
	int m_rollover_delay;
	int m_pixels_since_rollover;

	pixel_data m_input_data;
	std::array<std::vector<pixel_data>,2> m_data_vector;
	
	//std::ifstream m_infile;
	boost::iostreams::mapped_file m_infile;
	const char* m_start_of_file;
	const char* m_end_of_file;
	const char* m_temp_pos;

	//define the variables for the info tree:
	double m_start_time, m_acq_time, m_bias, m_threshold, m_serie_start_time;
	std::string m_chipboard_id, m_interface, m_pixet_version, m_start_time_string;
	short m_dacs[17];
};

#endif