#pragma once

#include "PrecompilerDefinitions.h"

struct pixel_data{
	short pixel_x, pixel_y;
	float pixel_x_mm, pixel_y_mm;
	long long sToA;
	int tot, fToA, matrix_idx;
	float tot_keV;
	int overflow;
	double toa;
	long long idx;
	double absolute_time_s;

	pixel_data() : pixel_x(-1), pixel_y(-1), pixel_x_mm(-1), pixel_y_mm(-1), sToA(-1), tot(-1), fToA(-1), matrix_idx(-1), tot_keV(-1), overflow(-1), toa(-1), idx(-1), absolute_time_s(-1) { };

	pixel_data(short x, short y, int e, double t) : pixel_x(x) , pixel_y(y) , tot(e), toa(t) {
		pixel_x_mm = (pixel_x * float(PIXEL_PITCH_ASIC))/1000.;
		pixel_y_mm = (pixel_y * float(PIXEL_PITCH_ASIC))/1000.;
		fToA = -1;
		sToA = -1;
		matrix_idx = x+ MATRIX_WIDTH * y ;
		tot_keV = -1;
		idx = -1;
		absolute_time_s = -1;
		overflow = -1;
	}

	pixel_data(const pixel_data &pix){ 
		pixel_x = pix.pixel_x;
		pixel_y = pix.pixel_y;
		pixel_x_mm = pix.pixel_x_mm;
		pixel_y_mm = pix.pixel_y_mm;
		toa = pix.toa;
		sToA = pix.sToA;
		tot = pix.tot;
		fToA = pix.fToA;
		matrix_idx = pix.matrix_idx;
		tot_keV = pix.tot_keV;
		idx = pix.idx;
		absolute_time_s = pix.absolute_time_s;
		overflow = pix.overflow;
	}

	static bool compare_by_toa(const pixel_data &a, const pixel_data &b){
		return a.toa < b.toa;
	}
};

