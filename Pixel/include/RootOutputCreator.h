#pragma once

#include<iostream>
#include<sstream>
#include<vector>
#include<algorithm>
#include<array>
#include <functional>

#include "TFile.h"
#include "TTree.h"

#include "DetectorConfigurator.h"
#include "Pixel_data.h"
#include "PrecompilerDefinitions.h"
#include "SpatialClusterer.h"

using namespace std;

class RootOutputCreator{
	public:
		RootOutputCreator(const string &fn, const short &ndet, const bool &ecali );
		~RootOutputCreator();
		
		void SaveAnalysisDescription( const double &tw, vector<double>& to );
		void SaveAnalysisDescription( const double &tw, vector<sensor_description>& to );

		void AddPixel( pixel_data &pix );
		void AddCluster(Cluster& pix, vector<pair<double,int>>);
		//void AddClusterList(vector<Cluster>& pix);

		float GetClstrVolume_keV() 				const			{ return m_clstrVolume_keV; }
		ULong64_t GetClstrVolume_ToT() 			const			{ return m_clstrVolume_ToT; }
		int GetClstrSize() 						const			{ return m_clstrSize; }
		double GetMinToA() 						const			{ return m_min_ToA; }
		void SetCoincGroupSize( const short &s )				{ m_coincidence_group_size = s; }
		void SetCoincGroupSize( const size_t& s)				{ m_coincidence_group_size = (short) s; }
		void AddCoincidentNumber( const Long64_t& c )			{ m_coincNo = c; }		

		void FillTree( short l );
		void FillTree();
		void WriteToDisk();

		void NextCoincidenceGroupNotification()					{ m_time_slice_counter++; }
		ULong64_t GetTimeSlice() 				const			{ return m_time_slice_counter; }

		void SaveAdditionalResults(TTree * t);
		void SaveAdditionalResults(TTree* t, string name);
		
		void SetStartTime( const double &st, const short &l  ){ m_start_time[l] = st; }
		void SetTriggerWindow( const double trg_w){ m_triggerWindow = trg_w;}

		void SetEnergyCali(const bool &b){ 
			_use_ecali = b; 		
			m_tout->Branch("ToT_keV", m_tot_keV.data(), "ToT_keV[clstrSize]/F");
			m_tout->Branch("clstrVolume_keV", &m_clstrVolume_keV, "clstrVolume_keV/F");
			m_tout->Branch("clstrHeight_keV", &m_clstrHeight_keV, "clstrHeight_keV/F");
		}

	private:
		RootOutputCreator( const RootOutputCreator& ro);

		void CreateClusterBranches();
		void CreateDSCBranches();

		bool _use_ecali;
		std::string m_filename;
		TFile* m_fout;
		TTree* m_tout;
		TTree* m_tdsc;

		short m_number_of_detectors;
		double m_time_window;
		vector<double> m_time_offset;
		vector<double> m_start_time;

		ULong64_t m_time_slice_counter;
		ULong64_t m_clstrVolume_ToT;
		Long64_t m_coincNo;
		
		int m_clstrSize;

		double m_triggerWindow;
		bool m_isTriggered;
		int m_triggerID;
		double m_trigger_time_s;

		size_t trigger_list_counter;

		array<short, ARRAY_LENGTH>	m_pix_x;
		array<short, ARRAY_LENGTH>	m_pix_y;
		array<float, ARRAY_LENGTH>	m_pix_x_mm;
		array<float, ARRAY_LENGTH>	m_pix_y_mm;
		array<double, ARRAY_LENGTH> m_toa;
		array<int, ARRAY_LENGTH>	m_tot;
		array<float, ARRAY_LENGTH>	m_tot_keV;

		double m_absolute_time_s;
		float m_clstrVolume_keV, m_clstrHeight_keV;
		int m_clstrHeight_ToT;
		double m_delta_ToA, m_min_ToA, m_max_ToA;
		short m_layer, m_coincidence_group_size;
		float m_clstrMeanX, m_clstrMeanY, m_clstrStdX, m_clstrStdY, m_clstrVolCentroidX, m_clstrVolCentroidY;
		float m_clstrMeanX_mm, m_clstrMeanY_mm, m_clstrStdX_mm, m_clstrStdY_mm, m_clstrVolCentroidX_mm, m_clstrVolCentroidY_mm;
		string _sw_version;
};