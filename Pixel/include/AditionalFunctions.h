#pragma once

#include<vector>
#include<string>
#include<sstream>

using namespace std;

void SplitCommaSeparatedInput(string& s, vector<string>& v);

void SplitCommaSeparatedInput(string& s, vector<double>& v);

void SplitCommaSeparatedInput(string& s, vector<int>& v);