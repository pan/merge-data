#pragma once

#include <string>
#include <vector>

using namespace std;

struct sensor_description {

	sensor_description() :id(""), layer(0), calib_dir(""), tw_file(""), time_offset(0), toa_column_corr_file(""), mask_file("") {};
	sensor_description(const string& _id) : id(_id), layer(0), calib_dir(""), tw_file(""), time_offset(0), toa_column_corr_file(""), mask_file("") {};
	sensor_description(const string& _id, const short& _l) : id(_id), layer(_l), calib_dir(""), tw_file(""), time_offset(0), toa_column_corr_file(""), mask_file("") {};
	sensor_description(const sensor_description& a) = default;

	string id; 
	int layer;
	string calib_dir;
	string tw_file;
	double time_offset;
	pair<double,double> pixel_pitch_gap_xy;
	vector<pair<int, float>> rotation; // _first: axis indicator (1-x, 2-y, 3-z); _second: rotation; 
	pair<int, int> translation;
	string toa_column_corr_file;
	string mask_file;
};

class DetectorConfigurator {
public:
	DetectorConfigurator() {};
	~DetectorConfigurator() {};
	DetectorConfigurator(const DetectorConfigurator& a) = default;

	void AddSensor(const string& s_id) { _vec_sensor_data.push_back(sensor_description(s_id)); }
	void AddSensor(const string& s_id, const int& l){ _vec_sensor_data.push_back(sensor_description(s_id,l)); }

	void ReadConfigurationFromFile(const string& fn);
	
	void SetLayer(const string& s_id, const int& layer);
	void AddRotation(const string& s_id, const int& r_axis, const float& r_angle);
	void SetPixelPitchGap(const double& pp_gapX, const double& pp_gapY);
	void SetTranslation(const string& s_id, const int& x_shift, const int& y_shift);
	void SetCalibrationDir(const string& s_id, const string& c_dir);
	void SetTimewalkFile(const string& s_id, const string& tw_file);
	void SetMaskFile(const string& s_id, const string& mask_file);
	void SetColumnCorrectionFile(const string& s_id, const string& mask_file);
	void SetTimeOffset(const string& s_id, const double& t_offset);

	vector<sensor_description>& GetDetectorConfiguration(){ return _vec_sensor_data; }
	int GetNumberOfLayers();
	size_t GetNumberOfSensors() const { return _vec_sensor_data.size(); }

	void PrintConfiguration();

private:
	vector<sensor_description> _vec_sensor_data;
};
