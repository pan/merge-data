#pragma once

#include <vector>
#include <sstream>
#include <iostream>
#include <array>
//#include <boost/filesystem.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

#include "FileReader.h"
#include "Pixel_data.h"
#include "PrecompilerDefinitions.h"

using namespace std;

class KathrineFileReaderFast : public FileReader {
public:
	KathrineFileReaderFast(string& fn);
	~KathrineFileReaderFast();
	KathrineFileReaderFast(const KathrineFileReaderFast& a) = default;

	void Initialize();
	Long64_t GetEntries() 								const { return m_nentries; }
	bool LoadNextEntry();

	const pixel_data& GetPixelData() 					const { return m_data_vector[m_active_vector].at(m_current_position[m_active_vector] - 1); }
	const double& GetToA() 								const { return m_data_vector[m_active_vector].at(m_current_position[m_active_vector] - 1).toa; }
	double GetStartTime()								const { return m_start_time; }


private:
	bool GetLastLine(string& last_line, std::ifstream& in);
	void TimeOverlapRangeCheck(vector<pixel_data>& v_act, vector<pixel_data>& v_pas);
	bool ReadChunkOfData(vector<pixel_data>& vec);
	void AnalyseHeaderLine(const string& s);
	bool MemoryMapNextChunk();

	void FillLineBuffer(size_t& i_start, const size_t& i_end) {
		while (i_start < i_end) {
			if (i_start >= i_end || _data[i_start] == '\n') {
				break;
			}
			_line_buffer[_pos_buffer] = _data[i_start];
			_pos_buffer++;
			i_start++;
		}
	}

	void FillChar(char* c, const size_t& i_start, const size_t& i_end) {
		size_t char_pos = 0;
		for (size_t i = i_start; i < i_end; ++i) {
			c[char_pos] = _data[i];
			char_pos++;
		}
	}

	template<class T>
	void GetNumber(char* str, size_t& i_start, T& res, const size_t& line_end){
		while (str[i_start] == ' ' || str[i_start] == '\t')
			i_start++;

		res = 0;
		while (1) {
			if (str[i_start] == ' ' || str[i_start] == '\t' || str[i_start] == '\n' || str[i_start] == '\r')
				return;

			if (i_start == line_end)
				return;

			res = res * 10 + str[i_start] - '0';
			i_start++;
		}
	}

	short m_active_stream, m_passive_stream, m_active_vector;
	unsigned long m_current_position[2];
	bool m_last_line_read;

	string m_filename;
	Long64_t m_nentries;
	Long64_t m_nlines;

	double m_overlap_time, m_start_time_compensation_ns;
	bool m_eof;
	bool m_finish;

	pixel_data m_input_data;
	array<vector<pixel_data>, 2> m_data_vector;

	std::ifstream m_infile;

	boost::iostreams::mapped_file_params params;
	boost::iostreams::mapped_file_source mf;
	
	size_t _file_size;
	size_t _max_chunk;
	size_t _chunk_size;
	char* _line_buffer; 
	int _pos_buffer;
	size_t _file_location;
	size_t _initial_position;
	size_t _chunk_location;
	char* _data;

	//define the variables for the info tree:
	float m_readout_temp, m_sensor_temp;
	double m_start_time, m_bias, m_relative_start_time, m_relative_end_time;
	int m_threshold, m_lost_hits;
	string m_chipboard_id, m_readout_ip, m_interface, m_start_time_string, m_readout_mode, m_det_mode;
	short m_dacs[18];
};

