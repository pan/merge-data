#ifndef RootFileReader_H
#define RootFileReader_H

#include<iostream>
#include<fstream>

#include "TFile.h"
#include "TTree.h"

#include "FileReader.h"
#include "Pixel_data.h"
#include "PrecompilerDefinitions.h"

using namespace std;
using namespace boost::filesystem;

class RootFileReader : public FileReader{
	public:
		RootFileReader(std::string &fn);
		~RootFileReader();

		void PrintCurrentFilename()					{ cerr << m_filename.c_str() << endl; }
		
		void Initialize(){ 
			SetBranchAddresses(); 
			LoadNextEntry();
		}
		Long64_t GetEntries() 			const		{ return m_entries; }
		bool LoadNextEntry();
		const pixel_data& GetPixelData() 	const		{ return m_input_data; }
		const double& GetToA() 		const		{ return m_input_data.toa; }
		double GetStartTime() 			const		{ return -1; }
		
		//void ReadInfoFile( string fn ){ cerr << "NOT SUPPORTED BY THE ROOTFILEREADER" << endl; }

	private:
		RootFileReader( const RootFileReader& r);

		void SetBranchAddresses();		
		string m_filename;
		Long64_t m_entries;
		Long64_t m_current_position;
		TFile* m_f;
		TTree* m_tree;
};

#endif // !RootFileReader_H