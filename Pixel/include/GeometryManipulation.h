#pragma once 

#define _USE_MATH_DEFINES
#include <math.h> 
#include <iostream>
#include <vector>
#include <array>

#include "Pixel_data.h"

using namespace std;

class GeometryManipulator {
public:
	GeometryManipulator();
	~GeometryManipulator() {};
	GeometryManipulator(const GeometryManipulator& a) = default;

	void SetRotationX(const float& a) { _rot_x_deg = a; CalculateRotationMatrix(1); }
	void SetRotationY(const float& a) { _rot_y_deg = a; CalculateRotationMatrix(2); }
	void SetRotationZ(const float& a) { _rot_z_deg = a; CalculateRotationMatrix(3); }
	
	void SetRotationAxisAngle(const int& i, const float& a);

	void PrintRotationMatrices() const;

	void SetTranslationX(const int& t) { _translation_x_pix = t; }
	void SetTranslationY(const int& t) { _translation_y_pix = t; }
	
	void SetPixelPitchGapX(const float& t) { _gap_x_pix = t; }
	void SetPixelPitchGapY(const float& t) { _gap_y_pix = t; }

	void SetMatrixCenter(const float& x, const float& y) { _matrix_center[0] = x; _matrix_center[1] = y; }

	void Rotate(int axis, vector<pixel_data>& vec) const;
	void Translate(vector<pixel_data>& vec) const;

	void SetTranslationXZ(const int& tx, const int& ty) {
		_translation_x_pix = tx;
		_translation_y_pix = ty;
	}

private:
	float _rot_x_deg;
	float _rot_y_deg;
	float _rot_z_deg;

	int _translation_x_pix;
	int _translation_y_pix;

	float _gap_x_pix;
	float _gap_y_pix;

	std::array<std::array<float, 2>, 2> _rot_matrix_x;
	std::array<std::array<float, 2>, 2> _rot_matrix_y;
	std::array<std::array<float, 2>, 2> _rot_matrix_z;

	std::array<float, 2> _matrix_center;

	void CalculateRotationMatrix(int axis); //axis: 1 - rot_x; 2 - rot_y; 3 - rot_z
};