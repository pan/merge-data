#pragma once

#include <iostream>
#include <fstream>
#include <array>

#include <boost/filesystem.hpp>
#include "TROOT.h"
#include "TTree.h"

#include "Pixel_data.h"
#include "PixelCalibrator.h"
#include "PrecompilerDefinitions.h"
#include "GeometryManipulation.h"
#include "AditionalFunctions.h"

using namespace std;
using namespace boost::filesystem;

class FileReader{
	public:
		FileReader();
		virtual ~FileReader() = 0;
		FileReader(const FileReader& f) = default;
		
		void SetTimeOffset(double t)								{ m_time_offset = t; }
		double GetTimeOffset() const								{ return m_time_offset; }
		
		void LoadCalibrationFiles(const string &dir);
		void LoadColumnwiseToaCorrectionFile(std::string& file) { FillMatrixFromAsciiFile(file.c_str(), m_toa_bug_correction); PrintToACorrectionMap();}
		bool LoadMaskedPixelList(std::string &fn);

		void SetTimewalkCorrectionParams( const float &p0, const float &p1, const float &p2, const float &p3 );
		void PrintToACorrectionMap();
		void PrintTimewalkCorrectionParams();

		virtual void Initialize() = 0;
		virtual Long64_t GetEntries() const = 0;
		virtual bool LoadNextEntry() = 0;
		virtual const pixel_data& GetPixelData() const = 0;
		virtual const double& GetToA() const = 0;
		virtual double GetStartTime() const = 0;

		void SetMatrixClock(const int& m)					{ m_matrix_clock = m; }
		const int GetMatrixClock() const					{ return m_matrix_clock; }

		void SetPixelPitch(const int& p)					{ m_pixel_pitch = p; m_pixel_pitch_scale = m_pixel_pitch / PIXEL_PITCH_ASIC; }
		const int GetPixelPitch() const						{ return m_pixel_pitch; }
		const int GetPixelPitchScale() const				{ return m_pixel_pitch_scale; }

		void SetMatrixClockRescale(const double& rs)		{ m_matrix_clock_rescale = rs; }
		double GetMatrixClockRescale() const				{ return m_matrix_clock_rescale; }

		TTree* GetDscData() const							{ return m_dsc_tree; }
		
		void MaskPixel( const int& x, const int& y )		{ m_mask[ x + MATRIX_WIDTH*y ] = true; }
		void MaskPixel( const int& idx )					{ m_mask[idx] = true; }

		void SetRotationX(const float& a)					{ _geometry_manipulation.SetRotationX(a); _rotation_commands.push_back(1); }
		void SetRotationY(const float& a)					{ _geometry_manipulation.SetRotationY(a); _rotation_commands.push_back(2); }
		void SetRotationZ(const float& a)					{ _geometry_manipulation.SetRotationZ(a); _rotation_commands.push_back(3); }
		void SetRotationAxisAngle(const int& i, const float& a)			
															{ _geometry_manipulation.SetRotationAxisAngle(i, a); _rotation_commands.push_back(i); }
		void SetTranslationXY(const int& x, const int& y)	{ _geometry_manipulation.SetTranslationXZ(x, y); _translate = true; }

		void SetPixelPitchGapXY(const float& x, const float& y)	{ _geometry_manipulation.SetPixelPitchGapX(x); _geometry_manipulation.SetPixelPitchGapY(y);}

		void SetLayer(const int& l)							{ _layer = l; }
		const int& GetLayer() const							{ return _layer; }

		void PrintCalibrationFiles();

	protected:
		bool FillMatrixFromAsciiFile(const std::string& fn, float* m);
		bool FillMatrixFromAsciiFile(const std::string& fn, std::array<float,MATRIX_WIDTH>& m);

		pixel_data m_input_data;
		int _layer;

		bool m_energy_calibration;
		PixelCalibrator _calibrator;
		
		GeometryManipulator _geometry_manipulation;
		vector<int> _rotation_commands; //for each added rotation push_back the axis index: 1-x, 2-y, 3-z
		bool _translate;

		std::array<float, MATRIX_WIDTH> m_toa_bug_correction;
		std::array<bool, ARRAY_LENGTH> m_mask;
		
		int m_matrix_clock; //Matrix clock for sToA measurement in MHz
		double m_toa_clock_scale;
		double m_ftoa_clock_scale;
		
		int m_pixel_pitch;
		int m_pixel_pitch_scale;

		double m_matrix_clock_rescale;

		double m_time_offset;
		TTree* m_dsc_tree;
};