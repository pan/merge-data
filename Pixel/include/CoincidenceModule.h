#ifndef CoincidenceModule_H
#define CoincidenceModule_H

#include<vector>
#include<sstream>

#include "TFile.h"
#include "TTree.h"

#include "SpatialClusterer.h"
//#include "ClusterAnalyser.h"

using namespace std;

class CoincidenceModule{
	public:
		CoincidenceModule(short &l);
		~CoincidenceModule();

		void DefineRootTree();
		void Reset();

		void AddCoincidenceGroup(vector<Cluster>& c) { _coincident_clusters = c; }

		bool EvaluateCoincidence();
		void FillTree();

		Long64_t GetCoincNo() const { if (_save) { return m_coinc_number; } else { return -1; } }
		TTree* GetResults() const					{ return m_t;}

	private:
		CoincidenceModule(const CoincidenceModule &c);

		vector<Cluster> _coincident_clusters;

		bool _save;
		vector<bool>	m_involved_layers;
		vector<float>	m_tot_layer;
		vector<int>		m_no_of_pixels_layer;
		vector<double>	m_min_toa_layer;
		
		short m_no_layers;
		TTree* m_t;
		
		Long64_t m_coinc_number;
		short m_coinc_size;
		short m_coinc_type;
		
		//Long64_t m_cluster_index;
		
		double m_min_toa;
};

#endif // !CoincidenceModule_H