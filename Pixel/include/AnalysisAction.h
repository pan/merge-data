#pragma once

#include<vector>

#include "boost/bind/bind.hpp"
#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#undef min
#undef max

#include "FileReader.h"
#include "RootOutputCreator.h"
#include "CoincidenceModule.h"
#include "PrecompilerDefinitions.h"
#include "Pixel_data.h"
#include "CoincidenceGroup.h"
#include "SpatialClusterer.h"
#include "AsciiFileReader.h"
#include "KathrineFileReader.h"
#include "KatherineFileReaderFast.h"
#include "InputDataFormats.h"
#include "DetectorConfigurator.h"

using namespace std;
using namespace boost::filesystem;
using namespace boost::placeholders;

class AnalysisAction{
	public:
		AnalysisAction();
		~AnalysisAction();
		AnalysisAction(const AnalysisAction& a) = default; // TO BE DONE

		void Initialize(vector<string>& f_in, const string& f_trg_in, double &trg_w, const string& f_out, double &tw, vector<double>& to);
		void Initialize(const string& fn, const string& f_trg_in, double &trg_w, const string& f_out, const string& config_file, double& tw, const string& suffix);
		
		short GetNDetectors() const							{ return m_number_of_detectors; }
		FileReader* GetFileReader(int i) const				{ return raw_input[i]; }

		bool SetToABugCorrectionMap( vector<string> &files );
		void SetEnergyCalibration( vector<string> &dir );
		void SetTimewalkCorrectionParams( vector<string> &files );
		void SetInputFormat( input_data_formats f )			{ m_data_format = f; }
		void SetMatrixClocks( vector<int> &mc );
		void SetMatrixClockRescale( vector<double> &rs );
		void SetPixelPitch( vector<int> &pp );
		
		void LoadPixelMasks( vector<string> &files );
		
		void Run();
		void Finalize();
		
	private:
		void SetTimeWindow(const double &t){ m_time_window = t; }
		double GetTimeWindow() const { return m_time_window; }
		void ReadTimewalkCorrectionFile(string fn, FileReader* fr);
		void ReadTriggerFile(string fn);
		
		short GetClosestEntry(AsciiFileReader** rr, bool &eof);
		short GetClosestEntry(vector<AsciiFileReader*> rr, bool& eof);
		
		short GetClosestEntry(FileReader** rr, bool &eof);
		short GetClosestEntry(vector<FileReader*> rr, bool& eof);
		
		void FillOutputDataContainer();

		double m_time_window;
		short m_number_of_detectors, m_number_of_layers;
		bool m_coincidence_module;
		bool m_use_ecali;

		vector<Cluster> _vec;
		vector<Cluster> _coincident_clusters;

		vector<pair<double,int>> trigger_list;   //trigger time, trigger ID

		
		input_data_formats m_data_format;

		vector<bool> m_root_eof;
		long long m_cluster_counter;

		//vector<FileReader*> raw_input;
		//vector<SpatialClusterer*> coinc_group;

		DetectorConfigurator _detector_configuration;
		FileReader** raw_input;
		RootOutputCreator* cluster_output;
		CoincidenceModule* coinc_analyser;
		SpatialClusterer** coinc_group;
};