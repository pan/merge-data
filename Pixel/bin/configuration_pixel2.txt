#Define detectors
Chip Id: J10-W00076 E9-W00076 D9-W00076 

Rotation: 	J10-W00076 3 90
Translation: 	J10-W00076 256 256
Layer:		J10-W00076 0
Timewalk:	J10-W00076 /cvmfs/pan.cern.ch/Data/BeamTest/2023/Apr_PS_CERN/MiniPAN/calibration/J10-W00076/timewalk_parameters.txt	
Calibration:	J10-W00076 /cvmfs/pan.cern.ch/Data/BeamTest/2023/Apr_PS_CERN/MiniPAN/calibration/J10-W00076

Rotation: 	D9-W00076 3 90
Translation: 	D9-W00076 256 0
Layer:		D9-W00076 0
#Column correction:	D9-W00076 /mnt/35251/data/2022_SPS_August_H8/__PAN/analysis_configs/E9-W00076_columns.txt
Timewalk:	D9-W00076 /cvmfs/pan.cern.ch/Data/BeamTest/2023/Apr_PS_CERN/MiniPAN/calibration/D9-W00076/timewalk_parameters.txt	
Calibration:	D9-W00076 /cvmfs/pan.cern.ch/Data/BeamTest/2023/Apr_PS_CERN/MiniPAN/calibration/D9-W00076

Rotation: 	E9-W00076 3 270
Layer:		E9-W00076 0
#Column correction:	/mnt/35251/data/2022_SPS_August_H8/__PAN/analysis_configs/E9-W00076_columns.txt
Timewalk:	/cvmfs/pan.cern.ch/Data/BeamTest/2023/Apr_PS_CERN/MiniPAN/calibration/E9-W00076/timewalk_parameters.txt	
Calibration:	/cvmfs/pan.cern.ch/Data/BeamTest/2023/Apr_PS_CERN/MiniPAN/calibration/E9-W00076

Pixel_Pitch_Gap_XY: 2.3521521 1.7810288
