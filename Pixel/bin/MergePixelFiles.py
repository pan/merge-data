#!/usr/bin/env python3
import csv
import glob, os
import os.path
import sys
import time
import re

def main():

  filePrefix = ""
  if not (len(sys.argv) == 3): 
    PrintSetup()
    return
  else:
    print(sys.argv)
    fileDirectory = sys.argv[1]
    filePrefix    = sys.argv[2]

  ReadContent(fileDirectory, filePrefix)

def PrintSetup():
  print("Wrong format. Use:")
  print("MergePixelFiles.py <directory> <prefix_name>")

def ReadContent(fileDirectory, filePrefix):

  os.chdir(fileDirectory)

  #trigger
  f_trigger = open(filePrefix+"trigger.txt", "w")
  files       = []

  for filename in glob.glob(filePrefix+"*_trigger.txt"):
    files.append(filename)

  files.sort()

  t_unix0_trig = 0
  t_unix_trig  = [0]*len(files)
  max_trigger_files = [0]*5
  file_counter = -1
  for filename in files:
    file_counter+=1
    ii_max_trigger = [0]*5
    with open(filename, newline='') as csvfile:
      spamreader = csv.reader(csvfile, delimiter='\t')

      for row in spamreader:
        if "#" in row[0]: 
          if "_0_trigger" in filename: 
            f_trigger.write(row[0]+"\n")

          if "unix time:" in row[0]:
            if "_0_trigger" in filename: 
              t_unix0_trig = float(row[0].split("unix time:")[1])
            t_unix_trig[file_counter] = float(row[0].split("unix time:")[1])

          continue

        gpio_id = int(row[0])
        tt_     = float(row[1])
        trig_id = int(row[2])

        gpio_id_array = None
        if(gpio_id>>0): gpio_id_array = 0
        if(gpio_id>>1): gpio_id_array = 1
        if(gpio_id>>2): gpio_id_array = 2
        if(gpio_id>>3): gpio_id_array = 3
        if(gpio_id>>4): gpio_id_array = 4

        f_trigger.write("%d\t%10.10lf\t%d\n" % (gpio_id,tt_ + 3600.*file_counter, max_trigger_files[gpio_id_array] + trig_id))
        if(trig_id > ii_max_trigger[gpio_id_array]): ii_max_trigger[gpio_id_array] = trig_id

    for ii in range(0,5): max_trigger_files[ii] += ii_max_trigger[ii]

  f_trigger.close()  
  print("Trigger files merged! \n")      

  #data
  f_data = {}
  files = []
  for filename in glob.glob(filePrefix+"*_*.txt"):
    if "trigger" in filename: continue
    files.append(filename)

  files.sort()
  file_counter = {}
  t_unix0_data = 0
  t_unix_data  = [0]*len(files)
  for filename in files:
  
    print(filename,file_counter)

    chip_id = re.search('%s(.+?)%s' % (filePrefix,"_*.txt"), filename).group(1)[:-2]
    if not chip_id in f_data:
      print(filename,chip_id)
      f_data[chip_id] = open(filePrefix+chip_id+".txt", "w")
      file_counter[chip_id] = 0
    else:
      file_counter[chip_id] += 1

    with open(filename, newline='') as csvfile:
      spamreader = csv.reader(csvfile, delimiter='\t')

      for row in spamreader:
        if "#" in row[0]: 
          if "_0.txt" in filename: 
            for iRow in  range(0,len(row)):
              f_data[chip_id].write(row[iRow]+"\t")
            f_data[chip_id].write("\n")

          if "unix time:" in row[0]:
            if "_0.txt" in filename: 
              t_unix0_data = float(row[0].split("unix time:")[1])
            t_unix_data[file_counter[chip_id]] = float(row[0].split("unix time:")[1])

          continue

        f_data[chip_id].write("%ld\t%ld\t%ld\t%ld\n" % (int(row[0]),int(row[1])+int((3600.*file_counter[chip_id])/25/1e-9),int(row[2]),int(row[3])))
        #f_data[chip_id].write("%ld\t%lf\t%ld\t%ld\n" % (int(row[0]),float(row[1])*25*1e-9+3600.*file_counter[chip_id],int(row[2]),int(row[3])))

  for key in f_data:
    f_data[key].close() 

  print("Data files merged! \n")   

if __name__ == "__main__":
        main()
