#!/bin/sh
#SBATCH --job-name PixelClustering_PAN
#SBATCH --error=/home/users/h/hulsman/scratch/job_info/err/PixelClustering_PAN_%j.err
#SBATCH --output=/home/users/h/hulsman/scratch/job_info/out/PixelClustering_PAN_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=private-dpnc-cpu
#SBATCH --mem-per-cpu=10000
#SBATCH --time 24:00:00
#SBATCH --mail-user=johannes.hulsman@unige.ch
#SBATCH --mail-type=ALL

PIXEL_DIR="/home/users/h/hulsman/scratch/merge-data/Pixel/"

##Set your environment
source /home/users/h/hulsman/scratch/AnalysisEnv.sh

#go to bin/ and generate cluster files
cd ${PIXEL_DIR}/bin

#SRUN_COMMAND1_XXX

SRUN_COMMAND2_XXX
