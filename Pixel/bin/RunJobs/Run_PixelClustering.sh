#!/bin/sh

########################################################################

DATA_DIR="/srv/beegfs/scratch/groups/dpnc/pan/BeamTest/2023/August_SPS/"
PIXEL_DIR="/home/users/h/hulsman/scratch/merge-data/Pixel/"
MERGE_DIR="/home/users/h/hulsman/scratch/merge-data/Merge/"

job_id=1

#Actions in Pixel directory
for dir in $DATA_DIR/Pixels/rawdata/*/; do
    dir_name=${dir#"$DATA_DIR/Pixels/rawdata/"}
    dir_name=${dir_name%"/"}

    cp $PIXEL_DIR/bin/RunJobs/Run_PixelClustering_tmp.sh $PIXEL_DIR/bin/RunJobs/Run_PixelClustering_${dir_name}_${job_id}.sh

    ##one of pixels did not work
    #command1="srun ${PIXEL_DIR}/bin/Pixel_Clustering --multilayer ${PIXEL_DIR}/bin/configuration_pixel1.txt --trigger_file ${dir}/pixel1_trigger.txt -i ${dir}/pixel1_ -o ${PIXEL_DIR}/bin/pixel1_${dir_name}.root"
    command2="srun ${PIXEL_DIR}/bin/Pixel_Clustering --multilayer ${PIXEL_DIR}/bin/configuration_pixel3.txt --trigger_file ${dir}/pixel2_trigger.txt -i ${dir}/pixel2_ -o ${PIXEL_DIR}/bin/pixel3_${dir_name}.root"

    #sed -i "s|SRUN_COMMAND1_XXX|$command1|g" $PIXEL_DIR/bin/RunJobs/Run_PixelClustering_${dir_name}_${job_id}.sh
    sed -i "s|SRUN_COMMAND2_XXX|$command2|g" $PIXEL_DIR/bin/RunJobs/Run_PixelClustering_${dir_name}_${job_id}.sh

    #echo "$PIXEL_DIR/bin/RunJobs/Run_PixelClustering_${dir_name}_${job_id}.sh"
    sbatch $PIXEL_DIR/bin/RunJobs/Run_PixelClustering_${dir_name}_${job_id}.sh

    ((job_id=job_id+1))

done
