#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING true

#include <algorithm>
#include <string>
#include <experimental/filesystem>

#include "FileReader.h"

using namespace std;

namespace fs = std::experimental::filesystem;

FileReader::FileReader() {
	std::fill(m_toa_bug_correction.begin(), m_toa_bug_correction.end(), 0.f);
	std::fill(m_mask.begin(), m_mask.end(), false);

	m_time_offset = 0;
	m_energy_calibration = false;

	m_matrix_clock = 40;
	m_matrix_clock_rescale = 1;

	m_pixel_pitch = PIXEL_PITCH_ASIC;
	m_pixel_pitch_scale = m_pixel_pitch / PIXEL_PITCH_ASIC;
	_translate = false;
	_layer = 0;

	m_dsc_tree = NULL;
	m_ftoa_clock_scale = 1.5625;
	m_toa_clock_scale = 25.;
}

FileReader::~FileReader() {}

bool FileReader::FillMatrixFromAsciiFile(const std::string& fn, float* m) {
	std::ifstream in;
	in.open(fn.c_str(), ios::in);

	long position = 0;
	if (!in.good()) {
		cerr << "WARNING: " << fn.c_str() << " does not exist!" << endl;
		return false;
	}
	else {
		while (1) {
			in >> m[position];
			if (in.eof()) { break; }
			position++;
		}
		return true;
	}
}

bool FileReader::FillMatrixFromAsciiFile(const std::string& fn, std::array<float,MATRIX_WIDTH>& m) {
	std::ifstream in;
	in.open(fn.c_str(), ios::in);

	long position = 0;
	if (!in.good()) {
		cerr << "WARNING: " << fn.c_str() << " does not exist!" << endl;
		return false;
	}
	else {
		while (1) {
			in >> m[position];
			if (in.eof()) { break; }
			position++;
		}
		return true;
	}
}

void FileReader::LoadCalibrationFiles(const string &dir){
	array<path,4> p_calib;
	p_calib.fill(path(""));

	for (const auto& entry : fs::directory_iterator(dir)) {
		if (entry.path().filename().string().find("a.txt") != string::npos)
			p_calib[0] = entry.path();
		if (entry.path().filename().string().find("A.txt") != string::npos)
			p_calib[0] = entry.path();

		if (entry.path().filename().string().find("b.txt") != string::npos)
			p_calib[1] = entry.path();
		if (entry.path().filename().string().find("B.txt") != string::npos)
			p_calib[1] = entry.path();

		if (entry.path().filename().string().find("c.txt") != string::npos)
			p_calib[2] = entry.path();
		if (entry.path().filename().string().find("C.txt") != string::npos)
			p_calib[2] = entry.path();

		if (entry.path().filename().string().find("t.txt") != string::npos)
			p_calib[3] = entry.path();
		if (entry.path().filename().string().find("T.txt") != string::npos)
			p_calib[3] = entry.path();
	}

	if (p_calib[0].string() != "")
		_calibrator.load_a(p_calib[0].string());
	if (p_calib[1].string() != "")
		_calibrator.load_b(p_calib[1].string());
	if (p_calib[2].string() != "")
		_calibrator.load_c(p_calib[2].string());
	if (p_calib[3].string() != "")
		_calibrator.load_t(p_calib[3].string());

	m_energy_calibration = true;
}

bool FileReader::LoadMaskedPixelList( std::string& fn ){
	std::ifstream in;
	in.open(fn.c_str(), ios::in);

	int x,y;
	long position = 0;
	if(!in.good()){
		cerr << "WARNING: Masked Pixel List " << fn.c_str() << " does not exist!" <<endl;
		return false;
	}
	
	std::string line;

	while(getline(in, line)){
		if (line == "") continue;

		std::stringstream ss(line);
		ss >> x >> y;
		MaskPixel( x, y );
		position++;
		
		if( in.eof() ){ break; }
	}

	cerr << "Masked pixel list contains "<< position << " masked pixels" << endl;

	if (position > 0)
		return true;
	else
		return false;
}

void FileReader::PrintToACorrectionMap(){
	//~ for(int i = 0; i < MATRIX_WIDTH; i++){
		//~ cerr << m_toa_bug_correction[i] << ", ";
	//~ }
	//~ cerr << "\n";
	
	for(int i = 0; i < MATRIX_WIDTH; i++){
		if( m_toa_bug_correction[i] != 0){
			cerr <<"correct column: " << i << " with " << m_toa_bug_correction[i] << endl;;
		}
	}
	cerr << "\n";
}

void FileReader::PrintCalibrationFiles(){
	for (int i = 0; i < ARRAY_LENGTH; i++) {
		cout << _calibrator.get_coeffs()[i].a << ", ";
	}
	cout << "\n";

	for (int i = 0; i < ARRAY_LENGTH; i++) {
		cout << _calibrator.get_coeffs()[i].b << ", ";
	}
	cout << "\n";

	for (int i = 0; i < ARRAY_LENGTH; i++) {
		cout << _calibrator.get_coeffs()[i].c << ", ";
	}
	cout << "\n";

	for (int i = 0; i < ARRAY_LENGTH; i++) {
		cout << _calibrator.get_coeffs()[i].t << ", ";
	}
	cout << "\n";
}

void FileReader::SetTimewalkCorrectionParams(const float &p0, const float& p1, const float&p2, const float&p3){
	_calibrator.load_tw_coeffs(p0, p1, p2, p3);
}

void FileReader::PrintTimewalkCorrectionParams(){
	cerr << "f(x) = p0 / (x - p1)**p2 + p3; x = Energy (keV); \n Parameters: \n"; 
	cerr << "p0 = " << _calibrator.get_tw_parameter(0) << "," <<
			"p1 = " << _calibrator.get_tw_parameter(1) << "," <<
			"p2 = " << _calibrator.get_tw_parameter(2) << "," <<
			"p3 = " << _calibrator.get_tw_parameter(3) << endl;
}