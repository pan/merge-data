#include<vector>
#include<string>
#include<sstream>

#include "AditionalFunctions.h"

using namespace std;

void SplitCommaSeparatedInput(string& s, vector<string>& v) {
	v.clear();
	istringstream ss(s);
	std::string t;
	while (getline(ss, t, ',')) {
		v.push_back(t);
	}
}

void SplitCommaSeparatedInput(string& s, vector<double>& v) {
	v.clear();
	istringstream ss(s);
	std::string t;

	double value;
	while (ss >> value) {
		v.push_back(value);
		if (ss.peek() == ',') { ss.ignore(); }
	}
}

void SplitCommaSeparatedInput(string& s, vector<int>& v) {
	v.clear();
	istringstream ss(s);
	std::string t;

	double value;
	while (ss >> value) {
		v.push_back((int)value);
		if (ss.peek() == ',') { ss.ignore(); }
	}
}