#include <algorithm>
#include <iomanip>
#include <string>

//#include <boost\iostreams\stream.hpp> 
//#include <boost\spirit\include\qi.hpp>
//#include <cstdint>

#include "AsciiFileReader.h"

using namespace std;
namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;

#define SAFETY_MARGIN 300000

BOOST_FUSION_ADAPT_STRUCT(
	pixel_data,
	(long long, idx)
	(int, matrix_idx)
	(long long, sToA)
	(int, tot)
	(int, fToA)
	(int, overflow)
)

template <typename Iterator>
struct pixel_parser : qi::grammar<Iterator, pixel_data(), ascii::space_type>
{
	pixel_parser() : pixel_parser::base_type(mystruct)
	{
		using qi::int_;
		using qi::long_long;
		mystruct %= int_ >> int_ >> long_long >> int_ >> int_ >> int_;
	}
	qi::rule<Iterator, pixel_data(), ascii::space_type> mystruct;
};

AsciiFileReader::AsciiFileReader(string &fn){
	m_infile.open(fn, boost::iostreams::mapped_file::readonly);
	m_start_of_file = m_infile.const_begin();
	m_end_of_file = m_infile.const_end();
	m_temp_pos = m_start_of_file;

	std::cerr << fn.c_str() << std::endl;
	if (!m_infile.is_open()){
		std::cerr << "No input file was found!" << endl;
	}

	m_filename = fn;

	m_eof = false;
	m_finish = false;
	m_nlines = 0;

	m_dsc_tree = new TTree("InfoTree", "");
	m_rollover_counter = 0;
	m_rollover_delay = 1000;
	m_pixels_since_rollover = 0;

	m_data_vector[0].reserve(MAX_CHUNK_LENGTH);
	m_data_vector[1].reserve(MAX_CHUNK_LENGTH);
}

AsciiFileReader::~AsciiFileReader(){
	m_infile.close();
}

void AsciiFileReader::ProcessPixel() {
	//std::cerr << "process pixel" << std::endl;
	m_input_data.pixel_x = m_input_data.matrix_idx % MATRIX_WIDTH;
	m_input_data.pixel_y = m_input_data.matrix_idx / MATRIX_WIDTH;

	m_input_data.toa = m_input_data.sToA * 25. - m_input_data.fToA * 1.5625 + m_toa_bug_correction[m_input_data.pixel_x];
	m_input_data.toa += m_time_offset;
}

//void AsciiFileReader::CalibratePixel() {
//	m_input_data.tot_keV = E_cali(
//		m_a[m_input_data.matrix_idx],
//		m_b[m_input_data.matrix_idx],
//		m_c[m_input_data.matrix_idx],
//		m_t[m_input_data.matrix_idx],
//		m_input_data.tot
//	);
//	if (m_tw_correction) {
//		Timewalk_correction(m_input_data.toa, m_input_data.tot_keV);
//	}
//}

bool AsciiFileReader::ReadChunkOfData(vector<pixel_data> &vec) {
	std::cerr << "READ CHUNK OF DATA" << endl;

	using iterator_type = const char*;
	using pixel_parser = pixel_parser<iterator_type>;

	string line;
	bool eof = false;

	short overflow = 0;
	long long previous_toa = 0;
	double prev_toa_ns = 0;

	if ( !m_infile.is_open() || m_temp_pos == m_end_of_file ) { return true; }

	while (m_temp_pos != m_end_of_file) {
		m_start_of_file = static_cast<const char*>(memchr(m_temp_pos, '\n', m_end_of_file - m_temp_pos));
		//string line(m_temp_pos, m_start_of_file);
		pixel_parser pp;
		bool r = phrase_parse( m_temp_pos, m_start_of_file, pp, boost::spirit::ascii::space, m_input_data );
		
		//if (line.compare(0, 5, "Index") == 0) {
		if(!r){
			string test(m_temp_pos, m_start_of_file - m_temp_pos);
			std::cerr << "HEADER: " << test << std::endl;
		}
		else{
			//std::istringstream iss(line);
			//iss >> m_input_data.idx >> m_input_data.matrix_idx >> m_input_data.toa >> m_input_data.tot >> m_input_data.fToA;
			ProcessPixel();
			m_pixels_since_rollover++;

			if (m_input_data.sToA - previous_toa > 8E8 &&
				m_pixels_since_rollover < m_rollover_delay) {
				m_input_data.toa -= ROLLOVER_ADD * m_rollover_counter * 25.;
			}

			if (m_input_data.sToA - previous_toa < -8E8) {
				std::cerr << "overflow counter: " << m_rollover_counter << endl;
				m_pixels_since_rollover = 0;
				m_rollover_counter++;
			}

			m_input_data.toa += ROLLOVER_ADD * m_rollover_counter * 25.;

			//if (m_energy_calibration) { CalibratePixel(); }
			//else{ m_input_data.tot_keV = -1;}

			if (!m_mask[m_input_data.pixel_x + MATRIX_WIDTH * m_input_data.pixel_y]) {
				vec.push_back(m_input_data);
			}

			if ((m_input_data.toa - prev_toa_ns) / 1E9 < -1) {
				std::cerr << m_input_data.toa / 1E9 << ", " << prev_toa_ns / 1E9 << ", " << (m_input_data.toa - prev_toa_ns) / 1E9 << endl;
				m_rollover_counter++;
			}

			previous_toa = m_input_data.sToA;
			prev_toa_ns = m_input_data.toa;
		}

		m_start_of_file++;
		m_temp_pos = m_start_of_file;

		m_nlines++;
		if (vec.size() > MAX_CHUNK_LENGTH - SAFETY_MARGIN) { break; }
	}

	if (m_temp_pos == m_end_of_file) {
	//if(m_start_of_file == m_end_of_file){
		vec.pop_back();
		eof = true;
	}

	m_nentries = vec.size();
	std::sort(vec.begin(), vec.end(), pixel_data::compare_by_toa);

	std::cerr << "vector size: " << vec.size() << endl;
	if (m_energy_calibration) { _calibrator.calibrate(vec); }


	return eof;
}

void AsciiFileReader::Initialize(){
	cerr << "INITIALIZE" << endl;
	ReadInfoFile( (m_filename + ".info").c_str() );

	for( int i = 0; i < 2; ++i ){
		m_eof = ReadChunkOfData( m_data_vector[i] );
		m_current_position[i] = 0;
	}
	
	m_active_stream = 0;
	m_passive_stream = 1;
	m_active_vector = 0;
	m_current_position[0]++;
	
	if(m_data_vector[1].size() == 0){ m_finish = true; }
	else{ 
		m_overlap_time = m_data_vector[1].at(0).toa; 
		TimeOverlapRangeCheck( m_data_vector[m_active_stream], m_data_vector[m_passive_stream] );
	}
}

bool AsciiFileReader::LoadNextEntry(){
	if( m_current_position[m_active_vector] == m_data_vector[m_active_vector].size() ){

		if( m_finish ){ return true; }
		
		if( m_eof ){ m_finish = true; }
		else{
			m_data_vector[m_active_vector].clear();
			m_eof = ReadChunkOfData(m_data_vector[m_active_vector]);
			m_overlap_time = m_data_vector[m_active_vector].at(0).toa;
		}

		m_current_position[m_active_vector] = 0;

		if( m_active_stream == 0 ){ 
			m_active_stream = 1; 
			m_passive_stream = 0;
		}
		else{ 
			m_active_stream = 0; 
			m_passive_stream = 1;
		}
		m_active_vector = m_active_stream;
		
		if( !m_eof ){
			TimeOverlapRangeCheck( m_data_vector[m_active_stream], m_data_vector[m_passive_stream] );
		}
	}
	
	if( m_data_vector[m_active_vector].at(m_current_position[m_active_vector]).toa <	
		m_overlap_time || m_finish ){
			m_current_position[m_active_stream]++;
	}
	else{
		if( m_data_vector[0].at(m_current_position[0]).toa < 
			m_data_vector[1].at(m_current_position[1]).toa ){
				m_current_position[0]++;
				m_active_vector = 0;
		}
		else if( m_data_vector[0].at(m_current_position[0]).toa >= 
			m_data_vector[1].at(m_current_position[1]).toa ){
				m_current_position[1]++;
				m_active_vector = 1;
		}
	}
	
	return false;
}

void AsciiFileReader::ReadInfoFile( string fn ){
	 cerr << "READ INFO FILE" << endl;

	 string line;
	 string format;
	 std::ifstream info(fn.c_str());
	 if( !info.good() ){
		 cerr << "WARNING :: INFO FILE NOT FOUND!" << endl;
	 }

	 while( getline(info, line) ){
		 if( line.compare( "\"Acq Serie Start time\" (\"Acquisition serie start time\"):" ) == 0){
			 m_dsc_tree->Branch("serie_start_time", &m_serie_start_time, "serie_start_time/D");
			 info >> format >> m_serie_start_time;
		 }
		 else if( line.compare( "\"Acq time\" (\"Acquisition time [s]\"):" ) == 0){
			 m_dsc_tree->Branch("acq_time", &m_acq_time, "acq_time/D");
			 info >> format >> m_acq_time;
		 }
		 else if( line.compare("\"ChipboardID\" (\"Chipboard ID\"):") == 0 ){
			 m_dsc_tree->Branch("chipboard_id", &m_chipboard_id);
			 info >> format >> m_chipboard_id;
		 }
		 else if( line.compare("\"DACs\" (\"DACs\"):") == 0 ){
			 m_dsc_tree->Branch("dacs", m_dacs, "dacs[17]/S");
			 info >> format;
			 for(int i = 0; i < 17; ++i){
				 info >> m_dacs[i];
			 }
		 }
		 else if( line.compare( "\"HV\" (\"High voltage [V]\"):" ) == 0 ){
			 m_dsc_tree->Branch("bias", &m_bias, "bias/D");
			 info >> format >> m_bias;
		 }
		 else if( line.compare( "\"Interface\" (\"Readout interface\"):" ) == 0 ){
			  m_dsc_tree->Branch("interface", &m_interface);
			  info >> format >> m_interface;
		 }
		 else if( line.compare( "\"Pixet version\" (\"Pixet version\"):" ) == 0 ){
			 m_dsc_tree->Branch("pixet_version", &m_pixet_version);
			 info >> format >> m_pixet_version;
		 }
		 else if( line.compare( "\"Start time\" (\"Acquisition start time\"):" ) == 0){
			 m_dsc_tree->Branch("start_time", &m_start_time, "start_time/D");
			 info >> format >> m_start_time;
		 }
		 else if( line.compare( "\"Start time (string)\" (\"Acquisition start time (string)\"):") == 0 ){
			 m_dsc_tree->Branch("start_time_string", &m_start_time_string);
			 getline(info,format);
			 getline(info,m_start_time_string);
		 }
		 else if( line.compare( "\"Threshold\" (\"Threshold [keV]\"):" ) == 0 ){
			 m_dsc_tree->Branch("threshold", &m_threshold, "threshold/D");
			 info >> format >> m_threshold;
		 }
	 }
	 info.close();
	 m_dsc_tree->Fill();
 }

void AsciiFileReader::TimeOverlapRangeCheck( vector<pixel_data> &v_act, vector<pixel_data> &v_pas ){
	//Check if active vector's last entry is bigger than passive vector's last entry.
	//Remove entries from the active vector until passive vector has higher end time.
	size_t size_passive = v_pas.size();
	double passive_end_time = v_pas.at( size_passive-1 ).toa;

	cerr << "passive end time: " << setprecision(20) << passive_end_time << ", " << v_act.at( v_act.size()-1 ).toa << endl;

	while( v_act.at( v_act.size()-1 ).toa > passive_end_time ){
		cerr << v_act.at( v_act.size()-1 ).toa << endl;
		cerr << "removing entry and adding it to passive vector! " << endl;
		v_pas.insert( v_pas.begin() + size_passive, v_act.at( v_act.size()-1 ) ); 
		v_act.pop_back();
	}
}

