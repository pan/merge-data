#include "PixelCalibrator.h"

#include<omp.h>
#include<math.h>

void PixelCalibrator::load_file(const string& f, setter set) {
    std::ifstream in;
    in.open(f, ios::in);
    float val;
    int i = 0;
    while (!in.eof()) {
        in >> val;
        if (i >= MATRIX_HEIGHT * MATRIX_WIDTH) { break; }
        set(val, i);
        ++i;
    }
    cout << "Found " << i << " parameters in file: " << f << endl;
    in.close();
}

void PixelCalibrator::calibrate(vector<pixel_data>& vec) {
	vector<pixel_data>::iterator it;
	for (it = vec.begin(); it < vec.end(); ++it) {
		it->tot_keV = energy_cali(_coeff[it->matrix_idx], it->tot);
		if (_timewalk_correction) {
			size_t bin = (int) it->tot_keV / _bin_width;

			if (bin < _time_walk_look_up.size())
				it->toa = it->toa - _time_walk_look_up[bin];
		}
	}
}

float PixelCalibrator::energy_cali(const calib& c, const int& tot) {
    float arg = (c.b + c.a * c.t - tot) * (c.b + c.a * c.t - tot) + 4.0f * c.a * c.c;
    if (c.a == 0 || arg < 0) { return -1; }

    float e = 1.0f / (2.0f * c.a) * (tot + c.a * c.t - c.b + std::sqrt(arg));
    return !(e < 0) ? e : 0;
}

void PixelCalibrator::timewalk_correction(double& t, const float& e_keV) {
    if (e_keV > 40.f) { return; }

    double delta_t = (double) std::max<float>(_tw_parameters[0] / pow((e_keV - _tw_parameters[1]), _tw_parameters[2]) + _tw_parameters[3], 0.f);
    delta_t = std::min<double>(delta_t, 20.);

    if (delta_t < 0 || delta_t > 20)
        return;
    
    t = t - delta_t;
    return;
}

double PixelCalibrator::timewalk_correction(const float& e_keV) {
    if (e_keV == 0.f) { return 20.; }
    double delta_t = (double)std::max<float>(_tw_parameters[0] / pow((e_keV - _tw_parameters[1]), _tw_parameters[2]) + _tw_parameters[3], 0.f);

    if(isnan(delta_t) ||isinf(delta_t)) 
        delta_t = 20.;

    return std::min<double>(delta_t, 20.);
}

void PixelCalibrator::CreateTimeWalkLoopUp()
{
    for (size_t i = 0; i < _time_walk_look_up.size(); ++i) {
        _time_walk_look_up[i] = (double) timewalk_correction(i*_bin_width);
        //cerr << "energy: " << i * _bin_width << ", dt: " << _time_walk_look_up[i] << endl;
    }
}
