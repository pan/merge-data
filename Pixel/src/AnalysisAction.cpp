#include <fstream>
#include <iomanip>

#include "AnalysisAction.h"

using namespace std;

AnalysisAction::AnalysisAction(){
	m_cluster_counter = 0; //represents the number of the cluster -> entry in the cluster tree!
	m_coincidence_module = false;
	m_use_ecali = false;
	m_data_format = ASCII_KATHRINE;
	m_number_of_detectors = 1;
	m_number_of_layers = 1;
	m_time_window = 100.;
	coinc_analyser = NULL;
	cluster_output = NULL;
	raw_input = NULL;
	coinc_group = NULL;
}

AnalysisAction::~AnalysisAction(){
	for(int i = 0; i < m_number_of_detectors; i++){
		delete raw_input[i];
	}

	for (short i = 0; i < m_number_of_layers; ++i) {
		delete coinc_group[i];
	}

	delete[] raw_input;
	delete[] coinc_group;

	delete cluster_output;
	delete coinc_analyser;
}

void AnalysisAction::Initialize(vector<string>& f_in, const string& f_trg_in, double &trg_w, const string& f_out, double &tw, vector<double>& to){
	SetTimeWindow(tw);
	m_number_of_detectors = (short) f_in.size();

	if(m_number_of_detectors > 1){ m_coincidence_module = true; }

	//raw_input.resize(m_number_of_detectors);
	//coinc_group.resize(m_number_of_detectors);
	
	m_root_eof.resize(m_number_of_detectors);
	raw_input = new FileReader * [m_number_of_detectors];
	ReadTriggerFile(f_trg_in);
	coinc_group = new SpatialClusterer * [m_number_of_detectors];


	for(short i = 0; i < m_number_of_detectors; i++){
		if( m_data_format == ROOT_FILE ){
			//raw_input[i] = new RootFileReader(f_in.at(i));
			cout << "INPUT FORMAT NO LONGER SUPPORTED" << endl;
			exit(-1);
		}
		else if( m_data_format == ASCII_T3PA ){
			raw_input[i] = new AsciiFileReader(f_in.at(i));
		}
		else if( m_data_format == ASCII_KATHRINE ){
			//~ raw_input[i] = new KathrineFileReaderFast(f_in.at(i));
			raw_input[i] = new KathrineFileReader(f_in.at(i));
		}
		else{
			cerr << "INPUT FILE FORMAT NOT SUPPORTED" << endl;
			exit(-1);
		}
		
		coinc_group[i] = new SpatialClusterer();
		raw_input[i]->SetTimeOffset(to.at(i));
		m_root_eof[i] = false;
	}

	cluster_output = new RootOutputCreator(f_out, m_number_of_detectors, m_use_ecali);
	cluster_output->SetTriggerWindow(trg_w);
	coinc_analyser = new CoincidenceModule(m_number_of_detectors);
	cluster_output->SaveAnalysisDescription(tw, to);
}

void AnalysisAction::Initialize(const string& fn, const string& f_trg_in, double &trg_w, const string& f_out, const string& config_file, double& tw, const string& suffix)
{
	cout << "Read detector configuration from external file" << endl;
	_detector_configuration.ReadConfigurationFromFile(config_file);

	const int n_det = _detector_configuration.GetNumberOfSensors();
	const short n_layers = _detector_configuration.GetNumberOfLayers();
	m_number_of_detectors = (short) _detector_configuration.GetNumberOfSensors();
	m_number_of_layers = n_layers;
	
	if (n_layers > 1) { m_coincidence_module = true; }

	m_root_eof.resize(m_number_of_detectors);

	//raw_input.resize(m_number_of_detectors);
	//coinc_group.resize(m_number_of_detectors);

	raw_input = new FileReader * [n_det];
	ReadTriggerFile(f_trg_in);
	coinc_group = new SpatialClusterer * [n_layers];

	vector<sensor_description> vec = _detector_configuration.GetDetectorConfiguration();
	
	for (int i = 0; i < n_det; i++) {
		string file((fn + vec[i].id+suffix+".txt").c_str());
		cerr << "File name: " << file << endl;

		switch (m_data_format) {
			case ROOT_FILE:{
				cout << "Input file format no longer supported!" << endl; exit(-1); 
				break;
			}
			case ASCII_T3PA:{
				raw_input[i] = new AsciiFileReader(file);
				break;
			}
			case ASCII_KATHRINE:{
				raw_input[i] = new KathrineFileReader(file); 
				break;
			}
			default:{
				cerr << "Input file format not known!" << endl; exit(-1);
			}
		}
	}

	for (int i = 0; i < n_det; i++) {
		raw_input[i]->SetLayer(vec[i].layer);
		raw_input[i]->SetTimeOffset(vec[i].time_offset);

		for (size_t j = 0; j < vec[i].rotation.size(); ++j)
			raw_input[i]->SetRotationAxisAngle(vec[i].rotation[j].first, vec[i].rotation[j].second);
	
		if(vec[i].translation.first != 0 ||  vec[i].translation.second != 0)
			raw_input[i]->SetTranslationXY(vec[i].translation.first, vec[i].translation.second);

		if(vec[i].pixel_pitch_gap_xy.first != 0 ||  vec[i].pixel_pitch_gap_xy.second != 0)
			raw_input[i]->SetPixelPitchGapXY(vec[i].pixel_pitch_gap_xy.first, vec[i].pixel_pitch_gap_xy.second);

		if (vec[i].calib_dir != "") {
			raw_input[i]->LoadCalibrationFiles(vec[i].calib_dir);
			m_use_ecali = true;
		}

		if (vec[i].tw_file != ""){
			ReadTimewalkCorrectionFile(vec[i].tw_file, raw_input[i]);
		}

		if (vec[i].mask_file != "")
			raw_input[i]->LoadMaskedPixelList(vec[i].mask_file);
		
		if (vec[i].toa_column_corr_file != "")
			raw_input[i]->LoadColumnwiseToaCorrectionFile(vec[i].toa_column_corr_file);

		m_root_eof[i] = false;
	}

	for(short j = 0; j < n_layers; ++j)
		coinc_group[j] = new SpatialClusterer();

	cluster_output = new RootOutputCreator(f_out, m_number_of_detectors, m_use_ecali);
	cluster_output->SetTriggerWindow(trg_w);
	coinc_analyser = new CoincidenceModule(m_number_of_detectors);
	cluster_output->SaveAnalysisDescription(tw, vec);
	cluster_output->SetEnergyCali(m_use_ecali);
}

Long64_t ii;

void AnalysisAction::Run(){

	//Initialize the raw data reader!
	for(int i = 0; i < m_number_of_detectors; ++i){
		raw_input[i]->Initialize();		
		cluster_output->SetStartTime( raw_input[i]->GetStartTime() , i );
	}

	Long64_t entries = 0;
	ii = 0;

	for(int i = 0; i < m_number_of_detectors; i++){
		entries += raw_input[i]->GetEntries();
	}

	bool input_files_eof;
	short detector = GetClosestEntry(raw_input, input_files_eof);
	double toa_temp = raw_input[detector]->GetToA();

	while( !input_files_eof ){
		detector = GetClosestEntry(raw_input,input_files_eof);

		if( input_files_eof || (ii > 0 && fabs(raw_input[detector]->GetToA() - toa_temp) > m_time_window) ){
			FillOutputDataContainer();
			if(detector != -1){ toa_temp = raw_input[detector]->GetToA(); }
		}
		else{
			coinc_group[raw_input[detector]->GetLayer()]->AddPixel( raw_input[detector]->GetPixelData() );
			//coinc_group[detector]->AddPixel( raw_input[detector]->GetPixelData() );
			toa_temp = raw_input[detector]->GetToA();

			m_root_eof[detector] = raw_input[detector]->LoadNextEntry();
			
			if( m_root_eof[detector] ){ cerr << "eof flag reached" << endl; }
			ii++;
		}
		
		float progress = (float) ii / entries;
		if((int) (progress*100) % 5 == 0)
		{
			cout<<"\rProgress: "<< (int) (progress * 100.) << " %" << std::flush;
		}
	}
}

void AnalysisAction::FillOutputDataContainer(){
	_coincident_clusters.clear();

	for(int i = 0; i < m_number_of_layers; i++){
		coinc_group[i]->FindClusters();

		_vec.clear();
		_vec = coinc_group[i]->GetClusterList();
		
		//Here probably change needed ... 
		for ( size_t j = 0; j < _vec.size(); ++j ){
			_vec[j].SetLayer( i+1 ); // layer = i+1 :-) -> Layer 1 + Layer 2
		}
		
		_coincident_clusters.insert( std::end( _coincident_clusters ) , std::begin( _vec ), std::end( _vec ) );
	}
	
	sort( _coincident_clusters.begin(), _coincident_clusters.end() , Cluster::SortByMinToA);

	if (m_coincidence_module && _coincident_clusters.size() > 0) {
		coinc_analyser->AddCoincidenceGroup(_coincident_clusters);
		coinc_analyser->EvaluateCoincidence();
	}
	
	for( size_t i = 0; i < _coincident_clusters.size(); ++i ){
		cluster_output->AddCluster(_coincident_clusters[i],trigger_list);

		if( m_coincidence_module ){
			cluster_output->AddCoincidentNumber(coinc_analyser->GetCoincNo());
		}

		cluster_output->SetCoincGroupSize(_coincident_clusters.size());
		cluster_output->FillTree();
	}
	
	//******** FINALIZE COINCIDENCE SEARCH **********//
	if(m_coincidence_module){
		coinc_analyser->FillTree();
	}
	cluster_output->NextCoincidenceGroupNotification();
}

void AnalysisAction::Finalize()
{
	cerr << "finalize" << endl;
	if( m_coincidence_module ){
		//cerr << "coincidence module" << endl;
		//cluster_output->SaveAdditionalResults(coinc_analyser->GetResults());
	}
	if( m_data_format == ASCII_T3PA || m_data_format == ASCII_KATHRINE){
		for(int i = 0; i < m_number_of_detectors; ++i){
			cerr << "layer info" << endl;
			stringstream treename; 
			treename << "InfoTree_layer" << i;
			cluster_output->SaveAdditionalResults( raw_input[i]->GetDscData(), treename.str() );
		}
	}
	cluster_output->WriteToDisk();

	cerr << "test finalize" << endl;
}

bool AnalysisAction::SetToABugCorrectionMap(vector<std::string> &files)
{
	for( size_t i = 0; i < files.size(); i++){
		raw_input[i]->LoadColumnwiseToaCorrectionFile(files.at(i));
	}
	return true;
}

void AnalysisAction::SetEnergyCalibration(vector<std::string> &dir)
{
	for(size_t i = 0; i < dir.size(); i++){
		raw_input[i]->LoadCalibrationFiles(dir.at(i));
	}
	m_use_ecali = true;
	cluster_output->SetEnergyCali(m_use_ecali);
}

void AnalysisAction::LoadPixelMasks( vector<string> &files ){
	for( size_t i = 0; i < files.size(); ++i ){
		raw_input[i]->LoadMaskedPixelList( files.at(i) );
	}
}

short AnalysisAction::GetClosestEntry(AsciiFileReader** rr, bool &eof)
{
	eof = false;
	vector<pair<short,double>> items;

	for(int i = 0; i < m_number_of_detectors; ++i)
	{	
		if(!m_root_eof[i]){ items.push_back(std::make_pair(i,rr[i]->GetToA())); }
	}

	if(items.size() == 0)
	{
		eof = true;
		return -1;
	}

	//See: http://stackoverflow.com/questions/279854/how-do-i-sort-a-vector-of-pairs-based-on-the-second-element-of-the-pair
	std::sort(items.begin(), items.end(), 
			boost::bind(&std::pair<short, double>::second, _1) <
			boost::bind(&std::pair<short, double>::second, _2));
	
	short detector = items.at(0).first;
	return detector;
}

short AnalysisAction::GetClosestEntry(vector<AsciiFileReader*> rr, bool& eof)
{
	eof = false;
	vector<pair<short, double>> items;

	for (int i = 0; i < m_number_of_detectors; ++i)
	{
		if (!m_root_eof[i]) { items.push_back(std::make_pair(i, rr[i]->GetToA())); }
	}

	if (items.size() == 0)
	{
		eof = true;
		return -1;
	}

	//See: http://stackoverflow.com/questions/279854/how-do-i-sort-a-vector-of-pairs-based-on-the-second-element-of-the-pair
	std::sort(items.begin(), items.end(),
		boost::bind(&std::pair<short, double>::second, _1) <
		boost::bind(&std::pair<short, double>::second, _2));

	short detector = items.at(0).first;
	return detector;
}

short AnalysisAction::GetClosestEntry(FileReader** rr, bool &eof)
{
	eof = false;
	vector<pair<short,double>> items;

	for(int i = 0; i < m_number_of_detectors; ++i)
	{	
		if(!m_root_eof[i]){ items.push_back(std::make_pair(i,rr[i]->GetToA())); }
	}

	if(items.size() == 0)
	{
		cerr << "Finished: Analysis Action!" << endl;
		eof = true;
		return -1;
	}

	std::sort(items.begin(), items.end(), 
			boost::bind(&std::pair<short, double>::second, _1) <
			boost::bind(&std::pair<short, double>::second, _2));
	
	short detector = items.at(0).first;
	return detector;
}

short AnalysisAction::GetClosestEntry(vector<FileReader*> rr, bool& eof)
{
	eof = false;
	vector<pair<short, double>> items;

	for (int i = 0; i < m_number_of_detectors; ++i)
	{
		if (!m_root_eof[i]) { items.push_back(std::make_pair(i, rr[i]->GetToA())); }
	}

	if (items.size() == 0)
	{
		cerr << "Finished: Analysis Action!" << endl;
		eof = true;
		return -1;
	}

	std::sort(items.begin(), items.end(),
		boost::bind(&std::pair<short, double>::second, _1) <
		boost::bind(&std::pair<short, double>::second, _2));

	short detector = items.at(0).first;
	return detector;
}

void AnalysisAction::ReadTimewalkCorrectionFile(string fn, FileReader* fr) {
	float a1 = 0;
	float a2 = 0;
	float a3 = 0;
	float a4 = 0;

	std::ifstream in;
	in.open(fn.c_str(), ios::in);
	if (!in.good()) { cerr << "WARNING: Timewalk coefficients not found!!" << endl; }

	string line;
	while (getline(in, line)) {
		if (line[0] == 'a') {
			line = line.substr(2);
			a1 = (float) atof(line.c_str());
		}
		else if (line[0] == 'b') {
			line = line.substr(2);
			a2 = (float) atof(line.c_str());
		}
		else if (line[0] == 'c') {
			line = line.substr(2);
			a3 = (float) atof(line.c_str());
		} 
		else if (line[0] == 'd') {
			line = line.substr(2);
			a4 = (float) atof(line.c_str());
		}
		else {
			std::stringstream ss(line);
			ss >> a1 >> a2 >> a3 >> a4;
		}
	}
	in.close();

	fr->SetTimewalkCorrectionParams(a1, a2, a3, a4);
	fr->PrintTimewalkCorrectionParams();
}

void AnalysisAction::ReadTriggerFile(string fn) {

	if(fn == "") {return;}
	trigger_list.clear();

	std::ifstream in;
	in.open(fn.c_str(), ios::in);
	if (!in.good()) { cerr << "WARNING: Trigger File not found!!" << endl; }

	string line;
	string delimiter = "\t";

	int triggerID = 0;
	pair<double,int> hold_trigger;
	bool hold_trig = false;
	while (getline(in, line)) {
		if (line.find("#")  != string::npos) {continue;} //ignoring all commented lines

		//gpio id
    	int gpio_id = stoi(line.substr(0, line.find(delimiter)));
    	if(line.find(delimiter)!=string::npos) {line.erase(0, line.find(delimiter) + delimiter.length());}
    	else{line.erase(line.begin(),line.end());}

    	//trigger time
    	double t_tgr = stod(line.substr(0, line.find(delimiter)));
    	if(line.find(delimiter)!=string::npos) {line.erase(0, line.find(delimiter) + delimiter.length());}
    	else{line.erase(line.begin(),line.end());}

    	bool start_master, start_slave, minipan_trigger, busy_trigger, scint_trigger;

    	//trigger ID
    	if(!line.empty()){
    		triggerID = stoi(line.substr(0, line.find(delimiter)));
    		if(line.find(delimiter)!=string::npos) {line.erase(0, line.find(delimiter) + delimiter.length());}
    		else{line.erase(line.begin(),line.end());}

			start_master    = bool((gpio_id>>0) & 0x1);
    		start_slave     = bool((gpio_id>>1) & 0x1);
    		minipan_trigger = bool((gpio_id>>2) & 0x1);
    		busy_trigger    = bool((gpio_id>>3) & 0x1);
    		scint_trigger   = bool((gpio_id>>4) & 0x1);

    		if(minipan_trigger){
    			hold_trigger = make_pair(t_tgr,triggerID);
    			hold_trig = true;
    		}

    	} else {
			
			start_master  	= ((gpio_id>>0) & 0x1);
    		scint_trigger   = ((gpio_id>>1) & 0x1);
    		busy_trigger    = ((gpio_id>>2) & 0x1);
    		minipan_trigger = ((gpio_id>>3) & 0x1);
    		start_slave   	= ((gpio_id>>4) & 0x1);

    		if(minipan_trigger){
    			hold_trigger = make_pair(t_tgr,triggerID);
    			triggerID++;
    			hold_trig = true;
    		}

    	}

    	if(hold_trig){
    		trigger_list.push_back(hold_trigger);
    		hold_trig = false;
    	}

    	if(!start_master && !scint_trigger && !minipan_trigger && !start_slave && !busy_trigger) {
    		cout << "GPIO board ID " << gpio_id << " not recognized." << endl;
    	}
	}

	in.close();

	cout << "Finished Reading Trigger File. " << trigger_list.size() << " triggers found!" << endl;
}

void AnalysisAction::SetTimewalkCorrectionParams(vector<std::string> &f){
	for (size_t i = 0; i < f.size(); i++) {
		ReadTimewalkCorrectionFile(f[i], raw_input[i]);
	}
}

void AnalysisAction::SetMatrixClocks(vector<int> &mc) {
	for (size_t i = 0; i < mc.size(); ++i) {
		raw_input[i]->SetMatrixClock(mc.at(i));
	}
}

void AnalysisAction::SetMatrixClockRescale(vector<double> &rs) {
	for (size_t i = 0; i < rs.size(); ++i) {
		raw_input[i]->SetMatrixClockRescale(rs.at(i));
	}
}

void AnalysisAction::SetPixelPitch(vector<int> &pp) {
	for ( size_t i = 0; i < pp.size(); ++i ) {
		raw_input[i]->SetPixelPitch(pp.at(i));
	}
}