#include <iostream>
#include "CoincidenceGroup.h"

using namespace std;

CoincidenceGroup::CoincidenceGroup(){
	pixel = new pixel_data[ARRAY_LENGTH];
	coincGroupSize = 0;
}

CoincidenceGroup::~CoincidenceGroup(){
	delete[] pixel;
}

void CoincidenceGroup::Fill(pixel_data pix){
	pixel[coincGroupSize] = pix;
	coincGroupSize++;
}

pixel_data CoincidenceGroup::GetEntry(int i){
	if(i > ARRAY_LENGTH){
		cerr << "Coincidence Group FATAL ERROR: Accessing Memory Out of Range" << endl;
		exit(-1);
	}
	pixel_data pix = pixel[i];
	return pix;
}