#include <iostream>
#include <cmath>
#include <cstring>
#include <memory.h>
#include <algorithm>
#include <iomanip>

#include "SpatialClusterer.h"

using namespace std;

Cluster::Cluster() {
	ClearCluster();
}

void Cluster::AddPixel( const pixel_data &pix ){
	m_vec_pixels.push_back( pix );
	
	if( m_min_toa == -1 ||  pix.toa < m_min_toa ){ m_min_toa = pix.toa; }
}

void Cluster::ClearCluster() { 
	m_vec_pixels.clear(); 

	m_layer						=  1;
	m_min_toa					= -1;
	_max_toa					= NAN;
	_cluster_volume				= 0;
	_cluster_volume_keV			= NAN;
	_cluster_height				= -1;
	_cluster_height_keV			= NAN;
	_cluster_mean_x				= NAN;
	_cluster_mean_y				= NAN;
	_cluster_mean_x_mm			= NAN;
	_cluster_mean_y_mm			= NAN;
	_cluster_rms_x				= NAN;
	_cluster_rms_y				= NAN;
	_cluster_volume_centroid_x	= NAN;
	_cluster_volume_centroid_y	= NAN;
}

const float& Cluster::GetVolume_keV()
{
	if (!isnan(_cluster_volume_keV))
		return _cluster_volume_keV;

	GetVolume_keV(_cluster_volume_keV);
	return _cluster_volume_keV;
}

void Cluster::GetVolume_keV(float& sum)
{
	sum = 0;
	for_each(m_vec_pixels.begin(), m_vec_pixels.end(),
		[&sum](const pixel_data& elem) {
		sum = sum + elem.tot_keV;
	});
}

const float& Cluster::GetHeight_keV()
{
	if (!isnan(_cluster_height_keV))
		return _cluster_height_keV;

	return _cluster_height_keV = max_element(m_vec_pixels.begin(), m_vec_pixels.end(),
		[](const pixel_data& a, const pixel_data& b) {return a.tot_keV < b.tot_keV; })->tot_keV;
}

const unsigned long long& Cluster::GetVolume_ToT()
{
	if (_cluster_volume != 0)
		return _cluster_volume;
	
	GetVolume_ToT(_cluster_volume);
	return _cluster_volume;
}

void Cluster::GetVolume_ToT(unsigned long long& sum)
{
	sum = 0;
	for_each(m_vec_pixels.begin(), m_vec_pixels.end(),
		[&sum](const pixel_data& elem) {
		sum = sum + elem.tot;
	});
}

const int& Cluster::GetHeight_ToT()
{
	if (_cluster_height != -1) {
		return _cluster_height;
	}

	return _cluster_height = max_element(m_vec_pixels.begin(), m_vec_pixels.end(),
		[](const pixel_data& a, const pixel_data& b) {return a.tot < b.tot; })->tot;
}

const double& Cluster::GetMaxToA()
{
	if (!isnan(_max_toa))
		return _max_toa;

	return _max_toa = max_element(m_vec_pixels.begin(), m_vec_pixels.end(), 
		[](const pixel_data& a, const pixel_data& b) {return a.toa < b.toa; })->toa;
}

void Cluster::GetSumEnergyWeightedCoordinates(float &sum_x, float& sum_y, float& sum_x_mm, float& sum_y_mm)
{
	sum_x = 0;
	sum_y = 0;
	
	sum_x_mm = 0;
	sum_y_mm = 0;

	for (auto it = m_vec_pixels.begin(); it != m_vec_pixels.end(); ++it) {
		sum_x += (it->pixel_x * it->tot_keV);
		sum_y += (it->pixel_y * it->tot_keV);

		sum_x_mm += (it->pixel_x_mm * it->tot_keV);
		sum_y_mm += (it->pixel_y_mm * it->tot_keV);
	}
}

void Cluster::GetSumToTWeightedCoordinates(float& sum_x, float& sum_y, float& sum_x_mm, float& sum_y_mm)
{
	sum_x = 0;
	sum_y = 0;

	sum_x_mm = 0;
	sum_y_mm = 0;

	for (auto it = m_vec_pixels.begin(); it != m_vec_pixels.end(); ++it) {
		sum_x += (it->pixel_x * it->tot);
		sum_y += (it->pixel_y * it->tot);

		sum_x_mm += (it->pixel_x_mm * it->tot);
		sum_y_mm += (it->pixel_y_mm * it->tot);
	}
}

void Cluster::GetBinarySumCoordinates(float& sum_x, float& sum_y, float& sum_x_mm, float& sum_y_mm)
{
	sum_x = 0;
	sum_y = 0;

	sum_x_mm = 0;
	sum_y_mm = 0;

	for (auto it = m_vec_pixels.begin(); it != m_vec_pixels.end(); ++it) {
		sum_x += it->pixel_x;
		sum_y += it->pixel_y;

		sum_x_mm += it->pixel_x_mm;
		sum_y_mm += it->pixel_y_mm;
	}
}

void Cluster::GetBinarySquareSumCoordinates(float& sum_x, float& sum_y, float& sum_x_mm, float& sum_y_mm)
{
	sum_x = 0;
	sum_y = 0;

	sum_x_mm = 0;
	sum_y_mm = 0;

	for (auto it = m_vec_pixels.begin(); it != m_vec_pixels.end(); ++it) {
		sum_x += it->pixel_x*it->pixel_x;
		sum_y += it->pixel_y*it->pixel_y;

		sum_x_mm += it->pixel_x_mm*it->pixel_x_mm;
		sum_y_mm += it->pixel_y_mm*it->pixel_y_mm;
	}
}

/************ SPATIAL CLUSTERER *********/
SpatialClusterer::SpatialClusterer(){
	m_total_size = 0;
	m_idx = 0;
}

SpatialClusterer::SpatialClusterer(const SpatialClusterer &c){
	m_idx = c.m_idx;
	m_total_size = c.m_total_size;

	m_cluster_list = c.m_cluster_list;
	m_pixel_list = c.m_pixel_list;
	_cluster_temp = c._cluster_temp;
}

SpatialClusterer::~SpatialClusterer(){}

bool SpatialClusterer::IsNeighbor(pixel_data &p1, pixel_data &p2){
	// 8-fold neighbor!!
	if( std::fabs( (float) (p1.pixel_x - p2.pixel_x) ) <= 1 && 
		std::fabs( (float) (p1.pixel_y - p2.pixel_y) ) <= 1 ){
		return true;
	}
	else{
		return false;
	}
}
	
int SpatialClusterer::FindClusters() {
	m_cluster_list.clear();
	m_total_size = (int)m_pixel_list.size();
	m_idx = 0;

	_cluster_temp.ClearCluster();
	
	if (m_pixel_list.size() == 0) {
		return (int)m_cluster_list.size();
	}


	_cluster_temp.AddPixel(m_pixel_list[m_pixel_list.size() - 1]);
	m_pixel_list.pop_back();
	
	//CURRENT PIXEL IS ALWAYS: m_cluster_array[m_idx]
	while( 1 ){
		if( m_idx == _cluster_temp.GetSize() || m_pixel_list.size() == 0 ){
			m_cluster_list.push_back( _cluster_temp );
			_cluster_temp.ClearCluster();
			
			m_idx = 0;
			
			if( m_pixel_list.size() ==  0 ) break;
			
			_cluster_temp.AddPixel(m_pixel_list.at(m_pixel_list.size()-1));
			m_pixel_list.pop_back();
		}
		
		//ITERATE THROUGH THE REMAINING CLUSTER
		size_t i = 0;
		while( i < m_pixel_list.size() ){
			
			if( IsNeighbor( _cluster_temp.GetPixel(m_idx), m_pixel_list.at(i) ) ){
				_cluster_temp.AddPixel( m_pixel_list.at(i) );
				m_pixel_list.erase( m_pixel_list.begin() + i );
			}
			else{
				i++;
			}
		}
		m_idx++;
	}
	//~ sort( m_cluster_list.begin(), m_cluster_list.end(),  Cluster::SortByMinToA);
	return (int) m_cluster_list.size();
}
