#include <omp.h>

#include "RootOutputCreator.h"

using namespace std;

RootOutputCreator::RootOutputCreator(const string &fn, const short &ndet, const bool &ecali)
{
	m_fout = new TFile(fn.c_str(), "RECREATE");
	m_tout = new TTree("clusteredData", "clustered data");
	m_tdsc = new TTree("analysisDescription", "description of values used for clustering");

	m_number_of_detectors = ndet;

	m_time_offset.resize(m_number_of_detectors);
	m_start_time.resize(m_number_of_detectors);
	std::fill(m_time_offset.begin(), m_time_offset.end(), 0);
	std::fill(m_start_time.begin(), m_start_time.end(), 0);
	
	std::fill(m_pix_x.begin(), m_pix_x.end(), 0);
	std::fill(m_pix_y.begin(), m_pix_y.end(), 0);
	std::fill(m_pix_x_mm.begin(), m_pix_x_mm.end(), 0);
	std::fill(m_pix_y_mm.begin(), m_pix_y_mm.end(), 0); 
	std::fill(m_toa.begin(), m_toa.end(), 0);
	std::fill(m_tot.begin(), m_tot.end(), 0);
	std::fill(m_tot_keV.begin(), m_tot_keV.end(), 0);

	m_time_slice_counter = 0;
	m_clstrSize = 0;
	_use_ecali = ecali;

	CreateClusterBranches();
	CreateDSCBranches();
	
	m_coincNo = -1;
	m_clstrVolume_keV = 0;
	m_clstrVolume_ToT = 0;
	m_clstrHeight_ToT = 0;

	m_clstrMeanX = 0, m_clstrMeanY = 0, m_clstrStdX = 0, m_clstrStdY = 0;
	m_clstrMeanX_mm = 0, m_clstrMeanY_mm = 0, m_clstrStdX_mm = 0, m_clstrStdY_mm = 0;

	m_triggerID = 0;
	m_trigger_time_s = -1;

	trigger_list_counter = 0;
}

RootOutputCreator::~RootOutputCreator()
{
	m_fout->Close();
	delete m_fout;
}

void RootOutputCreator::CreateClusterBranches()
{
	m_tout->Branch("clstrSize",					&m_clstrSize,				"clstrSize/I");
	m_tout->Branch("PixX",						m_pix_x.data(),				"PixX[clstrSize]/S");
	m_tout->Branch("PixY",						m_pix_y.data(),				"PixY[clstrSize]/S");
	m_tout->Branch("PixX_mm",					m_pix_x_mm.data(),			"PixX_mm[clstrSize]/F");
	m_tout->Branch("PixY_mm",					m_pix_y_mm.data(),			"PixY_mm[clstrSize]/F");
	m_tout->Branch("ToA",						m_toa.data(),				"ToA[clstrSize]/D");
	m_tout->Branch("ToT",						m_tot.data(),				"ToT[clstrSize]/I");
	m_tout->Branch("coincidence_group",			&m_time_slice_counter,		"coincidence_group/l");
	m_tout->Branch("coincidence_group_size",	&m_coincidence_group_size,	"coincidence_group_size/S");
	m_tout->Branch("clstrVolume_ToT",			&m_clstrVolume_ToT,			"clstrVolume_ToT/l");
	m_tout->Branch("clstrHeight_ToT",			&m_clstrHeight_ToT,			"clstrHeight_ToT/I");
	m_tout->Branch("delta_ToA",					&m_delta_ToA,				"delta_ToA/D");
	m_tout->Branch("min_ToA",					&m_min_ToA,					"min_ToA/D");
	m_tout->Branch("clstrMeanX",				&m_clstrMeanX,				"clstrMeanX/F");
	m_tout->Branch("clstrMeanY",				&m_clstrMeanY,				"clstrMeanY/F");
	m_tout->Branch("clstrStdX",					&m_clstrStdX,				"clstrStdX/F");
	m_tout->Branch("clstrStdY",					&m_clstrStdY,				"clstrStdY/F");
	m_tout->Branch("clstrMeanX_mm",				&m_clstrMeanX_mm,			"clstrMeanX_mm/F");
	m_tout->Branch("clstrMeanY_mm",				&m_clstrMeanY_mm,			"clstrMeanY_mm/F");
	m_tout->Branch("clstrStdX_mm",				&m_clstrStdX_mm,			"clstrStdX_mm/F");
	m_tout->Branch("clstrStdY_mm",				&m_clstrStdY_mm,			"clstrStdY_mm/F");
	m_tout->Branch("clstrVolCentroidX",			&m_clstrVolCentroidX,		"clstrVolCentroidX/F");
	m_tout->Branch("clstrVolCentroidY",			&m_clstrVolCentroidY,		"clstrVolCentroidY/F");
	m_tout->Branch("clstrVolCentroidX_mm",		&m_clstrVolCentroidX_mm,	"clstrVolCentroidX_mm/F");
	m_tout->Branch("clstrVolCentroidY_mm",		&m_clstrVolCentroidY_mm,	"clstrVolCentroidY_mm/F");
	m_tout->Branch("abs_start_time_s",			&m_absolute_time_s,			"abs_start_time_s/D");
	m_tout->Branch("isTriggered", 				&m_isTriggered,				"isTriggered/B");
	m_tout->Branch("m_trigger_time_s",			&m_trigger_time_s,			"m_trigger_time_s/D");
	m_tout->Branch("triggerID", 				&m_triggerID,				"triggerID/I");
	
	if( m_number_of_detectors > 1 )
	{
		//layer branch starts at 1: 2 layers are 1 and 2;
		m_tout->Branch("layer", &m_layer, "layer/S");
		m_tout->Branch("coinc_no", &m_coincNo, "coinc_no/L");
	}
}

void RootOutputCreator::CreateDSCBranches()
{
	std::stringstream layer_stream;
	layer_stream << m_number_of_detectors;
	_sw_version = (string("v2.0 - ") + __DATE__).c_str();
	m_tdsc->Branch("Version", &_sw_version);
	m_tdsc->Branch("numberOfLayers",	&m_number_of_detectors,		"numberOfLayers/S");
	m_tdsc->Branch("time_window",		&m_time_window,				"time_window/D");
	m_tdsc->Branch("m_triggerWindow",   &m_triggerWindow,			"triggerWindow/D");
	m_tdsc->Branch("time_offset",		m_time_offset.data(),		("time_offset["+layer_stream.str()+"]/D").c_str());
}

void RootOutputCreator::SaveAnalysisDescription(const double &tw, vector<double>& to)
{
	m_time_window = tw;
	for( int i = 0; i < m_number_of_detectors; i++){
		m_time_offset[i] = to.at(i);
	}
	m_tdsc->Fill();
}

void RootOutputCreator::SaveAnalysisDescription(const double& tw, vector<sensor_description>& to)
{
	m_time_window = tw;
	for (int i = 0; i < m_number_of_detectors; i++) {
		m_time_offset[i] = to[i].time_offset;
	}
	m_tdsc->Fill();
}

void RootOutputCreator::AddPixel(pixel_data &pix){
	if(m_clstrSize == 0){
		m_min_ToA = m_max_ToA = pix.toa;
		m_clstrHeight_keV = pix.tot_keV;
		m_clstrHeight_ToT = pix.tot;

		m_clstrMeanX = pix.pixel_x;
		m_clstrMeanY = pix.pixel_y;
		m_clstrMeanX_mm = pix.pixel_x_mm;
		m_clstrMeanY_mm = pix.pixel_y_mm;

		m_clstrStdX  = pix.pixel_x*pix.pixel_x;
		m_clstrStdY  = pix.pixel_y*pix.pixel_y;
		m_clstrStdX_mm  = pix.pixel_x_mm*pix.pixel_x_mm;
		m_clstrStdY_mm  = pix.pixel_y_mm*pix.pixel_y_mm;

		if(_use_ecali){
			m_clstrVolCentroidX = pix.pixel_x * pix.tot_keV;
			m_clstrVolCentroidY = pix.pixel_y * pix.tot_keV;
			m_clstrVolCentroidX_mm = pix.pixel_x_mm * pix.tot_keV;
			m_clstrVolCentroidY_mm = pix.pixel_y_mm * pix.tot_keV;
		}
		else 
		{
			m_clstrVolCentroidX = pix.pixel_x * pix.tot;
			m_clstrVolCentroidY = pix.pixel_y * pix.tot;
			m_clstrVolCentroidX_mm = pix.pixel_x_mm * pix.tot;
			m_clstrVolCentroidY_mm = pix.pixel_y_mm * pix.tot;
		}
	}
	else{
		if(pix.toa < m_min_ToA){ m_min_ToA = pix.toa;}
		else if(pix.toa > m_max_ToA){ m_max_ToA = pix.toa; }
		
		if(pix.tot_keV > m_clstrHeight_keV){ m_clstrHeight_keV = pix.tot_keV; }
		if(pix.tot > m_clstrHeight_ToT){ m_clstrHeight_ToT = pix.tot; }

		m_clstrMeanX += pix.pixel_x;
		m_clstrMeanY += pix.pixel_y;
		m_clstrMeanX_mm += pix.pixel_x_mm;
		m_clstrMeanY_mm += pix.pixel_y_mm;

		m_clstrStdX  += pix.pixel_x*pix.pixel_x;
		m_clstrStdY  += pix.pixel_y*pix.pixel_y;
		m_clstrStdX_mm  += pix.pixel_x_mm*pix.pixel_x_mm;
		m_clstrStdY_mm  += pix.pixel_y_mm*pix.pixel_y_mm;

		if(_use_ecali){
			m_clstrVolCentroidX += (pix.pixel_x * pix.tot_keV);
			m_clstrVolCentroidY += (pix.pixel_y * pix.tot_keV);
			m_clstrVolCentroidX_mm += (pix.pixel_x_mm * pix.tot_keV);
			m_clstrVolCentroidY_mm += (pix.pixel_y_mm * pix.tot_keV);
		}
		else 
		{
			m_clstrVolCentroidX += (pix.pixel_x * pix.tot);
			m_clstrVolCentroidY += (pix.pixel_y * pix.tot);
			m_clstrVolCentroidX_mm += (pix.pixel_x_mm * pix.tot_keV);
			m_clstrVolCentroidY_mm += (pix.pixel_y_mm * pix.tot_keV);
		}
	}

	m_pix_x[m_clstrSize] = pix.pixel_x;
	m_pix_y[m_clstrSize] = pix.pixel_y;
	
	m_pix_x_mm[m_clstrSize] = pix.pixel_x_mm;
	m_pix_y_mm[m_clstrSize] = pix.pixel_y_mm;
	
	m_tot[m_clstrSize] = pix.tot;
	m_tot_keV[m_clstrSize] = pix.tot_keV;
	m_toa[m_clstrSize] = pix.toa;
	
	m_clstrVolume_keV += pix.tot_keV;
	m_clstrVolume_ToT += pix.tot;
	m_clstrSize++;
}

void RootOutputCreator::AddCluster(Cluster& pix, vector<pair<double,int>> trigger_list)
{

	m_clstrSize = (int)pix.GetSize();
	m_min_ToA = pix.GetMinToA();
	m_max_ToA = pix.GetMaxToA();
	m_delta_ToA = m_max_ToA - m_min_ToA;

	//~ #pragma omp parallel
	//~ {
	for (size_t i = 0; i < pix.GetSize(); ++i) {
		m_pix_x[i] = pix.GetCluster()[i].pixel_x;
		m_pix_x_mm[i] = pix.GetCluster()[i].pixel_x_mm;


	}	// one bit
	//~ }
	//~ {
	for (size_t i = 0; i < pix.GetSize(); ++i) {
		m_pix_y[i] = pix.GetCluster()[i].pixel_y;
		m_pix_y_mm[i] = pix.GetCluster()[i].pixel_y_mm;
	}
	//~ }
	
	//~ #pragma omp parallel
	//~ {
	for (size_t i = 0; i < pix.GetSize(); ++i) {
		m_toa[i] = pix.GetCluster()[i].toa;
	}
	for (size_t i = 0; i < pix.GetSize(); ++i) {
		m_tot[i] = pix.GetCluster()[i].tot;
	}
	//~ }

	if (_use_ecali) {
		for (size_t i = 0; i < pix.GetSize(); ++i) {
			m_tot_keV[i] = pix.GetCluster()[i].tot_keV;
		}
	}

	m_layer = pix.GetLayer();

	pix.GetVolume_ToT(m_clstrVolume_ToT);
	m_clstrHeight_ToT = pix.GetHeight_ToT();

	if (_use_ecali) {
		pix.GetVolume_keV(m_clstrVolume_keV);
		m_clstrHeight_keV = pix.GetHeight_keV();
		
		pix.GetSumEnergyWeightedCoordinates(m_clstrVolCentroidX, m_clstrVolCentroidY,m_clstrVolCentroidX_mm, m_clstrVolCentroidY_mm);
		m_clstrVolCentroidX	/= m_clstrVolume_keV;
		m_clstrVolCentroidY /= m_clstrVolume_keV;
		m_clstrVolCentroidX_mm	/= m_clstrVolume_keV;
		m_clstrVolCentroidY_mm /= m_clstrVolume_keV;
	}
	else {
		pix.GetSumToTWeightedCoordinates(m_clstrVolCentroidX, m_clstrVolCentroidY,m_clstrVolCentroidX_mm, m_clstrVolCentroidY_mm);
		m_clstrVolCentroidX /= m_clstrVolume_ToT;
		m_clstrVolCentroidY /= m_clstrVolume_ToT;
		m_clstrVolCentroidX_mm /= m_clstrVolume_ToT;
		m_clstrVolCentroidY_mm /= m_clstrVolume_ToT;
	}
	
	pix.GetBinarySumCoordinates(m_clstrMeanX, m_clstrMeanY, m_clstrMeanX_mm, m_clstrMeanY_mm);
	m_clstrMeanX /= pix.GetSize();
	m_clstrMeanY /= pix.GetSize();
	m_clstrMeanX_mm /= pix.GetSize();
	m_clstrMeanY_mm /= pix.GetSize();

	pix.GetBinarySquareSumCoordinates(m_clstrStdX, m_clstrStdY, m_clstrStdX_mm, m_clstrStdY_mm);
	m_clstrStdX = sqrt(m_clstrStdX/(pix.GetSize()-1.) - m_clstrMeanX*m_clstrMeanX*pix.GetSize()/(pix.GetSize()-1.));
	m_clstrStdY = sqrt(m_clstrStdY/(pix.GetSize()-1.) - m_clstrMeanY*m_clstrMeanY*pix.GetSize()/(pix.GetSize()-1.));
	m_clstrStdX_mm = sqrt(m_clstrStdX_mm/(pix.GetSize()-1.) - m_clstrMeanX_mm*m_clstrMeanX_mm*pix.GetSize()/(pix.GetSize()-1.));
	m_clstrStdY_mm = sqrt(m_clstrStdY_mm/(pix.GetSize()-1.) - m_clstrMeanY_mm*m_clstrMeanY_mm*pix.GetSize()/(pix.GetSize()-1.));

	m_absolute_time_s = m_min_ToA / 1E9 + m_start_time[m_layer - 1];

	m_isTriggered = false;
	m_trigger_time_s = -1;
	
	for(size_t iTrigg=trigger_list_counter; iTrigg<trigger_list.size(); iTrigg++){

		//check if it is inside the window
		if((m_min_ToA / 1E9> trigger_list[iTrigg].first-5./1E6) && (m_min_ToA / 1E9<(trigger_list[iTrigg].first+m_triggerWindow/1E6))){
			m_triggerID      = trigger_list[iTrigg].second;
			m_trigger_time_s = trigger_list[iTrigg].first;
	  		m_isTriggered = true;
	  		trigger_list_counter = iTrigg;
	  		break;
	  	} else {
		  	m_triggerID = -1;

		  	//break loop if current trigger time is higher than hit time
		  	if(m_min_ToA / 1E9 < (trigger_list[iTrigg].first+m_triggerWindow/1E6)){
		  		trigger_list_counter = iTrigg;
		  		break;
		  	}
	  	}
	  	
	}
	

}

void RootOutputCreator::SaveAdditionalResults(TTree* t)
{
	m_fout->cd();
	t->Write();
}

void RootOutputCreator::SaveAdditionalResults(TTree* t, string name)
{
	m_fout->cd();
	t->Write(name.c_str(), TObject::kOverwrite);
}


void RootOutputCreator::WriteToDisk()
{
	m_fout->cd();
	m_tout->Write("clusteredData", TObject::kOverwrite);
	m_tdsc->Write("analysisDescription", TObject::kOverwrite);
}

void RootOutputCreator::FillTree( short l ){
	m_delta_ToA = m_max_ToA - m_min_ToA;
	m_layer = l;
	m_absolute_time_s = m_min_ToA/1E9 + m_start_time[l-1];

	if(_use_ecali){
		m_clstrVolCentroidX /= m_clstrVolume_keV;
		m_clstrVolCentroidY /= m_clstrVolume_keV;
		m_clstrVolCentroidX_mm /= m_clstrVolume_keV;
		m_clstrVolCentroidY_mm /= m_clstrVolume_keV;
	}
	else 
	{
		m_clstrVolCentroidX /= m_clstrVolume_ToT;
		m_clstrVolCentroidY /= m_clstrVolume_ToT;
		m_clstrVolCentroidX_mm /= m_clstrVolume_ToT;
		m_clstrVolCentroidY_mm /= m_clstrVolume_ToT;
	}
	m_clstrMeanX /= m_clstrSize;
	m_clstrMeanY /= m_clstrSize;
	m_clstrMeanX_mm /= m_clstrSize;
	m_clstrMeanY_mm /= m_clstrSize;

	m_clstrStdX = sqrt(m_clstrStdX/m_clstrSize - m_clstrMeanX*m_clstrMeanX);
	m_clstrStdY = sqrt(m_clstrStdY/m_clstrSize - m_clstrMeanY*m_clstrMeanY);
	m_clstrStdX_mm = sqrt(m_clstrStdX_mm/m_clstrSize - m_clstrMeanX_mm*m_clstrMeanX_mm);
	m_clstrStdY_mm = sqrt(m_clstrStdY_mm/m_clstrSize - m_clstrMeanY_mm*m_clstrMeanY_mm);

	if(m_isTriggered) {m_tout->Fill();}

	//Reset precalculated values
	m_clstrVolume_keV = 0;
	m_clstrVolume_ToT = 0;
	m_clstrSize = 0;
	m_coincNo = -1;

	m_clstrMeanX = 0, m_clstrMeanY = 0, m_clstrStdX = 0, m_clstrStdY = 0;
	m_clstrMeanX_mm = 0, m_clstrMeanY_mm = 0, m_clstrStdX_mm = 0, m_clstrStdY_mm = 0;

	//reset trigger value
	m_isTriggered = false;
}

void RootOutputCreator::FillTree() {
	m_tout->Fill();
	m_coincNo = -1;
}