#include <algorithm>
#include <iomanip>
#include <cstdio>
#include <boost/lexical_cast.hpp>

#include "KathrineFileReader.h"

int convert_string_to_number(string& str, int& res, int i_start = 0)
{
	int i = i_start;
	res = 0;
	for (; str[i] != '\0'; ++i) {
		if (str[i] == ' ' || str[i] == '\t' || str[i] == '\r')
			return i;

		res = res * 10 + str[i] - '0';
	}
	return i;
}

int convert_string_to_number(string& str, long long& res, int i_start = 0)
{
	int i = i_start;
	res = 0;
	for (; str[i] != '\0'; ++i) {
		if (str[i] == ' ' || str[i] == '\t'|| str[i] == '\r')
			return i;

		res = res * 10 + str[i] - '0';
	}
	return i;
}

KathrineFileReader::KathrineFileReader(string &fn){
	m_infile.open(fn, ios::in);

	cerr << fn.c_str() << endl;
	if(!m_infile.is_open()){
		cerr << "NO INPUT FILE WAS FOUND" << endl;
		exit(-1);
	}
	m_filename = fn;

	m_eof = false;
	m_finish = false;
	m_last_line_read = false;
	m_nlines = 0;
	m_start_time_compensation_ns = 0;

	m_dsc_tree = new TTree("InfoTree", "");

	m_data_vector[0].reserve(MAX_CHUNK_LENGTH);
	m_data_vector[1].reserve(MAX_CHUNK_LENGTH);
}

KathrineFileReader::~KathrineFileReader(){
	m_infile.close();
}


bool KathrineFileReader::GetLastLine( string& last_line, std::ifstream& in){
	if( !in.good() ){ return false; }
	
	int original_pos = in.tellg();
	in.seekg( -3, ios_base::end );
	
	bool keep_looking = true;
	while( keep_looking ){
		char ch;
		in.get(ch);
		
		if( (std::size_t) in.tellg() <= 1 ){
			in.seekg(0);
			keep_looking = false;
		}
		else if( ch == '\n' ){
			keep_looking = false;
		}
		else{
			in.seekg( -2, ios_base::cur );
		}
	}
	getline( in, last_line);	

	in.seekg( original_pos, in.beg );
	return true;
}

bool KathrineFileReader::ReadChunkOfData(vector<pixel_data>& vec) {
	cerr << "READ CHUNK OF DATA" << endl;
	string line;
	stringstream data_line;
	bool eof = false;

	if (!m_infile.good() || m_infile.eof()) { return true; }

	size_t s_pos = 0;

	while (!m_infile.eof()) {
		getline(m_infile, line);

		if (line[0] == '#') {
			AnalyseHeaderLine(line);
		}

		if (line[0] != '#' && m_nlines > 0) {

			//data_line.clear();
			//data_line.str(line);
			//data_line >> m_input_data.matrix_idx >> m_input_data.sToA >> m_input_data.fToA >> m_input_data.tot;

			s_pos = line.find_first_of("12345667890");
			if (s_pos == string::npos) {
				cerr << "line " << line << endl;
				continue;
			}
			s_pos = convert_string_to_number(line, m_input_data.matrix_idx, s_pos);
			s_pos = convert_string_to_number(line, m_input_data.sToA, s_pos+1);
			s_pos = convert_string_to_number(line, m_input_data.fToA, s_pos+1);
			s_pos = convert_string_to_number(line, m_input_data.tot, s_pos+1);

			if (m_mask[m_input_data.matrix_idx]) continue;

			m_input_data.pixel_x = m_input_data.matrix_idx % MATRIX_WIDTH;
			m_input_data.pixel_y = m_input_data.matrix_idx / MATRIX_WIDTH;

			if (m_pixel_pitch_scale != 1) {
				m_input_data.pixel_x /= m_pixel_pitch_scale;
				m_input_data.pixel_y /= m_pixel_pitch_scale;
				m_input_data.matrix_idx = m_input_data.pixel_x + MATRIX_WIDTH / m_pixel_pitch_scale * m_input_data.pixel_y;
			}

			m_input_data.toa = m_input_data.sToA * m_toa_clock_scale - m_input_data.fToA * m_ftoa_clock_scale - m_toa_bug_correction[m_input_data.pixel_x];
			m_input_data.toa += m_time_offset;
			//~ m_input_data.toa += m_start_time_compensation_ns;

			//if (m_input_data.toa < 0) {
			//	cerr << line << endl;
			//	cerr << m_input_data.matrix_idx << ", " << m_input_data.sToA << " , " << m_input_data.fToA << " , " << m_input_data.tot << endl;
			//	cerr << endl;
			//}

			m_input_data.tot *= m_matrix_clock_rescale;

			m_input_data.absolute_time_s = m_input_data.toa / 1E9 + m_start_time;

			vec.push_back(m_input_data);
		}
		m_nlines++;
		if (vec.size() >= MAX_CHUNK_LENGTH) { break; }
	}

	if (m_infile.eof()) {
		//vec.pop_back();
		eof = true;
	}

	cerr << "vector size " << vec.size() << endl;
	m_nentries = vec.size();

	std::sort(vec.begin(), vec.end(), pixel_data::compare_by_toa);

	for (size_t i = 0; i < _rotation_commands.size(); ++i) {
		_geometry_manipulation.Rotate(_rotation_commands[i], vec);
	}
	if (_translate == true) {
		_geometry_manipulation.Translate(vec);
	}
	if (m_energy_calibration) {
		_calibrator.calibrate(vec);
	}

	return eof;
}

void KathrineFileReader::AnalyseHeaderLine(string &s){
	if( s.compare(0, 35, "# Start of measurement - unix time:") == 0 ){
		m_dsc_tree->Branch("start_time", &m_start_time, "start_time/D");
		m_start_time = stod( s.substr( s.find_first_of("1234567890") ) );
	}
	else if( s.compare(0, 10, "# Chip ID:") == 0 ){
		m_dsc_tree->Branch("chipboard_id", &m_chipboard_id);
		m_chipboard_id = s.substr( s.find(":")+1 );
	}
	else if(s.compare(0, 21, "# Readout IP address:") == 0 ){
		m_dsc_tree->Branch("readout_ip", &m_readout_ip);
		m_readout_ip = s.substr( s.find(":")+1 );
	}
	else if(s.compare(0, 16, "# Detector mode:") == 0 ){
		m_dsc_tree->Branch("detector_mode", &m_det_mode);
		m_det_mode = s.substr( s.find(":")+1 );
	}
	else if(s.compare(0, 15, "# Bias voltage:") == 0){
		m_dsc_tree->Branch("bias", &m_bias, "bias/D");
		size_t start_pos = s.find_first_of("0123456789");
		size_t end_pos = s.find_first_not_of("0123456789.", start_pos+1);
		m_bias = stod( s.substr( start_pos, end_pos-start_pos ) );
	}
	else if(s.compare(0, 7, "# THL =") == 0){
		m_dsc_tree->Branch("threshold", &m_threshold, "threshold/I");
		size_t start_pos = s.find("=")+1;
		size_t end_pos = s.find("(")-1;
		m_threshold = stoi( s.substr( start_pos, end_pos-start_pos ) );
	}
	else if(s.compare(0, 7, "# DACs:") == 0){
		m_dsc_tree->Branch("dacs", m_dacs, "dacs[18]/S");
		stringstream ss;
		ss <<  s.substr( s.find(":")+2 ) ;
		for(int i = 0; i < 18; ++i){ ss >> m_dacs[i]; }
	}
	else if(s.compare(0, 21, "# Sensor temperature:") == 0){
		m_dsc_tree->Branch("sensor_temperature", &m_sensor_temp, "sensor_temperature/F");
		size_t start_pos = s.find(":")+1;
		size_t end_pos = s.find_first_not_of("1234567890.", start_pos+1);
		m_sensor_temp = stof( s.substr(start_pos, end_pos - start_pos) );
	}
	else if(s.compare(0,22, "# Readout temperature:") == 0){
		m_dsc_tree->Branch("readout_temperature", &m_readout_temp, "readout_temp/F");
		size_t start_pos = s.find(":")+1;
		size_t end_pos = s.find_first_not_of("1234567890.", start_pos+1);
		m_readout_temp = stof( s.substr(start_pos, end_pos - start_pos) );
	}
	else if(s.compare(0,8, "# Frame:") == 0 && !m_last_line_read ){
		m_last_line_read = true;
		m_dsc_tree->Branch("relative_start_time", &m_relative_start_time, "relative_start_time/D");
		m_dsc_tree->Branch("relative_end_time", &m_relative_end_time, "relative_end_time/D");
		m_dsc_tree->Branch("lost_hits", &m_lost_hits, "lost_hits/I");

		size_t start_pos = s.find("Start time:")+12;
		start_pos = s.find_first_of("0123456789", start_pos);
		size_t end_pos = s.find_first_not_of("1234567890.E+-", start_pos+1);
		m_relative_start_time = stod( s.substr(start_pos, end_pos - start_pos) );
		if (m_matrix_clock != 40) { m_relative_start_time = 0; }
		
		start_pos = s.find("End time:")+10;
		start_pos = s.find_first_of("0123456789", start_pos);
		end_pos = s.find_first_not_of("1234567890.E-+", start_pos+1);
		m_relative_end_time = stod( s.substr(start_pos, end_pos - start_pos) );
		
		start_pos = s.find("Lost Hits:")+11;
		end_pos = s.find_first_not_of("1234567890.E+-", start_pos+1);
		m_lost_hits = stoi( s.substr(start_pos, end_pos - start_pos) );
		
		m_dsc_tree->Branch("start_time_compensation_ns", &m_start_time_compensation_ns, "m_start_time_compensation_ns/D");
		m_start_time_compensation_ns = (5.0E-8 - m_relative_start_time)/1.0E-9; // if 50 ns no offset has to be added; if 25 -> (-25 ns) have to be added to ToA;
	}
}

void KathrineFileReader::Initialize(){
	//cerr << "MATRIX clock used: " << m_matrix_clock << endl;
	//cerr << "MATRIX clock rescaled: " << m_matrix_clock * m_matrix_clock_rescale << endl;

	m_toa_clock_scale = 1E9 / 1E6 / m_matrix_clock;
	if (m_matrix_clock == 40) {
		m_ftoa_clock_scale = 1E9 / 1E6 / FTOA_CLOCK_MHZ;
	}
	else {
		m_ftoa_clock_scale = 0;
	}

	//cerr << "Clock scale (ToA, fToA): " << m_toa_clock_scale << ", " << m_ftoa_clock_scale << endl;
	//cerr << "Matrix clock ToT rescale factor: " << m_matrix_clock_rescale << endl;
 //
	//cerr << "Pixel Pitch: " << m_pixel_pitch << ", Pixel Pitch Scale: " << m_pixel_pitch_scale << endl;

	string line;
	GetLastLine( line, m_infile ) ;
	AnalyseHeaderLine(line);

	//cerr << "LAST LINE FOUND: " << line << endl;
	//cerr << "Start_time_compensation: " << m_start_time_compensation_ns << endl;
	//do hard coding to set start time compensation to "0" when using different matrix clock
	//if (m_matrix_clock != 40) m_start_time_compensation_ns = 50;
	//cerr << "Start_time_compensation: " << m_start_time_compensation_ns << endl;

	for( int i = 0; i < 2; ++i ){
		m_eof = ReadChunkOfData( m_data_vector[i] );
		m_current_position[i] = 0;
	}

	m_active_stream = 0;
	m_passive_stream = 1;
	m_active_vector = 0;
	m_current_position[0]++;
	
	if( m_data_vector[1].size() == 0 ){ m_finish = true; }
	else{ 
		m_overlap_time = m_data_vector[1].at(0).toa;
		TimeOverlapRangeCheck( m_data_vector[m_active_stream], m_data_vector[m_passive_stream] );	
	}
	
	m_dsc_tree->Branch("matrix_clock_MHz", &m_matrix_clock, "matrix_clock_MHz/I");
	m_dsc_tree->Branch("matrix_clock_rescale", &m_matrix_clock_rescale, "matrix_clock_rescale/D");
	m_dsc_tree->Fill();
}

bool KathrineFileReader::LoadNextEntry(){	
	if( m_current_position[m_active_vector] == m_data_vector[m_active_vector].size() ){	
		if( m_finish ){ return true; }
		
		if( m_eof ){ m_finish = true; }
		else{
			m_data_vector[m_active_vector].clear();
			m_eof = ReadChunkOfData(m_data_vector[m_active_vector]);
			m_overlap_time = m_data_vector[m_active_vector].at(0).toa;
		}

		m_current_position[m_active_vector] = 0;

		if( m_active_stream == 0 ){ 
			m_active_stream = 1; 
			m_passive_stream = 0;
		}
		else{ 
			m_active_stream = 0; 
			m_passive_stream = 1;
		}
		m_active_vector = m_active_stream;
				
		if( !m_eof ){
			TimeOverlapRangeCheck( m_data_vector[m_active_stream], m_data_vector[m_passive_stream] );
		}
	}
	
	// Before overlap region; just increment the position in the active vector.
	//In overlap region: Decide for each pixel and stream
	if( m_data_vector[m_active_vector].at(m_current_position[m_active_vector]).toa <	
		m_overlap_time || m_finish ){
			m_current_position[m_active_vector]++;
	}
	else{
		if( m_data_vector[0].at(m_current_position[0]).toa < 
			m_data_vector[1].at(m_current_position[1]).toa ){
				m_current_position[0]++;
				m_active_vector = 0;
		}
		else if( m_data_vector[0].at(m_current_position[0]).toa >= 
			m_data_vector[1].at(m_current_position[1]).toa ){
				m_current_position[1]++;
				m_active_vector = 1;
		}
	}

	return false;
}

void KathrineFileReader::TimeOverlapRangeCheck( vector<pixel_data> &v_act, vector<pixel_data> &v_pas ){
	//Check if active vector's last entry is bigger than passive vector's last entry.
	//Remove entries from the active vector until passive vector has higher end time.
	size_t size_passive = v_pas.size();
	double passive_end_time = v_pas.at( size_passive-1 ).toa;

	while( v_act.at( v_act.size()-1 ).toa > passive_end_time ){
		cerr << v_act.at( v_act.size()-1 ).toa << endl;
		cerr << "removing entry and adding it to passive vector! " << endl;
		v_pas.insert( v_pas.begin() + size_passive, v_act.at( v_act.size()-1 ) ); 
		v_act.pop_back();
	}
}