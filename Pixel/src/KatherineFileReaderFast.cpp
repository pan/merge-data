#include <algorithm>
#include <iomanip>
#include <cstdio>

#include <omp.h>
#include <boost/lexical_cast.hpp>

#include "KatherineFileReaderFast.h"

const char delim = '\n';

KathrineFileReaderFast::KathrineFileReaderFast(string& fn) {
	cerr << "mapped file reader constructor" << endl;

	m_infile.open(fn, ios::in);
	cerr << fn.c_str() << endl;
	if (!m_infile.is_open()) {
		cerr << "NO INPUT FILE WAS FOUND" << endl;
		exit(-1);
	}
	
	m_filename = fn;
	//~ _max_chunk = pow(1024, 3);
	_max_chunk = pow(1024,2);

	size_t i = max<size_t>(_max_chunk / (size_t)boost::iostreams::mapped_file::alignment(), 1);
	_chunk_size = i * boost::iostreams::mapped_file::alignment();
	cerr << _chunk_size << endl;

	params.path = fn;
	params.length = _chunk_size; // default: complete file
	params.flags = boost::iostreams::mapped_file::mapmode::readonly;
	
	_file_size = boost::filesystem::file_size(params.path);
	
	_file_location = 0;
	_chunk_location = 0;
	
	MemoryMapNextChunk();
	
	_line_buffer = new char[512];
	memset(_line_buffer, 0, sizeof(char) * 512);
	_pos_buffer = 0;

	m_eof = false;
	m_finish = false;
	m_last_line_read = false;
	m_nlines = 0;
	m_start_time_compensation_ns = 0;
		
	m_dsc_tree = new TTree("InfoTree", "");

	m_data_vector[0].reserve(MAX_CHUNK_LENGTH+1);
	m_data_vector[1].reserve(MAX_CHUNK_LENGTH+1);
}

KathrineFileReaderFast::~KathrineFileReaderFast() {
	if(m_infile.is_open())
		m_infile.close();
	
	if (mf.is_open())
		mf.close();

	delete[] _line_buffer;
}

bool KathrineFileReaderFast::GetLastLine(string& last_line, std::ifstream& in) {
	if (!in.good()) { return false; }

	int original_pos = in.tellg();
	in.seekg(-3, ios_base::end);

	bool keep_looking = true;
	while (keep_looking) {
		char ch;
		in.get(ch);

		if ((std::size_t)in.tellg() <= 1) {
			in.seekg(0);
			keep_looking = false;
		}
		else if (ch == '\n') {
			keep_looking = false;
		}
		else {
			in.seekg(-2, ios_base::cur);
		}
	}
	getline(in, last_line);

	in.seekg(original_pos, in.beg);
	in.close();
	return true;
}

//strace -- linux debugging
bool KathrineFileReaderFast::MemoryMapNextChunk() {
	//~ cerr << "memory map next chunk" <<endl;
	if(mf.is_open())
		mf.close();
	
	//~ cerr << "test" << endl;
	if (_file_location >= _file_size) {
		m_eof = true;
		return m_eof;
	}

	//~ cerr << "file_end" << endl;
	params.offset = _file_location;
	params.length = std::min(_chunk_size, _file_size - _file_location);

	mf.open(params);
	
	//~ cerr << "chunk mapped" << endl;
	
	_data = (char*)mf.data();
	
	//~ cerr  << "data available" << endl;
	_file_location += _chunk_size;
	//~ cerr << "file_loce: " << _file_location << endl;
	return false;
}

bool KathrineFileReaderFast::ReadChunkOfData(vector<pixel_data>& vec) {
	
	if (_file_location >= _file_size) { return true; }

	bool eof = false;

	if (m_nlines == 0) {
		_initial_position += 3;
		_chunk_location += 3;
	}

	_initial_position = _chunk_location;

	while (!m_eof) {
		if (_data[_chunk_location] == delim) {
			if (_data[_initial_position] == '#') {
				size_t len = _chunk_location - _initial_position;
				if (len > 0) {
					char* header_line = new char[len];
					memset(header_line, 0, sizeof(char) * len);
					FillChar(header_line, _initial_position, _initial_position + len);
					string line(header_line);
					AnalyseHeaderLine(line);
				}
			}
			else {
				//~ int i_aux = _initial_position;
				GetNumber(_data, _initial_position, m_input_data.matrix_idx, _chunk_location - 1);
				GetNumber(_data, _initial_position, m_input_data.sToA, _chunk_location - 1);
				GetNumber(_data, _initial_position, m_input_data.fToA, _chunk_location - 1);
				GetNumber(_data, _initial_position, m_input_data.tot, _chunk_location - 1);

				m_nlines++;
				
				if (!m_mask[m_input_data.matrix_idx]) {
					vec.push_back(m_input_data);
				}
			}
			_initial_position = _chunk_location + 1;
		}

		_chunk_location++;

		if (_chunk_location >= params.length) {
			FillLineBuffer(_initial_position, params.length);

			eof = MemoryMapNextChunk();

			if (eof)
				break;

			//cerr << "Finish line" << endl;
			_chunk_location = 0;

			if (_pos_buffer != 0) {
				FillLineBuffer(_chunk_location, params.length);
				
				size_t i_read = 0;
				GetNumber(_line_buffer, i_read, m_input_data.matrix_idx, _pos_buffer - 1);
				GetNumber(_line_buffer, i_read, m_input_data.sToA, _pos_buffer - 1);
				GetNumber(_line_buffer, i_read, m_input_data.fToA, _pos_buffer - 1);
				GetNumber(_line_buffer, i_read, m_input_data.tot, _pos_buffer - 1);

				m_nlines++;
				
				if (!m_mask[m_input_data.matrix_idx]) {
					vec.push_back(m_input_data);
				}

				_pos_buffer = 0;
			}
			_chunk_location++;
			_initial_position = _chunk_location;
		}

		if (eof)
			break;

		if (vec.size() >= MAX_CHUNK_LENGTH) { break; }
	}

	
	vector<pixel_data>::iterator it;
	for (it = vec.begin(); it < vec.end(); ++it) {
		it->pixel_x = it->matrix_idx % MATRIX_WIDTH;
	}
	for (it = vec.begin(); it < vec.end(); ++it) {
		it->pixel_y = it->matrix_idx / MATRIX_WIDTH;
	}
	
	if (m_pixel_pitch_scale != 1) {
		for (it = vec.begin(); it < vec.end(); ++it) {
			it->pixel_x /= m_pixel_pitch_scale;
		}
		
		for (it = vec.begin(); it < vec.end(); ++it) {
			it->pixel_y /= m_pixel_pitch_scale;
		}

		for (it = vec.begin(); it < vec.end(); ++it) {
			it->matrix_idx = it->pixel_x + MATRIX_WIDTH / m_pixel_pitch_scale * it->pixel_y;
		}
	}

	for (it = vec.begin(); it < vec.end(); ++it) {
		it->toa = it->sToA * m_toa_clock_scale - it->fToA * m_ftoa_clock_scale + m_toa_bug_correction[it->pixel_x];
	}

	for (it = vec.begin(); it < vec.end(); ++it) {
		it->toa += m_time_offset;
	}

	//~ for (it = vec.begin(); it < vec.end(); ++it) {
		//~ it->toa += m_start_time_compensation_ns;
	//~ }

	for (it = vec.begin(); it < vec.end(); ++it) {
		it->absolute_time_s = it->toa / 1E9 + m_start_time;
	}

	if (m_matrix_clock_rescale != 1) {
		for (it = vec.begin(); it < vec.end(); ++it) {
			it->tot *= m_matrix_clock_rescale;
		}
	}
	
	cerr << "vector size " << vec.size() << endl;
	m_nentries = std::max<size_t>(m_data_vector[0].size(), m_data_vector[1].size());
	std::sort(vec.begin(), vec.end(), pixel_data::compare_by_toa);

	for (size_t i = 0; i < _rotation_commands.size(); ++i) {
		_geometry_manipulation.Rotate(_rotation_commands[i], vec);
	}
	if (_translate == true) {
		_geometry_manipulation.Translate(vec);
	}
	if (m_energy_calibration) {
		_calibrator.calibrate(vec);
	}

	return eof;
}

void KathrineFileReaderFast::AnalyseHeaderLine(const string& s) {
	if (s.compare(0, 35, "# Start of measurement - unix time:") == 0) {
		m_dsc_tree->Branch("start_time", &m_start_time, "start_time/D");
		m_start_time = stod(s.substr(s.find_first_of("1234567890")));
	}
	else if (s.compare(0, 10, "# Chip ID:") == 0) {
		m_dsc_tree->Branch("chipboard_id", &m_chipboard_id);
		m_chipboard_id = s.substr(s.find(":") + 1);
	}
	else if (s.compare(0, 21, "# Readout IP address:") == 0) {
		m_dsc_tree->Branch("readout_ip", &m_readout_ip);
		m_readout_ip = s.substr(s.find(":") + 1);
	}
	else if (s.compare(0, 16, "# Detector mode:") == 0) {
		m_dsc_tree->Branch("detector_mode", &m_det_mode);
		m_det_mode = s.substr(s.find(":") + 1);
	}
	else if (s.compare(0, 15, "# Bias voltage:") == 0) {
		m_dsc_tree->Branch("bias", &m_bias, "bias/D");
		size_t start_pos = s.find_first_of("0123456789");
		size_t end_pos = s.find_first_not_of("0123456789.", start_pos + 1);
		m_bias = stod(s.substr(start_pos, end_pos - start_pos));
	}
	else if (s.compare(0, 7, "# THL =") == 0) {
		m_dsc_tree->Branch("threshold", &m_threshold, "threshold/I");
		size_t start_pos = s.find("=") + 1;
		size_t end_pos = s.find("(") - 1;
		m_threshold = stoi(s.substr(start_pos, end_pos - start_pos));
	}
	else if (s.compare(0, 7, "# DACs:") == 0) {
		m_dsc_tree->Branch("dacs", m_dacs, "dacs[18]/S");
		stringstream ss;
		ss << s.substr(s.find(":") + 2);
		for (int i = 0; i < 18; ++i) { ss >> m_dacs[i]; }
	}
	else if (s.compare(0, 21, "# Sensor temperature:") == 0) {
		m_dsc_tree->Branch("sensor_temperature", &m_sensor_temp, "sensor_temperature/F");
		size_t start_pos = s.find(":") + 1;
		size_t end_pos = s.find_first_not_of("1234567890.", start_pos + 1);
		m_sensor_temp = stof(s.substr(start_pos, end_pos - start_pos));
	}
	else if (s.compare(0, 22, "# Readout temperature:") == 0) {
		m_dsc_tree->Branch("readout_temperature", &m_readout_temp, "readout_temp/F");
		size_t start_pos = s.find(":") + 1;
		size_t end_pos = s.find_first_not_of("1234567890.", start_pos + 1);
		m_readout_temp = stof(s.substr(start_pos, end_pos - start_pos));
	}
	else if (s.compare(0, 8, "# Frame:") == 0 && !m_last_line_read) {
		m_last_line_read = true;
		m_dsc_tree->Branch("relative_start_time", &m_relative_start_time, "relative_start_time/D");
		m_dsc_tree->Branch("relative_end_time", &m_relative_end_time, "relative_end_time/D");
		m_dsc_tree->Branch("lost_hits", &m_lost_hits, "lost_hits/I");

		size_t start_pos = s.find("Start time:") + 12;
		start_pos = s.find_first_of("0123456789", start_pos);
		size_t end_pos = s.find_first_not_of("1234567890.E+-", start_pos + 1);
		m_relative_start_time = stod(s.substr(start_pos, end_pos - start_pos));
		if (m_matrix_clock != 40) { m_relative_start_time = 0; }

		start_pos = s.find("End time:") + 10;
		start_pos = s.find_first_of("0123456789", start_pos);
		end_pos = s.find_first_not_of("1234567890.E-+", start_pos + 1);
		m_relative_end_time = stod(s.substr(start_pos, end_pos - start_pos));

		start_pos = s.find("Lost Hits:") + 11;
		end_pos = s.find_first_not_of("1234567890.E+-", start_pos + 1);
		m_lost_hits = stoi(s.substr(start_pos, end_pos - start_pos));

		m_dsc_tree->Branch("start_time_compensation_ns", &m_start_time_compensation_ns, "m_start_time_compensation_ns/D");
		m_start_time_compensation_ns = (5.0E-8 - m_relative_start_time) / 1.0E-9; // if 50 ns no offset has to be added; if 25 -> (-25 ns) have to be added to ToA;

		//cerr << "start comp: "  << m_start_time_compensation_ns << endl;
	}
}

void KathrineFileReaderFast::Initialize() {
	m_toa_clock_scale = 1E9 / 1E6 / m_matrix_clock;
	if (m_matrix_clock == 40) {
		m_ftoa_clock_scale = 1E9 / 1E6 / FTOA_CLOCK_MHZ;
	}
	else {
		m_ftoa_clock_scale = 0;
	}

	string line;
	GetLastLine(line, m_infile);
	AnalyseHeaderLine(line);

	for (int i = 0; i < 2; ++i) {
		m_eof = ReadChunkOfData(m_data_vector[i]);
		m_current_position[i] = 0;
	}

	m_active_stream = 0;
	m_passive_stream = 1;
	m_active_vector = 0;
	m_current_position[0]++;

	if (m_data_vector[1].size() == 0) { m_finish = true; }
	else {
		m_overlap_time = m_data_vector[1].at(0).toa;
		TimeOverlapRangeCheck(m_data_vector[m_active_stream], m_data_vector[m_passive_stream]);
	}

	m_dsc_tree->Branch("matrix_clock_MHz", &m_matrix_clock, "matrix_clock_MHz/I");
	m_dsc_tree->Branch("matrix_clock_rescale", &m_matrix_clock_rescale, "matrix_clock_rescale/D");
	m_dsc_tree->Fill();
}

bool KathrineFileReaderFast::LoadNextEntry() {
	if (m_current_position[m_active_vector] == m_data_vector[m_active_vector].size()) {
		if (m_finish) { return true; }

		if (m_eof) { m_finish = true; }
		else {
			m_data_vector[m_active_vector].clear();
			m_eof = ReadChunkOfData(m_data_vector[m_active_vector]);
			m_overlap_time = m_data_vector[m_active_vector].at(0).toa;
		}

		m_current_position[m_active_vector] = 0;

		if (m_active_stream == 0) {
			m_active_stream = 1;
			m_passive_stream = 0;
		}
		else {
			m_active_stream = 0;
			m_passive_stream = 1;
		}
		m_active_vector = m_active_stream;

		if (!m_eof) {
			TimeOverlapRangeCheck(m_data_vector[m_active_stream], m_data_vector[m_passive_stream]);
		}
	}

	// Before overlap region; just increment the position in the active vector.
	//In overlap region: Decide for each pixel and stream
	if (m_data_vector[m_active_vector].at(m_current_position[m_active_vector]).toa <
		m_overlap_time || m_finish) {
		m_current_position[m_active_vector]++;
	}
	else {
		if (m_data_vector[0].at(m_current_position[0]).toa <
			m_data_vector[1].at(m_current_position[1]).toa) {
			m_current_position[0]++;
			m_active_vector = 0;
		}
		else if (m_data_vector[0].at(m_current_position[0]).toa >=
			m_data_vector[1].at(m_current_position[1]).toa) {
			m_current_position[1]++;
			m_active_vector = 1;
		}
	}

	return false;
}

void KathrineFileReaderFast::TimeOverlapRangeCheck(vector<pixel_data>& v_act, vector<pixel_data>& v_pas) {
	//Check if active vector's last entry is bigger than passive vector's last entry.
	//Remove entries from the active vector until passive vector has higher end time.
	size_t size_passive = v_pas.size();
	double passive_end_time = v_pas.at(size_passive - 1).toa;

	while (v_act.at(v_act.size() - 1).toa > passive_end_time) {
		cerr << v_act.at(v_act.size() - 1).toa << endl;
		cerr << "removing entry and adding it to passive vector! " << endl;
		v_pas.insert(v_pas.begin() + size_passive, v_act.at(v_act.size() - 1));
		v_act.pop_back();
	}
}