#include "CoincidenceModule.h"
#include <cfloat>
#include <iostream>

CoincidenceModule::CoincidenceModule(short &l)
{
	m_t = new TTree("coincidenceAnalysis", "Contains the results of the coincidence analysis");
	m_t->SetDirectory(0);

	_save = false;
	
	m_coinc_number = 0;
	m_no_layers = l;

	m_involved_layers.resize(m_no_layers);
	m_tot_layer.resize(m_no_layers);
	m_no_of_pixels_layer.resize(m_no_layers);
	m_min_toa_layer.resize(m_no_layers);
	
	DefineRootTree();
	Reset();
}

CoincidenceModule::~CoincidenceModule()
{
	delete m_t;
}

void CoincidenceModule::DefineRootTree()
{
	std::stringstream layer_stream;
	layer_stream << m_no_layers;
			
	/*	"coincNo": absolute number of the coincidence
			
		"coincType": describes the number of involved detectors
					in a coincidence assignment in the following way. 
		
		Example: 3 detectors (1, 2, 3);
		coincidence of 1 + 2 -> coincType: 3;
		coincidence of 2 + 3 -> coincType: 5;
		coincidence of 1 + 3 -> coincType: 4; e.g. when parallel in beam or at vertex;
		for more detectors accordingly					
			
		"coinc_size": holds the number of coincident clusters.
		
		"cluster_index": represents the FIRST ENTRY of the 
		coincidence event in the CLUSTER tree. Accordingly the full 
		event starts at "cluster_index" and finishes at 
		"cluster_index + coinc_size"
	*/

	//m_t->Branch("coincidence_group", &m_cluster_index, "coincidence_group/L");
	m_t->Branch("coinc_size", &m_coinc_size, "coinc_size/S");
	m_t->Branch("coinc_type", &m_coinc_type, "coinc_type/S");
	m_t->Branch("coinc_no", &m_coinc_number, "coinc_no/L");
	m_t->Branch("tot_layers", m_tot_layer.data(), ("tot_layers["+layer_stream.str()+"]/F").c_str());
	m_t->Branch("no_of_pixels_layer", m_no_of_pixels_layer.data(), ("no_of_pixels_layer["+layer_stream.str()+"]/I").c_str());
	m_t->Branch("min_toa_layer", m_min_toa_layer.data(), ("min_toa_layer["+layer_stream.str()+"]/D").c_str());
	m_t->Branch("min_toa", &m_min_toa, "min_toa/D");
}

void CoincidenceModule::Reset()
{
	m_coinc_size = 0;
	m_coinc_type = 0;
	m_min_toa = DBL_MAX;

	for(int i = 0; i < m_no_layers; i++)
	{
		std::fill(m_involved_layers.begin(), m_involved_layers.end(), false);
		std::fill(m_tot_layer.begin(), m_tot_layer.end(), 0);
		std::fill(m_no_of_pixels_layer.begin(), m_no_of_pixels_layer.end(), 0);
		std::fill(m_min_toa_layer.begin(), m_min_toa_layer.end(), DBL_MAX);
	}
}

void CoincidenceModule::FillTree(){
	if(_save){
		m_t->Fill();
	}
	Reset();
}

bool CoincidenceModule::EvaluateCoincidence(){

	for (auto it = _coincident_clusters.cbegin(); it != _coincident_clusters.cend(); ++it) {
		if (!m_involved_layers[it->GetLayer() - 1]) {
			m_involved_layers[it->GetLayer() - 1] = true;
			m_coinc_type += it->GetLayer();
		}
	}
	
	if (std::find(m_involved_layers.begin(), m_involved_layers.end(), false) != m_involved_layers.end()) {
		_save = false;
		return false;
	}
	
	m_min_toa = _coincident_clusters[0].GetMinToA();
	m_coinc_size = _coincident_clusters.size();

	for (auto it = _coincident_clusters.begin(); it != _coincident_clusters.end(); ++it) {
		m_tot_layer[it->GetLayer() - 1] += it->GetVolume_keV();
		m_no_of_pixels_layer[it->GetLayer()-1] += it->GetSize();
		
		if (it->GetMinToA() < m_min_toa_layer[it->GetLayer() - 1])
			m_min_toa_layer[it->GetLayer() - 1] = it->GetMinToA();
	}

	m_coinc_number++;
	_save = true;
	return true;
}