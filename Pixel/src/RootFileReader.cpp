#include <algorithm>
#include <iomanip>

#include "RootFileReader.h"

RootFileReader::RootFileReader(std::string &fn){
	m_f = new TFile(fn.c_str(), "OPEN");
	if( m_f->IsZombie() ){
		std::cerr << "ROOT File not found" << std::endl;
		exit(-1);
	}
	m_tree = (TTree*) m_f->Get("Datatree");


	m_filename = fn;
	m_entries = m_tree->GetEntriesFast();

	m_current_position = 0;

	//SetBranchAddresses();
}

RootFileReader::~RootFileReader(){
	delete m_f;
}

void RootFileReader::SetBranchAddresses(){
	m_tree->GetBranch("ToA")->SetAddress(&m_input_data.toa);
	m_tree->GetBranch("ToT")->SetAddress(&m_input_data.tot);
	m_tree->GetBranch("PixX")->SetAddress(&m_input_data.pixel_x);
	m_tree->GetBranch("PixY")->SetAddress(&m_input_data.pixel_y);
	//m_tree->GetBranch("triggerNo")->SetAddress(&m_input_data.triggerNo);
	m_tree->GetBranch("sToA")->SetAddress(&m_input_data.sToA);
	m_tree->GetBranch("fToA")->SetAddress(&m_input_data.fToA);
}

bool RootFileReader::LoadNextEntry(){
	//Prevent the program from trying to access an entry that does not exist
	//Return an End of File flag!
	if( m_current_position >= m_entries){
		return true;
	}
	else{
		m_tree->GetEntry(m_current_position);

		m_input_data.toa = m_input_data.toa + m_time_offset + m_toa_bug_correction[m_input_data.pixel_x];
		m_input_data.matrix_idx = m_input_data.pixel_x + MATRIX_WIDTH * m_input_data.pixel_y;
		
		//if(m_energy_calibration){
		//	m_input_data.tot_keV = E_cali(
		//		m_a[m_input_data.matrix_idx],
		//		m_b[m_input_data.matrix_idx],
		//		m_c[m_input_data.matrix_idx],
		//		m_t[m_input_data.matrix_idx],
		//		m_input_data.tot
		//	);
		//	if(m_tw_correction){
		//		Timewalk_correction(m_input_data.toa, m_input_data.tot_keV);
		//	}
		//}
		//else{
		//	m_input_data.tot_keV = -1;
		//}

		m_current_position++;
		return false;
	}
}

//bool RootFileReader::LoadEntry(Long64_t &i){
//	//Prevent the program from trying to access an entry that does not exist
//	//Return an End of File flag!
//	if( i >= m_entries){
//		return true;
//	}
//	else{
//		m_tree->GetEntry(i);
//
//		m_input_data.toa = m_input_data.toa + m_time_offset + m_toa_bug_correction[m_input_data.pixel_x];
//		m_input_data.matrix_idx = m_input_data.pixel_x + MATRIX_WIDTH * m_input_data.pixel_y;
//		
//		if(m_energy_calibration){
//			m_input_data.tot_keV = E_cali(
//				m_a[m_input_data.matrix_idx],
//				m_b[m_input_data.matrix_idx],
//				m_c[m_input_data.matrix_idx],
//				m_t[m_input_data.matrix_idx],
//				m_input_data.tot
//			);
//			if(m_tw_correction){
//				Timewalk_correction(m_input_data.toa, m_input_data.tot_keV);
//			}
//		}
//		else{
//			m_input_data.tot_keV = -1;
//		}
//
//		m_current_position = i;
//		return false;
//	}
//}
