#include <iostream>
#include <sstream>
#include <algorithm>
#include <fstream>

#include "DetectorConfigurator.h"

using namespace std;

void DetectorConfigurator::PrintConfiguration() {
	cout << "Detector contains: " << _vec_sensor_data.size() << " detectors and " << GetNumberOfLayers() << " layers" << endl;
	
	for (size_t i = 0; i < _vec_sensor_data.size(); ++i) {
		cout << "Sensor:\t"			<< _vec_sensor_data[i].id				<< endl;
		cout << "Layer:\t"			<< _vec_sensor_data[i].layer			<< endl;
		cout << "Translation:\t"	<< _vec_sensor_data[i].translation.first << ", " << _vec_sensor_data[i].translation.second << endl;
		cout << "Calib:\t"			<< _vec_sensor_data[i].calib_dir		<< endl;
		cout << "Timewalk:\t"		<< _vec_sensor_data[i].tw_file			<< endl;
		cout << "Time offset:\t"	<< _vec_sensor_data[i].time_offset		<< endl;
		for (size_t j = 0; j < _vec_sensor_data[i].rotation.size(); ++j) {
			cout << "Rotation:\t"	<< _vec_sensor_data[i].rotation[j].first << ", " <<
									 _vec_sensor_data[i].rotation[j].second << endl;
		}
	
		cout << "Pixel_Pitch_Gap_XY:\t"	<< _vec_sensor_data[i].pixel_pitch_gap_xy.first << ", " << _vec_sensor_data[i].pixel_pitch_gap_xy.second << endl;

		cout << endl;
	}
}

void DetectorConfigurator::ReadConfigurationFromFile(const string& fn)
{
	ifstream in;
	in.open(fn, ios::in);
	if (!in.good())
		cerr << "configuration file not found! " << endl;
	string line;

	const string chip_id_compare = "Chip Id:";
	const string layer_compare = "Layer:";
	const string rotation_compare = "Rotation:";
	const string translation_compare = "Translation:";
	
	const string calib_dir_compare = "Calibration:";
	const string tw_file_compare = "Timewalk:";
	const string mask_file_compare = "Mask:";
	const string column_corr_compare = "Column correction:";
	const string t_offset_compare = "Time offset:";

	const string pixel_pitch_gap_xy_compare = "Pixel_Pitch_Gap_XY:";

	stringstream ss;
	string detector;
	string command;

	while (getline(in, line)) {
		if (line[0] == '#')
			continue;

		ss.clear();
		ss.str(line);

		if (line.compare(0, chip_id_compare.size(), chip_id_compare) == 0) {
			while (getline(ss, line, ' ')) {				
				if (line[1] == 'h' || line[1] == 'd' || line == " " || line[0] == '\r')
					continue;

				AddSensor(line);
			}
		}

		int iv1, iv2;
		float g1, g2;
		float fv;
		double dv;
		string sv;
		short shv;
		
		if (line.compare(0, layer_compare.size(), layer_compare) == 0) {
			ss >> command >> detector >> shv;
			SetLayer(detector, shv);
		}

		if (line.compare(0, rotation_compare.size(), rotation_compare) == 0) {
			ss >> command >> detector >> iv1 >> fv;
			AddRotation(detector, iv1, fv);
		}

		if (line.compare(0, translation_compare.size(), translation_compare) == 0) {
			ss >> command >> detector >> iv1 >> iv2;
			SetTranslation(detector, iv1, iv2);
		}

		if (line.compare(0, calib_dir_compare.size(), calib_dir_compare) == 0) {
			ss >> command >> detector >> sv;
			SetCalibrationDir(detector, sv);
		}

		if (line.compare(0, tw_file_compare.size(), tw_file_compare) == 0) {
			ss >> command >> detector >> sv;
			SetTimewalkFile(detector, sv);
		}

		if (line.compare(0, mask_file_compare.size(), mask_file_compare) == 0) {
			ss >> command >> detector >> sv;
			SetMaskFile(detector, sv);
		}

		if (line.compare(0, column_corr_compare.size(), column_corr_compare) == 0) {
			ss >> command >> command >> detector >> sv;
			SetColumnCorrectionFile(detector, sv);
		}

		if (line.compare(0, t_offset_compare.size(), t_offset_compare) == 0) {
			ss >> command >> command >> detector >> dv;
			SetTimeOffset(detector, dv);
		}

		if (line.compare(0, pixel_pitch_gap_xy_compare.size(), pixel_pitch_gap_xy_compare) == 0) {
			ss >> command >> g1 >> g2;
			SetPixelPitchGap(g1, g2);
		}
	}
}

void DetectorConfigurator::SetLayer(const string& s_id, const int& layer){
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });
	if(it!= _vec_sensor_data.end())
		it->layer = layer;
}

void DetectorConfigurator::AddRotation(const string& s_id, const int& r_axis, const float& r_angle) {
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });
	if (it != _vec_sensor_data.end())
		it->rotation.push_back(make_pair(r_axis, r_angle));
}

void DetectorConfigurator::SetPixelPitchGap(const double& pp_gapX, const double& pp_gapY) {
	for (size_t i = 0; i < _vec_sensor_data.size(); ++i) {
	 	_vec_sensor_data[i].pixel_pitch_gap_xy = make_pair(pp_gapX, pp_gapY);
	}
}

void DetectorConfigurator::SetTranslation(const string& s_id, const int& x_shift, const int& y_shift) {
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });
	if (it != _vec_sensor_data.end())
		it->translation = make_pair(x_shift, y_shift);
}

void DetectorConfigurator::SetCalibrationDir(const string& s_id, const string& c_dir) {
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });
	if (it != _vec_sensor_data.end())
		it->calib_dir = c_dir;
}

void DetectorConfigurator::SetTimewalkFile(const string& s_id, const string& tw_file) {
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });
	if (it != _vec_sensor_data.end())
		it->tw_file = tw_file;
}

void DetectorConfigurator::SetMaskFile(const string& s_id, const string& mask_file) {
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });
	if (it != _vec_sensor_data.end())
		it->mask_file = mask_file;
}

void DetectorConfigurator::SetColumnCorrectionFile(const string& s_id, const string& cc_file) {
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });

	if (it != _vec_sensor_data.end())
		it->toa_column_corr_file = cc_file;

	cerr << "set colum correction file: " << it->toa_column_corr_file << endl;
}

void DetectorConfigurator::SetTimeOffset(const string& s_id, const double& t_offset) {
	auto it = std::find_if(_vec_sensor_data.begin(), _vec_sensor_data.end(),
		[s_id](const sensor_description& item) { return item.id == s_id; });
	if (it != _vec_sensor_data.end())
		it->time_offset = t_offset; 
}

int DetectorConfigurator::GetNumberOfLayers() {
	vector<int> vec;
	for (auto it = _vec_sensor_data.begin(); it != _vec_sensor_data.end(); ++it) {
		if (find(vec.begin(), vec.end(), it->layer) == vec.end())
			vec.push_back(it->layer);
	}
	//cout << "Found " << vec.size() << " layers!" << endl;
	return vec.size();
}

