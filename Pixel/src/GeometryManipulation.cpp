#include "GeometryManipulation.h"

using namespace std;

GeometryManipulator::GeometryManipulator() {
	_rot_x_deg = 0.f;
	_rot_y_deg = 0.f;
	_rot_z_deg = 0.f;

	_matrix_center[0] = MATRIX_WIDTH / 2.f - 0.5;
	_matrix_center[1] = MATRIX_HEIGHT / 2.f - 0.5;

	_translation_x_pix = 0;
	_translation_y_pix = 0;

	_gap_x_pix = 0;
	_gap_y_pix = 0;

	for (int i = 0; i < 2; ++i) {
		std::fill(_rot_matrix_x[i].begin(), _rot_matrix_x[i].end(), 0.f);
		std::fill(_rot_matrix_y[i].begin(), _rot_matrix_y[i].end(), 0.f);
		std::fill(_rot_matrix_z[i].begin(), _rot_matrix_z[i].end(), 0.f);
	}
	//PrintRotationMatrices();
}

void GeometryManipulator::SetRotationAxisAngle(const int& i, const float& a) {
	switch (i) {
		case 1: SetRotationX(a); break;
		case 2: SetRotationY(a); break;
		case 3: SetRotationZ(a); break;
	}
}

void GeometryManipulator::CalculateRotationMatrix(int axis) {
	if (axis == 1) {
		_rot_matrix_x[0][0] = 1;
		_rot_matrix_x[1][1] = cosf(_rot_x_deg / 180. * M_PI);
	}
	if (axis == 2) {
		_rot_matrix_y[0][0] = cosf(_rot_y_deg / 180. * M_PI);
		_rot_matrix_y[1][1] = 1;
	}
	if (axis == 3) {
		_rot_matrix_z[0][0] = round(cosf(_rot_z_deg / 180. * M_PI) * 1000.) / 1000.;
		_rot_matrix_z[0][1] = round(-sinf(_rot_z_deg / 180. * M_PI) * 1000.) / 1000.;
		_rot_matrix_z[1][0] = round(sinf(_rot_z_deg / 180. * M_PI) * 1000.) / 1000.;
		_rot_matrix_z[1][1] = round(cosf(_rot_z_deg / 180. * M_PI) * 1000.) / 1000.;
	}
}

void GeometryManipulator::Rotate(int axis, vector<pixel_data>& vec) const {
	vector<pixel_data>::iterator it;
	int x, y;
	for (it = vec.begin(); it < vec.end(); ++it) {
		x = it->pixel_x;
		y = it->pixel_y;
		
		if (axis == 1) {
			it->pixel_x = _rot_matrix_x[0][0] * (x - _matrix_center[0]) + _rot_matrix_x[0][1] * (y - _matrix_center[0]) + _matrix_center[0];
			it->pixel_y = _rot_matrix_x[1][0] * (x - _matrix_center[1]) + _rot_matrix_x[1][1] * (y - _matrix_center[1]) + _matrix_center[1];
		}																				   			
		if (axis == 2) {																   			
			it->pixel_x = _rot_matrix_y[0][0] * (x - _matrix_center[0]) + _rot_matrix_y[0][1] * (y - _matrix_center[0]) + _matrix_center[0];
			it->pixel_y = _rot_matrix_y[1][0] * (x - _matrix_center[1]) + _rot_matrix_y[1][1] * (y - _matrix_center[1]) + _matrix_center[1];
		}											   									   			
		if (axis == 3) {							   									   			
			it->pixel_x = _rot_matrix_z[0][0] * (x - _matrix_center[0]) + _rot_matrix_z[0][1] * (y - _matrix_center[0]) + _matrix_center[0];
			it->pixel_y = _rot_matrix_z[1][0] * (x - _matrix_center[1]) + _rot_matrix_z[1][1] * (y - _matrix_center[1]) + _matrix_center[1];
		}


		it->pixel_x_mm = it->pixel_x*float(PIXEL_PITCH_ASIC)/1000.;
		it->pixel_y_mm = it->pixel_y*float(PIXEL_PITCH_ASIC)/1000.;

	}
}

void GeometryManipulator::Translate(vector<pixel_data>& vec) const {
	vector<pixel_data>::iterator it;
	for (it = vec.begin(); it < vec.end(); ++it) {
		it->pixel_x = it->pixel_x + _translation_x_pix;
		it->pixel_y = it->pixel_y + _translation_y_pix;

		float x1_mm = (it->pixel_x >= 255 ? ((_gap_x_pix-1.)*float(PIXEL_PITCH_ASIC)/2.) : 0)/1000.;
		float x2_mm = (it->pixel_x >= 256 ? ((_gap_x_pix-1.)*float(PIXEL_PITCH_ASIC)) : 0)/1000.;
		float x3_mm = (it->pixel_x >= 257 ? ((_gap_x_pix-1.)*float(PIXEL_PITCH_ASIC)/2.) : 0)/1000.;
		it->pixel_x_mm = it->pixel_x*float(PIXEL_PITCH_ASIC)/1000. + (x1_mm + x2_mm + x3_mm);

		float y1_mm = (it->pixel_y >= 255 ? ((_gap_y_pix-1.)*float(PIXEL_PITCH_ASIC)/2.) : 0)/1000.;
		float y2_mm = (it->pixel_y >= 256 ? ((_gap_y_pix-1.)*float(PIXEL_PITCH_ASIC)) : 0)/1000.;
		float y3_mm = (it->pixel_y >= 257 ? ((_gap_y_pix-1.)*float(PIXEL_PITCH_ASIC)/2.) : 0)/1000.;
		it->pixel_y_mm = it->pixel_y*float(PIXEL_PITCH_ASIC)/1000. + (y1_mm + y2_mm + y3_mm);

	}
}

void GeometryManipulator::PrintRotationMatrices() const {
	std::cout << "Rotation matrix X:" << std::endl;
	std::cout << "| " << _rot_matrix_x[0][0] << " " << _rot_matrix_x[0][1] << " |" << std::endl;
	std::cout << "| " << _rot_matrix_x[1][0] << " " << _rot_matrix_x[1][1] << " |" << std::endl;
	std::cout << endl;

	std::cout << "Rotation matrix Y:" << std::endl;
	std::cout << "| " << _rot_matrix_y[0][0] << " " << _rot_matrix_y[0][1] << " |" << std::endl;
	std::cout << "| " << _rot_matrix_y[1][0] << " " << _rot_matrix_y[1][1] << " |" << std::endl;
	std::cout << endl;

	std::cout << "Rotation matrix Z:" << std::endl;
	std::cout << "| " << _rot_matrix_z[0][0] << " " << _rot_matrix_z[0][1] << " |" << std::endl;
	std::cout << "| " << _rot_matrix_z[1][0] << " " << _rot_matrix_z[1][1] << " |" << std::endl;
	std::cout << endl;
}