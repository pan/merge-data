#ifndef TOF_Tree_hpp
#define TOF_Tree_hpp

#include <iostream>
#include <vector>
#include <unordered_map>

#include "TTree.h"
#include "TString.h"
#include "TFile.h"
#include "TBranch.h"

class TOF_Tree {    
    std::size_t nQuads;
    
    TFile* _inFile{nullptr};
    TTree* _tree{nullptr};
    
protected:
    void initMembers();
    TTree* loadTree(TString filename);
    
public:
    TOF_Tree();
    TOF_Tree(TString filename);
    virtual ~TOF_Tree();
    
    TOF_Tree(const TOF_Tree& tree) = delete;
    TOF_Tree& operator=(const TOF_Tree& tree) = delete;
    
    void associateBranches();
    
    TTree* getTree() const {return _tree;}
    
    void setTree(TTree* tree) {_tree = tree;}
    
};

#endif /* TOF_Tree_hpp */
