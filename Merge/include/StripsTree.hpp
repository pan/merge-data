#ifndef StripsTree_hpp
#define StripsTree_hpp

#include <iostream>
#include <vector>
#include <unordered_map>

#include "TTree.h"
#include "TString.h"
#include "TFile.h"
#include "TBranch.h"

class StripsTree {
    std::unordered_map<int, std::vector<double> *> vintegrals, vcogs, vsoverns;
    std::unordered_map<int, std::vector<std::vector<double>> *> vsovernsPerChannel;
    std::unordered_map<int, std::vector<std::size_t> *> vlengths;
    std::unordered_map<int, UInt_t> eventNumberMap;
    std::unordered_map<int, ULong64_t> frameCounterMap;
    
    std::vector<std::array<TBranch *, 7>> branches;
    
    std::size_t nDetectors;
    
    TFile* _inFile{nullptr};
    TTree* _tree{nullptr};
    
protected:
    void initMembers();
    TTree* loadTree(TString filename);
    
public:
    StripsTree();
    StripsTree(TString filename);
    virtual ~StripsTree();
    
    StripsTree(const StripsTree& tree) = delete;
    StripsTree& operator=(const StripsTree& tree) = delete;
    
    void assignBranches();
    
    TTree* getTree() const {return _tree;}
    std::vector<double> getPos(std::size_t detector) const {return *(vcogs.at(detector));}
    std::vector<double> getIntegral(std::size_t detector) const {return *(vintegrals.at(detector));}
    std::vector<double> getSovern(std::size_t detector) const {return *(vsoverns.at(detector));}
    std::vector<std::vector<double>> getSovernsPerChannel(std::size_t detector) const {return *(vsovernsPerChannel.at(detector));}
    std::vector<std::size_t> getLength(std::size_t detector) const {return *(vlengths.at(detector));}
    std::size_t getEventNumber(std::size_t detector) const {return eventNumberMap.at(detector);}
    ULong64_t getFrameCounter(std::size_t detector) const {return frameCounterMap.at(detector);}
    
    void setTree(TTree* tree) {_tree = tree;}
    
};

#endif /* StripsTree_hpp */
