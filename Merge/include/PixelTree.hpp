#ifndef PixelTree_hpp
#define PixelTree_hpp

#include <iostream>
#include <vector>
#include <unordered_map>

#include "TTree.h"
#include "TString.h"
#include "TFile.h"
#include "TBranch.h"

class PixelTree {    
    std::size_t nQuads;
    
    TFile* _inFile{nullptr};
    TTree* _tree{nullptr};
    TTree* _treeQuad1{nullptr};
    TTree* _treeQuad2{nullptr};
    
protected:
    void initMembers();
    TTree* loadTree();
    TTree* loadTreeQuad1();
    TTree* loadTreeQuad2();
    
public:
    PixelTree();
    PixelTree(TString filename);
    virtual ~PixelTree();
    
    PixelTree(const PixelTree& tree) = delete;
    PixelTree& operator=(const PixelTree& tree) = delete;
    
    void associateBranches();
    
    TTree* getTree() const {return _tree;}
    TTree* getTreeQuad1() const {return _treeQuad1;}
    TTree* getTreeQuad2() const {return _treeQuad2;}
    
    void setTree(TTree* tree) {_tree = tree;}
    void setTreeQuad1(TTree* tree) {_treeQuad1 = tree;}
    void setTreeQuad2(TTree* tree) {_treeQuad2 = tree;}
    
};

#endif /* PixelTree_hpp */
