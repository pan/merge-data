#include "TOF_Tree.hpp"

TOF_Tree::TOF_Tree() : nQuads{2} {
    TTree tree;
    _tree = &tree;
}

TOF_Tree::TOF_Tree(TString filename) : nQuads{2} {
    _tree = loadTree(filename);
}

TOF_Tree::~TOF_Tree() {
    if (_inFile) _inFile->Close();
}

TTree* TOF_Tree::loadTree(TString filename) {
    _inFile = TFile::Open(filename, "read");
    TTree* tree = _inFile->Get<TTree>("ttof");
    return tree;
}
