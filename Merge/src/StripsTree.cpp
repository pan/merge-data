#include "StripsTree.hpp"

StripsTree::StripsTree() : nDetectors{6} {
    initMembers();
    TTree tree;
    _tree = &tree;
}

StripsTree::StripsTree(TString filename) : nDetectors{6} {
    initMembers();
    _tree = loadTree(filename);
    assignBranches();
}

StripsTree::~StripsTree() {
    for (std::size_t i{0}; i < nDetectors; ++i) {
        if (vintegrals[i]) {
            vintegrals[i]->clear();
            vintegrals[i]->shrink_to_fit();
            delete vintegrals[i];
            vcogs[i]->clear();
            vcogs[i]->shrink_to_fit();
            delete vcogs[i];
            vsoverns[i]->clear();
            vsoverns[i]->shrink_to_fit();
            delete vsoverns[i];
            vlengths[i]->clear();
            vlengths[i]->shrink_to_fit();
            delete vlengths[i];
            vsovernsPerChannel[i]->clear();
            vsovernsPerChannel[i]->shrink_to_fit();
            delete vsovernsPerChannel[i];
        }
    }
    
    vintegrals.clear();
    vcogs.clear();
    vsoverns.clear();
    vlengths.clear();
    vsovernsPerChannel.clear();
    eventNumberMap.clear();
    frameCounterMap.clear();
    
    if (_inFile) _inFile->Close();
}

void StripsTree::initMembers() {
    for (std::size_t i{0}; i < nDetectors; ++i) {
        vintegrals[i] = new std::vector<double>;
        vcogs[i] = new std::vector<double>;
        vsoverns[i] = new std::vector<double>;
        vlengths[i] = new std::vector<std::size_t>;
        vsovernsPerChannel[i] = new std::vector<std::vector<double>>;
        eventNumberMap[i] = 0;
        frameCounterMap[i] = 0;
    }
}

TTree* StripsTree::loadTree(TString filename) {
    _inFile = TFile::Open(filename, "read");
    TTree* tree = _inFile->Get<TTree>("sync_clusters_tree");
    return tree;
}

void StripsTree::assignBranches() {
    std::array<TBranch *, 7> temp_arr;
    branches.assign(nDetectors, temp_arr);
    
    for (std::size_t i{0}; i < nDetectors; ++i) {
        _tree->SetBranchAddress(Form("integral%lu", i), &vintegrals.at(i), &((branches.at(i)).at(0)));
        _tree->SetBranchAddress(Form("cog%lu", i), &vcogs.at(i), &((branches.at(i)).at(2)));
        _tree->SetBranchAddress(Form("sovern%lu", i), &vsoverns.at(i), &((branches.at(i)).at(3)));
        _tree->SetBranchAddress(Form("sovernPerChannel%lu", i), &vsovernsPerChannel.at(i), &((branches.at(i)).at(4)));
        _tree->SetBranchAddress(Form("length%lu", i), &vlengths.at(i), &((branches.at(i)).at(1)));
        _tree->SetBranchAddress(Form("eventNumber%lu", i), &eventNumberMap.at(i), &((branches.at(i)).at(5)));
        _tree->SetBranchAddress(Form("frameCounter%lu", i), &frameCounterMap.at(i), &((branches.at(i)).at(6)));
    }
    
    // branch check
    for (std::size_t i{0}; i < nDetectors; ++i) {
        for (uint br{0}; br < branches.at(i).size(); br++)
            std::cout << "ladder " << i << " branch " << br << " has name " << branches.at(i).at(br)->GetName() << std::endl;
    }
}
