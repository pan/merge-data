#include "PixelTree.hpp"

PixelTree::PixelTree() : nQuads{2} {
    TTree tree;
    _tree = &tree;
}

PixelTree::PixelTree(TString filename) : nQuads{2} {
    _inFile = TFile::Open(filename, "read");
    
    if(_inFile->GetListOfKeys()->Contains("clusteredData")){_tree = loadTree();}
    if(_inFile->GetListOfKeys()->Contains("clusteredData_Quad1")){_treeQuad1 = loadTreeQuad1();}
    if(_inFile->GetListOfKeys()->Contains("clusteredData_Quad2")){_treeQuad2 = loadTreeQuad2();}
}

PixelTree::~PixelTree() {
    if (_inFile) _inFile->Close();
}

TTree* PixelTree::loadTree() {
    TTree* tree = _inFile->Get<TTree>("clusteredData");
    return tree;
}

TTree* PixelTree::loadTreeQuad1() {
    TTree* tree = _inFile->Get<TTree>("clusteredData_Quad1");
    return tree;
}

TTree* PixelTree::loadTreeQuad2() {
    TTree* tree = _inFile->Get<TTree>("clusteredData_Quad2");
    return tree;
}