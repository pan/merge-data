#!/usr/bin/bash

#!/bin/sh
#SBATCH --job-name PixelClustering_PAN
#SBATCH --error=/home/users/h/hulsman/scratch/job_info/err/PixelClustering_PAN_%j.err
#SBATCH --output=/home/users/h/hulsman/scratch/job_info/out/PixelClustering_PAN_%j.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=private-dpnc-cpu
#SBATCH --mem-per-cpu=10000
#SBATCH --time 24:00:00
#SBATCH --mail-user=johannes.hulsman@unige.ch
#SBATCH --mail-type=ALL

DATA_DIR="/cvmfs/pan.cern.ch/Data/BeamTest/2023/Apr_PS_CERN/MiniPAN/"
PIXEL_DIR="/home/users/h/hulsman/scratch/merge-data/Pixel/bin/April_2023"
MERGE_DIR="/home/users/h/hulsman/scratch/merge-data/Merge/"

source /home/users/h/hulsman/scratch/AnalysisEnv.sh

for dir in $DATA_DIR/Pixel/*/; do
    dir_name=${dir#"$DATA_DIR/Pixel/"}
    dir_name=${dir_name%"/"}

    if test -f "${PIXEL_DIR}/pixel1_${dir_name}.root"; then
      if test -f "${PIXEL_DIR}/pixel2_${dir_name}.root"; then
	#merge pixel
	echo "srun ${MERGE_DIR}/bin/MergeData 0 ${PIXEL_DIR}/pixel1_${dir_name}.root ${PIXEL_DIR}/pixel2_${dir_name}.root ${MERGE_DIR}/bin/MergePixel_${dir_name}"
	#srun ${MERGE_DIR}/bin/MergeData 0 ${PIXEL_DIR}/pixel1_${dir_name}.root ${PIXEL_DIR}/pixel2_${dir_name}.root ${MERGE_DIR}/bin/MergePixel_${dir_name}
	
	#merge pixel+strip
	#srun ${MERGE_DIR}/bin/MergeData 1 ${MERGE_DIR}/bin/MergePixel_${dir_name}.root ${DATA_DIR}/Tracker/synchronized/synced_clusters_${dir_name}.root ${MERGE_DIR}/bin/${dir_name}

      fi

      if test -f "${PIXEL_DIR}/pixel3_${dir_name}.root"; then
        #merge pixel
	echo "srun ${MERGE_DIR}/bin/MergeData 0 ${PIXEL_DIR}/pixel1_${dir_name}.root ${PIXEL_DIR}/pixel3_${dir_name}.root ${MERGE_DIR}/bin/MergePixel_${dir_name}"
        #srun ${MERGE_DIR}/bin/MergeData 0 ${PIXEL_DIR}/pixel1_${dir_name}.root ${PIXEL_DIR}/pixel3_${dir_name}.root ${MERGE_DIR}/bin/MergePixel_${dir_name}

        #merge pixel+strip
        #srun ${MERGE_DIR}/bin/MergeData 1 ${MERGE_DIR}/bin/MergePixel_${dir_name}.root ${DATA_DIR}/Tracker/synchronized/synced_clusters_${dir_name}.root ${MERGE_DIR}/bin/${dir_name}

      fi
    fi
done

