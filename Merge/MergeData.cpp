#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <stdio.h>
#include <string> 

#include "StripsTree.hpp"
#include "PixelTree.hpp"
#include "TOF_Tree.hpp"

#include "TInterpreter.h"
#include "TSystem.h"
#include "TH2.h"
#include "TF1.h"
#include "TROOT.h"

#ifdef __ROOTCLING__
#pragma link C++ class vector<vector <int> >+;
#pragma link C++ class vector<vector <Short_t> >+;
#endif

using namespace std;

bool enableVerbose = false;

bool MergePixelQuadFiles(TFile*, string*);
void QuickAnalysis(TFile*);
void GenerateMergeTree(TFile*);
double gaus(double x, double* par);
double linear(double x, double* par);

//a few global variables
bool hasTOF_Data = false, hasStrip_Data = false, hasPixel_Data = false;

StripsTree *stripData;
PixelTree *PixelData;

int main(int argc, char **argv)
{

  if(argc == 1){
    cout << "To use the merging script. Do: " << endl;
    cout << "./MergeData 0 <Pixel Quad1 ROOT> <Pixel Quad2 ROOT> <output_filename> | Merge Quad1 + Quad2" << endl;
    cout << "./MergeData 1 <Pixel ROOT> <Strip ROOT> <output_filename>             | Pixel+Strip Merging" << endl;
    cout << "./MergeData 2 <Pixel ROOT> <TOF ROOT> <output_filename>               | Pixel+TOF Merging" << endl;
    cout << "./MergeData 3 <Strip ROOT> <TOF ROOT> <output_filename>               | Strip+TOF Merging" << endl;
    cout << "./MergeData 4 <Pixel ROOT> <Strip ROOT> <TOF ROOT> <output_filename>  | Pixel+Strip+TOF Merging" << endl;
    return 0;
  }

  gSystem->Exec("rm -f AutoDict*vector*vector*int*");
  gSystem->Exec("rm -f AutoDict*vector*vector*Short_t*");
  gInterpreter->GenerateDictionary("vector<vector<int> >", "vector");
  gInterpreter->GenerateDictionary("vector<vector<Short_t> >", "vector");

  const int iOpt = stoi(argv[1]);

  string pixel_quad[2] = {"",""};
  string pixel_filename = "";
  string strip_filename = "";
  string tof_filename = "";

  string rootfilename;
  if(iOpt == 0){
    pixel_quad[0] = string(argv[2]);
    pixel_quad[1] = string(argv[3]);
    rootfilename = string(argv[4]) + ".root";
  } else if(iOpt == 1){
    pixel_filename = string(argv[2]);
    strip_filename = string(argv[3]);
    rootfilename = string(argv[4]) + "_PixelStrip.root";
  } else if(iOpt == 2){
    pixel_filename = string(argv[2]);
    tof_filename   = string(argv[3]);
    rootfilename = string(argv[4]) + "_PixelTOF.root";
  } else if(iOpt == 3){
    strip_filename = string(argv[2]);
    tof_filename   = string(argv[3]);
    rootfilename = string(argv[4]) + "_StripTOF.root";
  } else if(iOpt == 4){
    pixel_filename = string(argv[2]);
    strip_filename = string(argv[3]);
    tof_filename   = string(argv[4]);
    rootfilename = string(argv[5]) + "_PixelStripTOF.root";
  }


  TFile *f_merge = new TFile(rootfilename.c_str(), "RECREATE");
  if (f_merge == NULL) { cout << "could not open root file " << endl; return 0; }

  if(pixel_quad[0] != "" && pixel_quad[1] != ""){
    if(!MergePixelQuadFiles(f_merge, pixel_quad)){
      cout << "Merge Failed" << endl;
      return 0;
    }
  }

  if(strip_filename != "") {
    stripData = new StripsTree(strip_filename);

    f_merge->cd();
    TTree* copyStripTree = stripData->getTree()->CloneTree();

    if(enableVerbose) {copyStripTree->Print();}
    f_merge->Write();

    hasStrip_Data = true;
    delete copyStripTree;
  }

  bool has2Quad = false;
  if(pixel_filename != "") {
    PixelData = new PixelTree(pixel_filename);

    f_merge->cd();
    TTree* copyPixelTreeQuad1 = (PixelData->getTreeQuad1()==NULL? NULL : PixelData->getTreeQuad1()->CloneTree());
    TTree* copyPixelTreeQuad2 = (PixelData->getTreeQuad2()==NULL? NULL : PixelData->getTreeQuad2()->CloneTree());

    if(enableVerbose) {
      if(copyPixelTreeQuad1!=NULL) copyPixelTreeQuad1->Print();
      if(copyPixelTreeQuad2!=NULL) copyPixelTreeQuad2->Print();
    }
    f_merge->Write();

    hasPixel_Data = true;
    has2Quad = ((copyPixelTreeQuad1!=NULL && copyPixelTreeQuad2!=NULL) ? true : false);
    delete copyPixelTreeQuad1;
    delete copyPixelTreeQuad2;
  }

  if(tof_filename   != "") {
    TOF_Tree TOFData(tof_filename);

    f_merge->cd();
    TTree* copyTOF_Tree = TOFData.getTree()->CloneTree();

    if(enableVerbose) {copyTOF_Tree->Print();}
    f_merge->Write();

    hasTOF_Data = true;
    delete copyTOF_Tree;
  }

  //quick analysis comparisons
  if(iOpt>0){
    GenerateMergeTree(f_merge);
    if(has2Quad) QuickAnalysis(f_merge);
  }    

  //add more with TOF
  f_merge->Write();
  f_merge->Close();

  gSystem->Exec("rm -f AutoDict*vector*vector*int*");
  gSystem->Exec("rm -f AutoDict*vector*vector*Short_t*");

  return 1;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

void GenerateMergeTree(TFile* f_merge){

  //entry counters
  Long64_t nEntriesStrip = 0;
  Long64_t StripEntryCounter = 0;

  Long64_t nEntriesQuad1 = 0;
  Long64_t Quad1EntryCounter = 0;
  Long64_t nEntriesQuad2 = 0;
  Long64_t Quad2EntryCounter = 0;

  Long64_t maxEntries = 0;

  //triggerID
  Long64_t triggerID;
  //strip content
  UInt_t eventNumber0;

  TTree* copyStripTree = NULL;
  vector<double> integral0, integral1, integral2, integral3, integral4, integral5;
  vector<int> length0, length1, length2, length3, length4, length5;
  vector<double> cog0, cog1, cog2, cog3, cog4, cog5;
  vector<double> sovern0, sovern1, sovern2, sovern3, sovern4, sovern5;

  vector<double> *r_integral0 = 0, *r_integral1 = 0, *r_integral2 = 0, *r_integral3 = 0, *r_integral4 = 0, *r_integral5 = 0;
  vector<int> *r_length0 = 0, *r_length1 = 0, *r_length2 = 0, *r_length3 = 0, *r_length4 = 0, *r_length5 = 0;
  vector<double> *r_cog0 = 0, *r_cog1 = 0, *r_cog2 = 0, *r_cog3 = 0, *r_cog4 = 0, *r_cog5 = 0;
  vector<double> *r_sovern0 = 0, *r_sovern1 = 0, *r_sovern2 = 0, *r_sovern3 = 0, *r_sovern4 = 0, *r_sovern5 = 0;

  //pixel content
  TTree* copyPixelTreeQuad1 = NULL;
  TTree* copyPixelTreeQuad2 = NULL;
  int triggerID_quad1, triggerID_quad2;

  vector<int> clstrSize_quad1, clstrSize_quad2;
  vector<float> clstrMeanX_quad1, clstrMeanX_quad2;
  vector<float> clstrMeanY_quad1, clstrMeanY_quad2;
  vector<float> clstrStdX_quad1, clstrStdX_quad2;
  vector<float> clstrStdY_quad1, clstrStdY_quad2;
  vector<float> clstrMeanX_quad1_mm, clstrMeanX_quad2_mm;
  vector<float> clstrMeanY_quad1_mm, clstrMeanY_quad2_mm;
  vector<float> clstrStdX_quad1_mm, clstrStdX_quad2_mm;
  vector<float> clstrStdY_quad1_mm, clstrStdY_quad2_mm;

  vector<int> *r_clstrSize_quad1 = 0, *r_clstrSize_quad2 = 0;
  vector<float> *r_clstrMeanX_quad1 = 0, *r_clstrMeanX_quad2 = 0;
  vector<float> *r_clstrMeanY_quad1 = 0, *r_clstrMeanY_quad2 = 0;
  vector<float> *r_clstrMeanX_quad1_mm = 0, *r_clstrMeanX_quad2_mm = 0;
  vector<float> *r_clstrMeanY_quad1_mm = 0, *r_clstrMeanY_quad2_mm = 0;
  vector<float> *r_clstrStdX_quad1 = 0, *r_clstrStdX_quad2 = 0;
  vector<float> *r_clstrStdY_quad1 = 0, *r_clstrStdY_quad2 = 0;
  vector<float> *r_clstrStdX_quad1_mm = 0, *r_clstrStdX_quad2_mm = 0;
  vector<float> *r_clstrStdY_quad1_mm = 0, *r_clstrStdY_quad2_mm = 0;

  //tof content


  f_merge->cd();
  TTree merge_tree("MiniPAN_Events","MiniPAN_Events");
  merge_tree.Branch("triggerID",&triggerID,"triggerID/L");
  
  //
  if(hasStrip_Data) {

    merge_tree.Branch("integral0",&integral0);merge_tree.Branch("integral1",&integral1);
    merge_tree.Branch("integral2",&integral2);merge_tree.Branch("integral3",&integral3);
    merge_tree.Branch("integral4",&integral4);merge_tree.Branch("integral5",&integral5);

    merge_tree.Branch("length0",&length0);merge_tree.Branch("length1",&length1);
    merge_tree.Branch("length2",&length2);merge_tree.Branch("length3",&length3);
    merge_tree.Branch("length4",&length4);merge_tree.Branch("length5",&length5);

    merge_tree.Branch("cog0",&cog0);merge_tree.Branch("cog1",&cog1);
    merge_tree.Branch("cog2",&cog2);merge_tree.Branch("cog3",&cog3);
    merge_tree.Branch("cog4",&cog4);merge_tree.Branch("cog5",&cog5);

    merge_tree.Branch("sovern0",&sovern0);merge_tree.Branch("sovern1",&sovern1);
    merge_tree.Branch("sovern2",&sovern2);merge_tree.Branch("sovern3",&sovern3);
    merge_tree.Branch("sovern4",&sovern4);merge_tree.Branch("sovern5",&sovern5);

    copyStripTree = stripData->getTree()->CloneTree();
    nEntriesStrip = copyStripTree->GetEntries();
    if(maxEntries < nEntriesStrip) {maxEntries = nEntriesStrip;}

    copyStripTree->SetBranchAddress("eventNumber0",&eventNumber0);

    copyStripTree->SetBranchAddress("integral0",&r_integral0), copyStripTree->SetBranchAddress("integral1",&r_integral1);
    copyStripTree->SetBranchAddress("integral2",&r_integral2), copyStripTree->SetBranchAddress("integral3",&r_integral3);
    copyStripTree->SetBranchAddress("integral4",&r_integral4), copyStripTree->SetBranchAddress("integral5",&r_integral5);

    copyStripTree->SetBranchAddress("length0",&r_length0), copyStripTree->SetBranchAddress("length1",&r_length1);
    copyStripTree->SetBranchAddress("length2",&r_length2), copyStripTree->SetBranchAddress("length3",&r_length3);
    copyStripTree->SetBranchAddress("length4",&r_length4), copyStripTree->SetBranchAddress("length5",&r_length5);

    copyStripTree->SetBranchAddress("cog0",&r_cog0), copyStripTree->SetBranchAddress("cog1",&r_cog1);
    copyStripTree->SetBranchAddress("cog2",&r_cog2), copyStripTree->SetBranchAddress("cog3",&r_cog3);
    copyStripTree->SetBranchAddress("cog4",&r_cog4), copyStripTree->SetBranchAddress("cog5",&r_cog5);

    copyStripTree->SetBranchAddress("sovern0",&r_sovern0), copyStripTree->SetBranchAddress("sovern1",&r_sovern1);
    copyStripTree->SetBranchAddress("sovern2",&r_sovern2), copyStripTree->SetBranchAddress("sovern3",&r_sovern3);
    copyStripTree->SetBranchAddress("sovern4",&r_sovern4), copyStripTree->SetBranchAddress("sovern5",&r_sovern5);

  }

  //
  if(hasPixel_Data){
    merge_tree.Branch("clstrSize_quad1",&clstrSize_quad1);merge_tree.Branch("clstrSize_quad2",&clstrSize_quad2);

    merge_tree.Branch("clstrMeanX_quad1",&clstrMeanX_quad1);merge_tree.Branch("clstrMeanX_quad2",&clstrMeanX_quad2);
    merge_tree.Branch("clstrMeanY_quad1",&clstrMeanY_quad1);merge_tree.Branch("clstrMeanY_quad2",&clstrMeanY_quad2);
    merge_tree.Branch("clstrMeanX_quad1_mm",&clstrMeanX_quad1_mm);merge_tree.Branch("clstrMeanX_quad2_mm",&clstrMeanX_quad2_mm);
    merge_tree.Branch("clstrMeanY_quad1_mm",&clstrMeanY_quad1_mm);merge_tree.Branch("clstrMeanY_quad2_mm",&clstrMeanY_quad2_mm);

    merge_tree.Branch("clstrStdX_quad1",&clstrStdX_quad1);merge_tree.Branch("clstrStdX_quad2",&clstrStdX_quad2);
    merge_tree.Branch("clstrStdY_quad1",&clstrStdY_quad1);merge_tree.Branch("clstrStdY_quad2",&clstrStdY_quad2);
    merge_tree.Branch("clstrStdX_quad1_mm",&clstrStdX_quad1_mm);merge_tree.Branch("clstrStdX_quad2_mm",&clstrStdX_quad2_mm);
    merge_tree.Branch("clstrStdY_quad1_mm",&clstrStdY_quad1_mm);merge_tree.Branch("clstrStdY_quad2_mm",&clstrStdY_quad2_mm);

    copyPixelTreeQuad1 = (PixelData->getTreeQuad1()==NULL? NULL : PixelData->getTreeQuad1()->CloneTree());
    copyPixelTreeQuad2 = (PixelData->getTreeQuad2()==NULL? NULL : PixelData->getTreeQuad2()->CloneTree());
    nEntriesQuad1 = (copyPixelTreeQuad1!=NULL ? copyPixelTreeQuad1->GetEntries() : 0);
    nEntriesQuad2 = (copyPixelTreeQuad2!=NULL ? copyPixelTreeQuad2->GetEntries() : 0);
    if(maxEntries < nEntriesQuad1) {maxEntries = nEntriesQuad1;}
    if(maxEntries < nEntriesQuad2) {maxEntries = nEntriesQuad2;}

    if(copyPixelTreeQuad1!=NULL){
      copyPixelTreeQuad1->SetBranchAddress("triggerID",&triggerID_quad1);
      copyPixelTreeQuad1->SetBranchAddress("clstrSize",&r_clstrSize_quad1);
      copyPixelTreeQuad1->SetBranchAddress("clstrMeanX",&r_clstrMeanX_quad1);
      copyPixelTreeQuad1->SetBranchAddress("clstrMeanY",&r_clstrMeanY_quad1);
      copyPixelTreeQuad1->SetBranchAddress("clstrMeanX_mm",&r_clstrMeanX_quad1_mm);
      copyPixelTreeQuad1->SetBranchAddress("clstrMeanY_mm",&r_clstrMeanY_quad1_mm);
      copyPixelTreeQuad1->SetBranchAddress("clstrStdX",&r_clstrStdX_quad1);
      copyPixelTreeQuad1->SetBranchAddress("clstrStdY",&r_clstrStdY_quad1);
      copyPixelTreeQuad1->SetBranchAddress("clstrStdX_mm",&r_clstrStdX_quad1_mm);
      copyPixelTreeQuad1->SetBranchAddress("clstrStdY_mm",&r_clstrStdY_quad1_mm);
    }

    if(copyPixelTreeQuad2!=NULL){
      copyPixelTreeQuad2->SetBranchAddress("triggerID",&triggerID_quad2);
      copyPixelTreeQuad2->SetBranchAddress("clstrSize",&r_clstrSize_quad2);
      copyPixelTreeQuad2->SetBranchAddress("clstrMeanX",&r_clstrMeanX_quad2);
      copyPixelTreeQuad2->SetBranchAddress("clstrMeanY",&r_clstrMeanY_quad2);
      copyPixelTreeQuad2->SetBranchAddress("clstrMeanX_mm",&r_clstrMeanX_quad2_mm);
      copyPixelTreeQuad2->SetBranchAddress("clstrMeanY_mm",&r_clstrMeanY_quad2_mm);
      copyPixelTreeQuad2->SetBranchAddress("clstrStdX",&r_clstrStdX_quad2);
      copyPixelTreeQuad2->SetBranchAddress("clstrStdY",&r_clstrStdY_quad2);
      copyPixelTreeQuad2->SetBranchAddress("clstrStdX_mm",&r_clstrStdX_quad2_mm);
      copyPixelTreeQuad2->SetBranchAddress("clstrStdY_mm",&r_clstrStdY_quad2_mm);
    }

  }

  ///// start merge /////
  if(hasStrip_Data){copyStripTree->GetEntry(0);}
  
  bool hasQuads[2] = {false,false};
  if(hasPixel_Data){
    if(copyPixelTreeQuad1!=NULL) {copyPixelTreeQuad1->GetEntry(0);hasQuads[0]=true;}
    if(copyPixelTreeQuad2!=NULL) {copyPixelTreeQuad2->GetEntry(0);hasQuads[1]=true;}
  }


  while(true){
    
    if( (StripEntryCounter>=nEntriesStrip) || ((Quad1EntryCounter>=nEntriesQuad1) && (Quad2EntryCounter>=nEntriesQuad2)) ) break;

    if(StripEntryCounter<nEntriesStrip) {copyStripTree->GetEntry(StripEntryCounter);}
    if(hasQuads[0] && Quad1EntryCounter<nEntriesQuad1) {copyPixelTreeQuad1->GetEntry(Quad1EntryCounter);}
    if(hasQuads[1] && Quad2EntryCounter<nEntriesQuad2) {copyPixelTreeQuad2->GetEntry(Quad2EntryCounter);}

    if((hasQuads[0] && hasQuads[1]) && !((triggerID_quad1 == triggerID_quad2) && (triggerID_quad1 == int(eventNumber0)))){
        if(triggerID_quad1<triggerID_quad2 || triggerID_quad1<int(eventNumber0)){Quad1EntryCounter++;}
        if(triggerID_quad2<triggerID_quad1 || triggerID_quad2<int(eventNumber0)){Quad2EntryCounter++;}
        if(int(eventNumber0)<triggerID_quad1 || int(eventNumber0)<triggerID_quad2){StripEntryCounter++;}
        continue;
    } else if (hasQuads[0] && !(triggerID_quad1 == int(eventNumber0))){
        if(triggerID_quad2<int(eventNumber0)){Quad2EntryCounter++;}
        if(int(eventNumber0)<triggerID_quad1){StripEntryCounter++;}
        continue;
    } else if (hasQuads[1] && !(triggerID_quad2 == int(eventNumber0))){
        if(triggerID_quad1<int(eventNumber0)){Quad1EntryCounter++;}
        if(int(eventNumber0)<triggerID_quad2){StripEntryCounter++;}
        continue;
    } else {
      if(StripEntryCounter<nEntriesStrip) {StripEntryCounter++;}
      if(hasQuads[0] && Quad1EntryCounter<nEntriesQuad1) {Quad1EntryCounter++;}
      if(hasQuads[1] && Quad2EntryCounter<nEntriesQuad2) {Quad2EntryCounter++;}
    }

    triggerID = (hasQuads[0] ? triggerID_quad1 : triggerID_quad2); 

    if(hasStrip_Data){
      integral0.clear(), integral1.clear(), integral2.clear(), integral3.clear(), integral4.clear(), integral5.clear();
      copy(r_integral0->begin(), r_integral0->end(),back_inserter(integral0));copy(r_integral1->begin(), r_integral1->end(),back_inserter(integral1));
      copy(r_integral2->begin(), r_integral2->end(),back_inserter(integral2));copy(r_integral3->begin(), r_integral3->end(),back_inserter(integral3));
      copy(r_integral4->begin(), r_integral4->end(),back_inserter(integral4));copy(r_integral5->begin(), r_integral5->end(),back_inserter(integral5));

      length0.clear(), length1.clear(), length2.clear(), length3.clear(), length4.clear(), length5.clear();
      copy(r_length0->begin(), r_length0->end(),back_inserter(length0));copy(r_length1->begin(), r_length1->end(),back_inserter(length1));
      copy(r_length2->begin(), r_length2->end(),back_inserter(length2));copy(r_length3->begin(), r_length3->end(),back_inserter(length3));
      copy(r_length4->begin(), r_length4->end(),back_inserter(length4));copy(r_length5->begin(), r_length5->end(),back_inserter(length5));

      cog0.clear(), cog1.clear(), cog2.clear(), cog3.clear(), cog4.clear(), cog5.clear();
      copy(r_cog0->begin(), r_cog0->end(),back_inserter(cog0));copy(r_cog1->begin(), r_cog1->end(),back_inserter(cog1));
      copy(r_cog2->begin(), r_cog2->end(),back_inserter(cog2));copy(r_cog3->begin(), r_cog3->end(),back_inserter(cog3));
      copy(r_cog4->begin(), r_cog4->end(),back_inserter(cog4));copy(r_cog5->begin(), r_cog5->end(),back_inserter(cog5));

      sovern0.clear(), sovern1.clear(), sovern2.clear(), sovern3.clear(), sovern4.clear(), sovern5.clear();
      copy(r_sovern0->begin(), r_sovern0->end(),back_inserter(sovern0));copy(r_sovern1->begin(), r_sovern1->end(),back_inserter(sovern1));
      copy(r_sovern2->begin(), r_sovern2->end(),back_inserter(sovern2));copy(r_sovern3->begin(), r_sovern3->end(),back_inserter(sovern3));
      copy(r_sovern4->begin(), r_sovern4->end(),back_inserter(sovern4));copy(r_sovern5->begin(), r_sovern5->end(),back_inserter(sovern5));
    }

    if(hasPixel_Data){
      if(hasQuads[0]){
        clstrSize_quad1.clear();
        copy(r_clstrSize_quad1->begin(), r_clstrSize_quad1->end(),back_inserter(clstrSize_quad1));

        clstrMeanX_quad1.clear();
        copy(r_clstrMeanX_quad1->begin(), r_clstrMeanX_quad1->end(),back_inserter(clstrMeanX_quad1));

        clstrMeanY_quad1.clear();
        copy(r_clstrMeanY_quad1->begin(), r_clstrMeanY_quad1->end(),back_inserter(clstrMeanY_quad1));

        clstrStdX_quad1.clear();
        copy(r_clstrStdX_quad1->begin(), r_clstrStdX_quad1->end(),back_inserter(clstrStdX_quad1));

        clstrStdY_quad1.clear();
        copy(r_clstrStdY_quad1->begin(), r_clstrStdY_quad1->end(),back_inserter(clstrStdY_quad1));

        clstrMeanX_quad1_mm.clear();
        copy(r_clstrMeanX_quad1_mm->begin(), r_clstrMeanX_quad1_mm->end(),back_inserter(clstrMeanX_quad1_mm));

        clstrMeanY_quad1_mm.clear();
        copy(r_clstrMeanY_quad1_mm->begin(), r_clstrMeanY_quad1_mm->end(),back_inserter(clstrMeanY_quad1_mm));

        clstrStdX_quad1_mm.clear();
        copy(r_clstrStdX_quad1_mm->begin(), r_clstrStdX_quad1_mm->end(),back_inserter(clstrStdX_quad1_mm));

        clstrStdY_quad1_mm.clear();
        copy(r_clstrStdY_quad1_mm->begin(), r_clstrStdY_quad1_mm->end(),back_inserter(clstrStdY_quad1_mm));
      }

      if(hasQuads[1]){
        clstrSize_quad2.clear();
        copy(r_clstrSize_quad2->begin(), r_clstrSize_quad2->end(),back_inserter(clstrSize_quad2));

        clstrMeanX_quad2.clear();
        copy(r_clstrMeanX_quad2->begin(), r_clstrMeanX_quad2->end(),back_inserter(clstrMeanX_quad2));

        clstrMeanY_quad2.clear();
        copy(r_clstrMeanY_quad2->begin(), r_clstrMeanY_quad2->end(),back_inserter(clstrMeanY_quad2));

        clstrStdX_quad2.clear();
        copy(r_clstrStdX_quad2->begin(), r_clstrStdX_quad2->end(),back_inserter(clstrStdX_quad2));

        clstrStdY_quad2.clear();
        copy(r_clstrStdY_quad2->begin(), r_clstrStdY_quad2->end(),back_inserter(clstrStdY_quad2));

        clstrMeanX_quad2_mm.clear();
        copy(r_clstrMeanX_quad2_mm->begin(), r_clstrMeanX_quad2_mm->end(),back_inserter(clstrMeanX_quad2_mm));

        clstrMeanY_quad2_mm.clear();
        copy(r_clstrMeanY_quad2_mm->begin(), r_clstrMeanY_quad2_mm->end(),back_inserter(clstrMeanY_quad2_mm));

        clstrStdX_quad2_mm.clear();
        copy(r_clstrStdX_quad2_mm->begin(), r_clstrStdX_quad2_mm->end(),back_inserter(clstrStdX_quad2_mm));

        clstrStdY_quad1_mm.clear(), clstrStdY_quad2_mm.clear();
        copy(r_clstrStdY_quad2_mm->begin(), r_clstrStdY_quad2_mm->end(),back_inserter(clstrStdY_quad2_mm));
      }
    }

    merge_tree.Fill();

  }

  f_merge->Write();


  if(hasStrip_Data) {delete copyStripTree;}
  if(hasPixel_Data) {
    delete copyPixelTreeQuad1;
    delete copyPixelTreeQuad2;
  }
  

}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

void QuickAnalysis(TFile* f_merge){

  f_merge->cd();

  Long64_t thisTriggerID =0;
  TTree analysis_tree("sanity_tree","Sanity Check Tree");
  if(hasTOF_Data) {analysis_tree.Branch("hasTOF_Data", &hasTOF_Data, "hasTOF_Data/O");}
  if(hasStrip_Data) {analysis_tree.Branch("hasStrip_Data", &hasStrip_Data, "hasStrip_Data/O");}
  if(hasPixel_Data) {analysis_tree.Branch("hasPixel_Data", &hasPixel_Data, "hasPixel_Data/O");}

  analysis_tree.Branch("triggerID", &thisTriggerID, "triggerID/L");
  TH2D clX_Quads = TH2D(Form("clX_Quads"),Form("clX_Quads"), 512, -0.5, 511.5, 512, -0.5, 511.5);
  TH2D clY_Quads = TH2D(Form("clY_Quads"),Form("clY_Quads"), 512, -0.5, 511.5, 512, -0.5, 511.5);
  TH2D clX_Quads_mm = TH2D(Form("clX_Quads_mm"),Form("clX_Quads_mm"), 600, 0, 30, 600, 0, 30);
  TH2D clY_Quads_mm = TH2D(Form("clY_Quads_mm"),Form("clY_Quads_mm"), 600, 0, 30, 600, 0, 30);
  TH2D StripQuad1_Y = TH2D(Form("StripQuad1_Y"),Form("StripQuad1_Y"), 512, -0.5, 511.5, 2048, -0.5, 2047.5);
  TH2D StripQuad2_Y = TH2D(Form("StripQuad2_Y"),Form("StripQuad2_Y"), 512, -0.5, 511.5, 2048, -0.5, 2047.5);
      
  if(hasStrip_Data && hasPixel_Data){ // (1)
    Long64_t DeltaframeCounter02, DeltaframeCounter24, DeltaframeCounter04;
    analysis_tree.Branch("DeltaframeCounter02", &DeltaframeCounter02, "DeltaframeCounter02/L");
    analysis_tree.Branch("DeltaframeCounter24", &DeltaframeCounter24, "DeltaframeCounter24/L");
    analysis_tree.Branch("DeltaframeCounter04", &DeltaframeCounter04, "DeltaframeCounter04/L");

    //strip
    TTree* copyStripTree = stripData->getTree();
    ULong64_t frameCounter[6];
    UInt_t eventNumber[6];
      
    vector<double> *cog0 = 0;
    map<Long64_t,vector<double> > map_cog0;
    vector<double> *cog5 = 0;
    map<Long64_t,vector<double> > map_cog5;

    copyStripTree->SetBranchAddress("frameCounter0",&frameCounter[0]);
    copyStripTree->SetBranchAddress("frameCounter1",&frameCounter[1]);
    copyStripTree->SetBranchAddress("frameCounter2",&frameCounter[2]);
    copyStripTree->SetBranchAddress("frameCounter3",&frameCounter[3]);
    copyStripTree->SetBranchAddress("frameCounter4",&frameCounter[4]);
    copyStripTree->SetBranchAddress("frameCounter5",&frameCounter[5]);
    copyStripTree->SetBranchAddress("eventNumber0",&eventNumber[0]);
    copyStripTree->SetBranchAddress("cog0",&cog0);
    copyStripTree->SetBranchAddress("cog5",&cog5);
      
    Long64_t stripTreeEntries = copyStripTree->GetEntries();
    for(Long64_t iEntry=0; iEntry<stripTreeEntries;iEntry++){
      copyStripTree->GetEntry(iEntry);
      thisTriggerID = eventNumber[0];

      DeltaframeCounter02 = frameCounter[0] - frameCounter[2];
      DeltaframeCounter24 = frameCounter[2] - frameCounter[4];
      DeltaframeCounter04 = frameCounter[0] - frameCounter[4];

      analysis_tree.Fill();        

      //
      if(cog0->size()==1){
        vector<double> to_vector;
        copy(cog0->begin(), cog0->end(),back_inserter(to_vector));
        map_cog0[thisTriggerID] = to_vector;}
      if(cog5->size()==1){
        vector<double> to_vector;
        copy(cog5->begin(), cog5->end(),back_inserter(to_vector));
        map_cog5[thisTriggerID] = to_vector;}
    }

    //pixel
    vector<float> *clstrMeanX1 = 0;
    vector<float> *clstrMeanY1 = 0;
    vector<float> *clstrMeanX2 = 0;
    vector<float> *clstrMeanY2 = 0;
    vector<float> *clstrMeanX1_mm = 0;
    vector<float> *clstrMeanY1_mm = 0;
    vector<float> *clstrMeanX2_mm = 0;
    vector<float> *clstrMeanY2_mm = 0;
    int triggerID1, triggerID2;
    TTree* copyPixelTree[2];
    
    if(PixelData->getTreeQuad1()!=NULL && PixelData->getTreeQuad2()!=NULL){
      copyPixelTree[0] = PixelData->getTreeQuad1();
      copyPixelTree[0]->SetBranchAddress("triggerID",&triggerID1);
      copyPixelTree[0]->SetBranchAddress("clstrMeanX",&clstrMeanX1);
      copyPixelTree[0]->SetBranchAddress("clstrMeanY",&clstrMeanY1);
      copyPixelTree[0]->SetBranchAddress("clstrMeanX_mm",&clstrMeanX1_mm);
      copyPixelTree[0]->SetBranchAddress("clstrMeanY_mm",&clstrMeanY1_mm);
      copyPixelTree[1] = PixelData->getTreeQuad2();
      copyPixelTree[1]->SetBranchAddress("triggerID",&triggerID2);
      copyPixelTree[1]->SetBranchAddress("clstrMeanX",&clstrMeanX2);
      copyPixelTree[1]->SetBranchAddress("clstrMeanY",&clstrMeanY2);
      copyPixelTree[1]->SetBranchAddress("clstrMeanX_mm",&clstrMeanX2_mm);
      copyPixelTree[1]->SetBranchAddress("clstrMeanY_mm",&clstrMeanY2_mm);

      Long64_t pixelTreeEntries = (copyPixelTree[0]->GetEntries() > copyPixelTree[1]->GetEntries() ? copyPixelTree[0]->GetEntries() : copyPixelTree[1]->GetEntries());
      Long64_t EntryCounter1 = 0;
      Long64_t EntryCounter2 = 0;

      for(Long64_t iEntry=0; iEntry<pixelTreeEntries;iEntry++){
        copyPixelTree[0]->GetEntry(iEntry+EntryCounter1);
        copyPixelTree[1]->GetEntry(iEntry+EntryCounter2);

        if(triggerID1 != triggerID2){
          if(triggerID1>triggerID2){EntryCounter2++;}
          if(triggerID2>triggerID1){EntryCounter1++;}
          continue;
        } 

        if(clstrMeanX1->size() == 1 && clstrMeanX2->size()==1){
          clX_Quads.Fill(clstrMeanX1->at(0),clstrMeanX2->at(0));
          clX_Quads_mm.Fill(clstrMeanX1_mm->at(0),clstrMeanX2_mm->at(0));
        }
        if(clstrMeanY1->size() == 1 && clstrMeanY2->size()==1){
          clY_Quads.Fill(clstrMeanY1->at(0),clstrMeanY2->at(0));
          clY_Quads_mm.Fill(clstrMeanY1_mm->at(0),clstrMeanY2_mm->at(0));
        }

        if(clstrMeanY1->size() == 1 && map_cog0.find(triggerID1) != map_cog0.end()){
          StripQuad1_Y.Fill(clstrMeanY1->at(0),map_cog0[triggerID1].at(0));
        }
        if(clstrMeanY2->size() == 1 && map_cog5.find(triggerID1) != map_cog5.end()){
          StripQuad2_Y.Fill(clstrMeanY2->at(0),map_cog5[triggerID1].at(0));
        }

      }
    }
  }

  analysis_tree.Write();
  clX_Quads.Write();clY_Quads.Write();
  clX_Quads_mm.Write();clY_Quads_mm.Write();
  StripQuad1_Y.Write();StripQuad2_Y.Write();
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

bool MergePixelQuadFiles(TFile* f_merge, string* pixel_quad){
  f_merge->cd();
  TFile *f_quad[2];

  for(int iQuad=0;iQuad<2;iQuad++){
      //check if file exists
      if(gSystem->AccessPathName(pixel_quad[iQuad].c_str())) continue; 
      f_quad[iQuad] = new TFile(pixel_quad[iQuad].c_str(), "READ");
      
      //check if file is corrupted
      if(f_quad[iQuad]->IsZombie() || f_quad[iQuad]->TestBit(TFile::kRecovered)) continue; 
    
      f_merge->cd();

      //XXX (add routine to see if tree exists)
      TTree* copyInfoTree_layer0 = f_quad[iQuad]->Get<TTree>("InfoTree_layer0")->CloneTree();
      copyInfoTree_layer0->SetName(Form("InfoTree_layer0_Quad%d",iQuad+1));
      TTree* copyInfoTree_layer1 = f_quad[iQuad]->Get<TTree>("InfoTree_layer1")->CloneTree();
      copyInfoTree_layer1->SetName(Form("InfoTree_layer1_Quad%d",iQuad+1));
      TTree* copyInfoTree_layer2 = f_quad[iQuad]->Get<TTree>("InfoTree_layer2")->CloneTree();
      copyInfoTree_layer2->SetName(Form("InfoTree_layer2_Quad%d",iQuad+1));
      //TTree* copyInfoTree_layer3 = f_quad[iQuad]->Get<TTree>("InfoTree_layer3")->CloneTree();
      ///copyInfoTree_layer3->SetName(Form("InfoTree_layer3_Quad%d",iQuad+1));

      TTree* copyanalysisDescription = f_quad[iQuad]->Get<TTree>("analysisDescription")->CloneTree();
      copyanalysisDescription->SetName(Form("analysisDescription_Quad%d",iQuad+1));

      //restructure clustered data
      TTree* clusteredData = f_quad[iQuad]->Get<TTree>("clusteredData")->CloneTree();

      int m_clstrSize, m_clstrHeight_ToT;
      ULong64_t m_clstrVolume_ToT, m_time_slice_counter;
      int m_tot[2000];
      double m_toa[2000];
      Short_t m_pix_x[2000], m_pix_y[2000];
      Short_t m_coincidence_group_size; 
      double m_delta_ToA, m_min_ToA;
      float m_clstrMeanX, m_clstrMeanY, m_clstrStdX, m_clstrStdY;
      float m_clstrMeanX_mm, m_clstrMeanY_mm, m_clstrStdX_mm, m_clstrStdY_mm;
      float m_clstrVolCentroidX, m_clstrVolCentroidY;
      double m_absolute_time_s;
      int m_triggerID;

      clusteredData->SetBranchAddress("clstrSize",         &m_clstrSize);
      clusteredData->SetBranchAddress("PixX",            &m_pix_x);
      clusteredData->SetBranchAddress("PixY",            &m_pix_y);
      clusteredData->SetBranchAddress("ToA",           &m_toa);
      clusteredData->SetBranchAddress("ToT",           &m_tot);
      clusteredData->SetBranchAddress("coincidence_group",     &m_time_slice_counter);
      clusteredData->SetBranchAddress("coincidence_group_size",  &m_coincidence_group_size);
      clusteredData->SetBranchAddress("clstrVolume_ToT",     &m_clstrVolume_ToT);
      clusteredData->SetBranchAddress("clstrHeight_ToT",     &m_clstrHeight_ToT);
      clusteredData->SetBranchAddress("delta_ToA",         &m_delta_ToA);
      clusteredData->SetBranchAddress("min_ToA",         &m_min_ToA);
      clusteredData->SetBranchAddress("clstrMeanX",        &m_clstrMeanX);
      clusteredData->SetBranchAddress("clstrMeanY",        &m_clstrMeanY);
      clusteredData->SetBranchAddress("clstrStdX",         &m_clstrStdX);
      clusteredData->SetBranchAddress("clstrStdY",         &m_clstrStdY);
      clusteredData->SetBranchAddress("clstrMeanX_mm",        &m_clstrMeanX_mm);
      clusteredData->SetBranchAddress("clstrMeanY_mm",        &m_clstrMeanY_mm);
      clusteredData->SetBranchAddress("clstrStdX_mm",         &m_clstrStdX_mm);
      clusteredData->SetBranchAddress("clstrStdY_mm",         &m_clstrStdY_mm);
      clusteredData->SetBranchAddress("clstrVolCentroidX",     &m_clstrVolCentroidX);
      clusteredData->SetBranchAddress("clstrVolCentroidY",     &m_clstrVolCentroidY);
      clusteredData->SetBranchAddress("abs_start_time_s",      &m_absolute_time_s);
      clusteredData->SetBranchAddress("triggerID",         &m_triggerID);

      f_merge->cd();
      cout << Form("clusteredData_Quad%d",iQuad+1) << endl;
      TTree* new_clusteredData = new TTree(Form("clusteredData_Quad%d",iQuad+1),Form("clusteredData_Quad%d",iQuad+1));

      //group clusters in vectors
      int v_triggerID;
      vector<int> v_clstrSize, v_clstrHeight_ToT;
      vector<ULong64_t> v_clstrVolume_ToT, v_time_slice_counter;
      vector<vector<int>> vv_tot;
      vector<vector<double>> vv_toa;
      vector<vector<Short_t>> vv_pix_x, vv_pix_y;
      vector<Short_t> v_coincidence_group_size; 
      vector<double> v_delta_ToA, v_min_ToA;
      vector<float> v_clstrMeanX, v_clstrMeanY, v_clstrStdX, v_clstrStdY;
      vector<float> v_clstrMeanX_mm, v_clstrMeanY_mm, v_clstrStdX_mm, v_clstrStdY_mm;
      vector<float> v_clstrVolCentroidX, v_clstrVolCentroidY;
      vector<double> v_absolute_time_s;

      new_clusteredData->Branch("triggerID",&v_triggerID,"triggerID/I");
      new_clusteredData->Branch("clstrSize", &v_clstrSize);
      new_clusteredData->Branch("clstrHeight_ToT",&v_clstrHeight_ToT);
      new_clusteredData->Branch("clstrVolume_ToT",&v_clstrVolume_ToT);
      new_clusteredData->Branch("time_slice_counter",&v_time_slice_counter);
      new_clusteredData->Branch("tot",&vv_tot);
      new_clusteredData->Branch("toa",&vv_toa);
      /*new_clusteredData->Branch("pix_x",&vv_pix_x);
      new_clusteredData->Branch("pix_y",&vv_pix_y);*/
      new_clusteredData->Branch("coincidence_group_size",&v_coincidence_group_size);
      new_clusteredData->Branch("delta_ToA",&v_delta_ToA);
      new_clusteredData->Branch("min_ToA",&v_min_ToA);
      new_clusteredData->Branch("clstrMeanX",&v_clstrMeanX);
      new_clusteredData->Branch("clstrMeanY",&v_clstrMeanY);
      new_clusteredData->Branch("clstrStdX",&v_clstrStdX);
      new_clusteredData->Branch("clstrStdY",&v_clstrStdY);
      new_clusteredData->Branch("clstrMeanX_mm",&v_clstrMeanX_mm);
      new_clusteredData->Branch("clstrMeanY_mm",&v_clstrMeanY_mm);
      new_clusteredData->Branch("clstrStdX_mm",&v_clstrStdX_mm);
      new_clusteredData->Branch("clstrStdY_mm",&v_clstrStdY_mm);
      new_clusteredData->Branch("clstrVolCentroidX",&v_clstrVolCentroidX);
      new_clusteredData->Branch("clstrVolCentroidY",&v_clstrVolCentroidY);
      new_clusteredData->Branch("absolute_time_s",&v_absolute_time_s);

      //TH1 to fit profile
      TF1 *f_gaus = new TF1("double_gaus","gaus(0)+gaus(3)",-0.5, 511.5); //((TF1*)(gROOT->GetFunction("gaus(0)+gaus(3)")));
      TF1 *f_lin  = new TF1("linear","[0]+[1]*x",-0.5, 511.5);


      TH1D x_hit = TH1D(Form("x_profile_%d",iQuad+1),Form("x_profile_%d",iQuad+1), 512, -0.5, 511.5);
      TH1D y_hit = TH1D(Form("y_profile_%d",iQuad+1),Form("y_profile_%d",iQuad+1), 512, -0.5, 511.5);

      //fill tree
      Long64_t nEntries = clusteredData->GetEntries();
      double pix_x_count[2] = {0.};
      double pix_y_count[2] = {0.};
      for(Long64_t ii=0;ii<nEntries;ii++){
        clusteredData->GetEntry(ii);

        for(int iHit=0;iHit<m_clstrSize;iHit++){
          if(!(m_pix_x[iHit]==255 || m_pix_x[iHit]==256 || m_pix_y[iHit]==255 || m_pix_y[iHit]==256)) {
            x_hit.Fill(m_pix_x[iHit]);
            y_hit.Fill(m_pix_y[iHit]);
          } else {
            if((m_pix_x[iHit]==255 || m_pix_x[iHit]==256) && (m_pix_y[iHit]==255 || m_pix_y[iHit]==256)) continue;
            if(m_pix_x[iHit]==255) pix_x_count[0]++;
            if(m_pix_x[iHit]==256) pix_x_count[1]++;
            if(m_pix_y[iHit]==255) pix_y_count[0]++;
            if(m_pix_y[iHit]==256) pix_y_count[1]++;
          }
        }

        if(m_triggerID>0) {
          v_triggerID = m_triggerID;
          v_clstrSize.push_back(m_clstrSize);
          v_clstrHeight_ToT.push_back(m_clstrHeight_ToT);
          v_clstrVolume_ToT.push_back(m_clstrVolume_ToT);
          v_time_slice_counter.push_back(m_time_slice_counter);
          v_coincidence_group_size.push_back(m_coincidence_group_size);
          v_delta_ToA.push_back(m_delta_ToA);
          v_min_ToA.push_back(m_min_ToA);
          v_clstrMeanX.push_back(m_clstrMeanX);
          v_clstrMeanY.push_back(m_clstrMeanY);
          v_clstrStdX.push_back(m_clstrStdX);
          v_clstrStdY.push_back(m_clstrStdY);
          v_clstrMeanX_mm.push_back(m_clstrMeanX_mm);
          v_clstrMeanY_mm.push_back(m_clstrMeanY_mm);
          v_clstrStdX_mm.push_back(m_clstrStdX_mm);
          v_clstrStdY_mm.push_back(m_clstrStdY_mm);
          v_clstrVolCentroidX.push_back(m_clstrVolCentroidX);
          v_clstrVolCentroidY.push_back(m_clstrVolCentroidY);
          v_absolute_time_s.push_back(m_absolute_time_s);

          vector<int> v_tot;
          vector<double> v_toa;
          vector<Short_t> v_pix_x, v_pix_y;
          for(int iHit=0;iHit<m_clstrSize;iHit++){
            v_pix_x.push_back(m_pix_x[iHit]);
            v_pix_y.push_back(m_pix_y[iHit]);
            v_toa.push_back(m_toa[iHit]);
            v_tot.push_back(m_tot[iHit]);
          }

          vv_pix_x.push_back(v_pix_x);
          vv_pix_y.push_back(v_pix_y);
          vv_tot.push_back(v_tot);
          vv_toa.push_back(v_toa);

        } 

        if(m_triggerID<0 && v_clstrSize.size()>0){//write & clear

          new_clusteredData->Fill();

          //clear for new cluster
          v_clstrSize.clear(), v_clstrHeight_ToT.clear(), v_clstrVolume_ToT.clear(), v_time_slice_counter.clear();
          v_coincidence_group_size.clear(), v_delta_ToA.clear(), v_min_ToA.clear();
          v_clstrMeanX.clear(), v_clstrMeanY.clear(), v_clstrStdX.clear(), v_clstrStdY.clear(), v_clstrVolCentroidX.clear(), v_clstrVolCentroidY.clear();
          v_clstrMeanX_mm.clear(), v_clstrMeanY_mm.clear(), v_clstrStdX_mm.clear(), v_clstrStdY_mm.clear();
          v_absolute_time_s.clear(), vv_pix_x.clear(), vv_pix_y.clear(), vv_tot.clear(), vv_toa.clear();

        }

      }


      delete clusteredData;

      float x_gap, y_gap;
      double *param;

      //x-axis (normalize and fit)
      double quad_ratio_x = x_hit.GetBinContent(254+1)/x_hit.GetBinContent(257+1);
      for(int iBin =0; iBin<=x_hit.GetNbinsX();iBin++){
        x_hit.SetBinContent(iBin,x_hit.GetBinContent(iBin)/(iBin<256 ? quad_ratio_x : 1.));
      }
      pix_x_count[0] /= quad_ratio_x;

      f_gaus->SetParameter(1,x_hit.GetMean());
      f_gaus->SetParameter(2,150);
      f_gaus->SetParLimits(2,100,200);
      f_gaus->SetParameter(4,x_hit.GetMean());
      f_gaus->SetParameter(5,75);
      f_gaus->SetParLimits(5,50,100);

      x_hit.Fit(f_lin,"RQ");
      float chi2_lin = f_lin->GetChisquare();
      x_hit.Fit(f_gaus,"RQ");
      float chi2_gaus = f_gaus->GetChisquare();
      param = f_gaus->GetParameters();

      if(chi2_lin>chi2_gaus){
        param = f_gaus->GetParameters();
        x_gap = (pix_x_count[0]+pix_x_count[1]) / ((pix_x_count[0]>0 ? 1 : 0)*gaus(255,param)+(pix_x_count[1]>0 ? 1 : 0)*gaus(256,param));
        x_hit.Fit(f_gaus,"RQ");
      } else {
        param = f_lin->GetParameters();
        x_gap = (pix_x_count[0]+pix_x_count[1]) / ((pix_x_count[0]>0 ? 1 : 0)*linear(255,param)+(pix_x_count[1]>0 ? 1 : 0)*linear(256,param));
        x_hit.Fit(f_lin,"RQ");
      }

      //y-axis
      double quad_ratio_y = y_hit.GetBinContent(254+1)/y_hit.GetBinContent(257+1);
      for(int iBin =0; iBin<=y_hit.GetNbinsX();iBin++){
        y_hit.SetBinContent(iBin,y_hit.GetBinContent(iBin)/(iBin<256 ? quad_ratio_y : 1.));
      }
      pix_y_count[0] /= quad_ratio_y;

      f_gaus->SetParameter(1,y_hit.GetMean());
      f_gaus->SetParameter(2,150);
      f_gaus->SetParLimits(2,100,200);
      f_gaus->SetParameter(4,x_hit.GetMean());
      f_gaus->SetParameter(5,75);
      f_gaus->SetParLimits(5,50,100);
      
      y_hit.Fit(f_lin,"RQ");
      chi2_lin = f_lin->GetChisquare();
      y_hit.Fit(f_gaus,"RQ");
      chi2_gaus = f_gaus->GetChisquare();

      if(chi2_lin>chi2_gaus){
        param = f_gaus->GetParameters();
        y_gap = (pix_y_count[0]+pix_y_count[1]) / ((pix_y_count[0]>0 ? 1 : 0)*gaus(255,param)+(pix_y_count[1]>0 ? 1 : 0)*gaus(256,param));
        y_hit.Fit(f_gaus,"RQ");
      } else {
        param = f_lin->GetParameters();
        y_gap = (pix_y_count[0]+pix_y_count[1]) / ((pix_y_count[0]>0 ? 1 : 0)*linear(255,param)+(pix_y_count[1]>0 ? 1 : 0)*linear(256,param));
        y_hit.Fit(f_lin,"RQ");
      }

      cout << x_gap << " " << y_gap << endl;

      TTree* pixelGap = new TTree(Form("pixelGap_Quad%d",iQuad+1),Form("pixelGap_Quad%d",iQuad+1));
      pixelGap->Branch("x_gap",&x_gap,"x_gap/F");
      pixelGap->Branch("y_gap",&y_gap,"y_gap/F");
      pixelGap->Fill();

      pixelGap->Write();
      f_merge->Write(); 

      f_quad[iQuad]->Close();

      delete copyInfoTree_layer0;
      delete copyInfoTree_layer1;
      delete copyInfoTree_layer2;
      //delete copyInfoTree_layer3;
      delete new_clusteredData;
      delete copyanalysisDescription;
      delete pixelGap;

  }

  return true;

}

double gaus(double x, double* par){
  return par[0]*exp(-0.5*pow(((x-par[1])/par[2]),2.))+par[3]*exp(-0.5*pow(((x-par[4])/par[5]),2.));
}

double linear(double x, double* par){
  return par[0] + x*par[1];
}
