"""
This is package of function to analyze data from strip and pixel detectors in PAN and 
find coincident clusters. 

Contents 
-------- 
AllCoincidences : class [line 42] 
    This class finds coincidences between strip detectors and pixel detectors. 
    Since triggers from pixel data and triggers from strip detectors may be shifted, 
    this class can shift the triggers. However, it cannot find the exact value of 
    the shift. You need to find the shift manually or guess it and give it to the class. 
ExtractData() : function [line 238] 
    This function loads data of one kind (for example energies of clusters) from 
    one detector whose name you have to provide. Input is AllCoincidences object. 
    It returns a list with the desired data. 
SaveRootCoinFile() : function [line 331] 
    This function saves a ROOT file with coincident clusters data. Input can is 
    AllCoincidences object. 
LoadRootCoinFile() : function [line 539] 
    This function loads a ROOT file with coincident clusters data. It returns 
    AllCoincidences object. 
    
Notes and warnings
------------------ 
I got quite excited about raising error messages and I used them a lot. They may make the 
code harder to understand. I am sorry. 

Feel free to modify functions and classes in this package, so that they fit your needs. 

In case of questions, you can contact me: 
Jindra Jelinek 
pavoucek007@email.cz 
"""

import numpy as np 
import math 
import ROOT 
import copy 
import time 
import array 

class AllCoincidences: 
    """
    This class stores clusters data from strip detectors and/or pixel detectors. 
    
    Class instances 
    --------------- 
    AllCoincidences.Names : list 
        List with string names of all detectors. 
    AllCoincidences.Types : list 
        List with string types of the detectors named in AllCoincidences.Names list. String 
        type can be either 'strip' or 'quad'. 
    AllCoincidences.CommonEntries : list 
        List with entry numbers. 
    AllCoincidences.Data : list 
        List with dictionaries that contain data from the strip and pixel detectors. This 
        list is filled only if you run AllCoincidences.calculatedata(). Rhen the list is 
        filled, it contains dictionaries with information about the coincident clusters in 
        the detectors. Let the strip detectors be named ["dets1", ..., "detsN"] and quad 
        detectors be named ["detq1", ..., "detqM"]. Keys of the dictionary are: 
        Dictionary = {
            "trigNo" : int entry number 
            "trigtime" : float time of the trigger (in ns) 
            "fCounter" : list with int frame counters in the respective strip detectors (some of them 
                are the same). 
            "dets1" : list with items [COG, ADC, LEN, SON] (centre of gravity, energy, length, signal 
                over noise of a cluster); every item describes one cluster at this entry in detector dets1
            ..... 
            "detsN" : list with items [COG, ADC, LEN, SON] (centre of gravity, energy, length, signal 
                over noise of a cluster); every item describes one cluster at this entry in detecto detsN 
            "detq1" : list with items [x, y, ToA, E] (x- and y-position of energy-weighted 
                centroid, time of arrival in ns, total energy of the cluster); 
                every item describes one cluster at this entry in detq1 
            ..... 
            "detqM" : list with items [x, y, ToA, E] (x- and y-position of energy-weighted 
                centroid, time of arrival in ns, total energy of the cluster); 
                every item describes one cluster at this entry in detqM 
        }
        
    Class methods 
    ------------- 
    AllCoincidences.calculatedata() : function 
        This function merges data from strip and pixel detectors and fills this class instance 
        with merged data. It can also take into account trigger shift between the two data files. 
    """
    def __init__(self):
        
        self.Names = [] # Names of the detectors. 
        self.Types = [] # Types of the detectors. 
        self.CommonEntries = [] # Numbers of entries when there were coincidences for all detectors. 
        self.Data = [] # Coincident clusters data. 
        
    def calculatedata(self, StripCoin, QuadCoin, shift=0, change_shift=np.inf, progress=True): 
        """
        This function merges data from strip and pixel detectors and fills this class instance 
        with merged data. It can also take into account trigger shift between the two data files. 
        
        Parameters 
        ---------- 
        StripCoin : detector7.AllCoincidences 
            Coincident clusters data from the strip detectors. 
        QuadCoin : detector7.AllCoincidences 
            Coincident clusters data from only pixel quad detectors. 
        shift : int or list or tuple (optional) 
            It may happen that trigger 0 in QuadCoin doesn't correspond to trigger 0 in 
            StripCoin - the triggers are shifted. Number 'shift' determines how the trigger 
            number from QuadCoin should be changed so that corresponding events in QuadCoin 
            and StripCoin have the same number trigger_quad -> trigger_quad + shift. 
            It can also happen that there were some data packets lost in the strip detectors. 
            In that case, input 'shift' must be a list or tuple with multiple integers. We 
            will start with the shift value given by the first item, but when trigger number 
            in StripCoin reaches 'change_shift' value, we will change to the next value in 
            'shift' list. Default value is shift=0 (i.e. we assume there was no trigger shift). 
        change_shift : int or list or tuple (optional) 
            If there were some data lost in strip detectors, 'change_shift' contains one number 
            or a list of numbers (corresponding to trigger number in StripCoin) where we should 
            change trigger shift (given by variable 'shift' (see above)) to the next value. 
            Default value is change_shift=numpy.inf (i.e. we don't change the shift at all). 
        progress : bool (optional)  
            Tick True if you want to print progress bar. Default value is progress=True (i.e. 
            the progress bar will be printed). 
        """
        start = time.time() 
        
        # Check if (some) of the inputs are correct. 
        # --------------------------------------------------------------------------------------------------
        if type(StripCoin)!=AllCoincidences: 
            raise TypeError("Invalid input at 'StripCoin'. It must be of type detector7.AllCoincidences.")
            
        if type(QuadCoin)!=AllCoincidences: 
            raise TypeError("Invalid input at 'QuadCoin'. It must be of type detector7.AllCoincidences.")

        if type(shift)==int: 
            shift=[shift] 
        elif type(shift)==list or type(shift)==tuple: 
            for i, item in enumerate(shift): 
                if type(item)!=int: 
                    raise TypeError("Input variable 'shift' must be int or list or tuple with integers.")
        else: 
            raise TypeError("Input variable 'shift' must be int or list or tuple with integers.")
        
        is_error=False
        if type(change_shift)==int or type(change_shift)==float: 
            change_shift=[change_shift] 
        elif type(change_shift)==list or type(change_shift)==tuple: 
            item0 = change_shift[0]
            for i, item in enumerate(change_shift): 
                if type(item)==int or type(change_shift)==float: 
                    if item < item0: 
                        is_error=True 
                    else: 
                        item0=item 
                else: 
                    is_error=True
        else: 
            is_error=True
        
        if is_error: 
            TypeError("Input variable 'change_shift' must be int, float, list or tuple with floats or integers in ascending order.") 
        
        change_shift.append(np.inf) 
        
        if len(change_shift)<len(shift): 
            raise ValueError("You didn't input enough values in 'shift' list.") 
        # --------------------------------------------------------------------------------------------------

        shift = iter(shift)
        shift_now = next(shift)
        change_shift = iter(change_shift)
        change = next(change_shift) 
            
        for i, name in enumerate(StripCoin.Names): 
            if type(name)==str: 
                self.Names.append(name) 
                self.Types.append("strip") 
            else: 
                raise TypeError("Unexpected input at StripCoin.Names={}. All inputs must be strings.".format(StripCoin.Names))
        for name in QuadCoin.Names: 
            if type(name)==str: 
                self.Names.append(name) 
                self.Types.append("quad")
            else: 
                raise TypeError("Unexpected input at QuadCoin.Names={}. All inputs must be strings.".format(StripCoin.Names))
                
        for j, name1 in enumerate(self.Names): 
            for k in range(j+1,len(self.Names)): 
                if name1==self.Names[k]: 
                    raise ValueError("Two new detector names are equal: '{}'.".format(name1))

        StripIter = iter(StripCoin.CommonEntries) 
        k = 0

        events = next(StripIter) 

        ll = len(QuadCoin.Data)
        n = int(math.ceil(ll/100))
        multiple = 0

        for i, coinp in enumerate(QuadCoin.Data): 
            if events < change: 
                eventp = coinp["trigNo"] + shift_now 
            else: 
                change = next(change_shift)
                shift_now = next(shift) 
                eventp = coinp["trigNo"] + shift_now 

            while events < eventp: 
                try: 
                    events = next(StripIter) 
                    k += 1
                except StopIteration: 
                    break 

            if events==eventp:
                Dictionary = dict.fromkeys(self.Names, []) 
                Dictionary["trigNo"] = events 
                Dictionary["trigtime"] = coinp["trigtime"] 
                Dictionary["fCounter"] = StripCoin.Data[k]["fCounter"] 
                for name in StripCoin.Names: 
                    Dictionary[name] = StripCoin.Data[k][name] 
                for name in QuadCoin.Names: 
                    Dictionary[name] = coinp[name]
                self.Data.append(Dictionary) 
                self.CommonEntries.append(events)
                try: 
                    events = next(StripIter) 
                    k += 1
                except StopIteration: 
                    break 

            if i == multiple*n and progress==True: 
                multiple += 1 
                print('|'*(multiple) + ' {} % triggers, {} s   '.format(multiple, round(time.time() - start, 2)), end='\r') 
                
        if progress==True: 
            print('|'*(100) + ' {} % triggers, {} s   '.format(100, round(time.time() - start, 2))) 
            
def ExtractData(AllCoin, data_type, DetName=None, fCounterIndex=None): 
    """
    This function extracts data of a given kind from one detector and returns the data in 
    a form of a list. 
    
    Useful if you want to make histograms of data. 
    
    Parameters 
    ---------- 
    AllCoin : detector5.AllCoincidences 
        Object with clusters data from the detectors. 
    data_type : string 
        Feature that you want to extract: 
        "trigNo", "trigtime", "x", "y", "ToA", "E", "size" for a quad detector 
        "trigNo", "fCounter", "cog", "integral", "length" for a strip detector 
    DetName : string (optional)
        If you want to extract "x", "y", "ToA", "E", "size" from a quad detector or 
        "cog", "integral", "length" from a strip detector, you must provide the detector 
        name. This doesn't apply if AllCoin is detector5.LoadData type where there is 
        data from just one detector. 
    fCounterIndex : int (optional)
        If AllCoin is type detector5.FindCoinStrips or detector5.AllCoincidences and you 
        want to extract frame counters, you must give an integer number which frame counter 
        you want to extract, since these objects hold data from multiple boards with different 
        frame counters. You do not need to provide fCounterIndex otherwise. 
        
    Returns 
    ------- 
    DataList : list 
        List with the extracted values. 
    """ 
    
    DataList = [] 
        
    if data_type in ["cog", "integral", "length", "x", "y", "ToA", "E", "size"]: 
        if type(DetName)!=str: 
            raise TypeError("You want to extract detector specific data, but you didn't provide the detector name!")
        else: 
            try: 
                index = AllCoin.Names.index(DetName)
                if type(AllCoin)==AllCoincidences: 
                    det_type = AllCoin.Types[index] 
            except ValueError: 
                raise ValueError("Detector {} doesn't exist. Detector names are {}.".format(DetName, AllCoin.Names)) 
    
    if data_type in ["trigNo", "trigtime"]: 
        try: 
            for Entry in AllCoin.Data: 
                DataList.append(Entry[data_type])
        except KeyError: 
            raise KeyError("Unsupported 'data_type={}' for input 'AllCoin' of type {}.".format(data_type, type(AllCoin)))

    elif data_type=="fCounter": 
        if type(AllCoin.Data[0]["fCounter"])==int or type(AllCoin.Data[0]["fCounter"])==float: 
            for Entry in AllCoin.Data: 
                DataList.append(Entry["fCounter"]) 
        elif type(fCounterIndex)==int: 
            if 0 <= fCounterIndex < len(AllCoin.Data[0]["fCounter"]): 
                for Entry in AllCoin.Data: 
                    DataList.append(Entry["fCounter"][fCounterIndex]) 
            else: 
                raise ValueError("'fCounterIndex' must be an integer number between 0 (inclusive) and {} (exclusive).".format(len(AllCoin.Data[0]["fCounter"])))
        else: 
            raise ValueError("'fCounterIndex' must be an integer number between 0 (inclusive) and {} (exclusive).".format(len(AllCoin.Data[0]["fCounter"])))

    elif det_type=="strip": 
        List = ["cog", "integral", "length"]
        if data_type in List: 
            idx = List.index(data_type)
            for Entry in AllCoin.Data: 
                for row in Entry[DetName]: 
                    DataList.append(row[idx]) 
        else: 
            raise ValueError("Invalid value in 'data_type'. Permitted entries for strip detector are ['trigNo', 'fCounter', 'cog', 'integral', 'length']. Permitted entries for pixel detector are ['trigNo', 'trigtime', 'x', 'y', 'ToA', 'E', 'size'].")
    elif det_type=="quad": 
        List = ["x", "y", "ToA", "E", "size"] 
        if data_type in List: 
            idx = List.index(data_type)
            for Entry in AllCoin.Data: 
                for row in Entry[DetName]: 
                    DataList.append(row[idx]) 
        else: 
            raise ValueError("Invalid value in 'data_type'. Permitted entries for pixel detector are ['trigNo', 'trigtime', 'x', 'y', 'ToA', 'E', 'size']. Permitted entries for strip detector are ['trigNo', 'fCounter', 'cog', 'integral', 'length'].")

    else: 
        raise ValueError("""Invalid input either in 'DetName' or in 'data_type' or 'det_type'. Permitted inputs in 'DetName' are: 
        {}. 
        Permitted inputs in 'data_type' are: 
        ["trigNo", "trigtime", "fCounter", "cog", "integral", "length", "x", "y", "ToA", "E", "size"].
        The known detector types are ['strip', 'quad']. You used {}.""".format(AllCoin.Names, det_type))

    return(DataList)
        
def LoadRootCoinFile(filepath, DetNames, DetTypes, creator='Jindra', print_=True): 
    """
    This function loads data measured by strip detectors from ROOT file. 
    It can analyze also data from more than 1 detector. 
    
    Parameters
    ----------
    filepath : string 
        Path to the ROOT file with data. 
    colnumber : int or list or tuple 
        Contains numbers of strips in the detectors. If all detectors had 
        the same number fo columns, you can write an integer. If detectors 
        had different number of columns, colnumber must be list or tuple 
        with integers and be of the same length as number of detectors. 
    DetNames : string or list or tuple 
        Contains string name(s) of all detectors (as they are named in the ROOT file). 
    DetTypes : string or list or tuple 
        Contains string types ('quad' or 'strip') of the detectors from DetNames list. 
    creator : string (optional)
        Name of the creator of the .ROOT file. Different people use different naming 
        conventions of the branches. The supported creators now are:  
            - 'Daniil - sync' (for .ROOT files with coincidences from all strip detectors)
            - 'Jindra' (for .ROOT file created by functions from detector5 package) 
            - 'Daniil - new' (for newer .ROOT files containing also 'sovern' branch) 
            - 'Benedikt' (for .ROOT files with data from pixel detectors created by Benedikt) 
        Default value is creator='Jindra'. You can easily add a new creator yourself. 
    print_ : bool (optional)
        Tick True if you want to print message about the data. Default value 
        is True - message will be printed. 
    
    Returns 
    -------
    Output : detector5.AllCoincidences  
        Object with all clusters data. 
        
    Warning! 
    Please note that natural coordinates of quad detector are rotated with respect to coordinates 
    of strip detectors! Whereas in .ROOT file all coordinates x and y correspond to each other, 
    this function loads pixel coordinate labelled as x in .ROOT file as y and pixel coordinate 
    labelled as y in .ROOT file as x. 
    """ 
    Ndet = len(DetNames) 
    start = time.time()
    
    # Check if (some of the) inputs are correct: 
    # ------------------------------------------------------------------------------------------------
    try: 
        if not filepath.endswith('.root'): 
            raise ValueError('The file {} is not a ROOT file!'.format(filepath))
        myfile = ROOT.TFile(filepath) 
    except OSError: 
        raise OSError("""File {} does not exist.
            Check again if you spelled name of the file correctly.""".format(filepath))
    
    if type(DetNames)==str: 
        DetNames = [DetNames] 
        
    if type(DetNames)==list or type(DetNames)==tuple: 
        for i, item in enumerate(DetNames): 
            if type(item)!=str: 
                raise TypeError("""All items in 'DetNames' input must be strings. 
            However, input 'DetNames[{}] is {}.'""".format(i, type(item))) 
    else: 
        raise TypeError("""Input 'DetNames' must be a string, a list with strings or a tuple with strings.
            You inputed type {}.""".format(type(DetNames))) 
    
    for i, item in enumerate(DetNames): 
        for j in range(i+1, len(DetNames)): 
            item2 = DetNames[j] 
            if item2==item: 
                raise ValueError("""All detectors must have different names. 
            Detectors 'DetNames[{}]' and 'DetNames[{}]' are both named {}.""".format(i, j, item)) 
                
    if type(DetTypes)==str: 
        DetTypes = [DetTypes] 
        
    if type(DetTypes)==list or type(DetTypes)==tuple: 
        if len(DetTypes)!=len(DetNames): 
            raise ValueError("Number of detector types 'DetTypes' must be the same as number of detector names 'DetNames'.")
        for i, item in enumerate(DetTypes): 
            if type(item)!=str: 
                raise TypeError("""All items in 'DetTypes' input must be strings. 
            However, input 'DetTypes[{}] is {}.'""".format(i, type(item))) 
    else: 
        raise TypeError("""Input 'DetTypes' must be a string, a list with strings or a tuple with strings.
            You inputed type {}.""".format(type(DetTypes))) 
        
    Br_Names_strip = [] 
    Br_Names_quad = []    
    
    if creator=='Jindra': 
        tree_name = 'CoincidentClusters' 
        entry_name = "entry" 
        trigtime_name = "trigger_time"
        for i, name in enumerate(DetNames): 
            det_type = DetTypes[i] 
            if det_type=='strip': 
                VarNames = [name+'_cog', name+'_integral', name+'_len', name+'_fCounter', name] 
                Br_Names_strip.append(VarNames) 
            elif det_type=='quad': 
                VarNames = [name+'_y', name+'_x', name+'_ToA', name+'_E', name+'_size', name]
                Br_Names_quad.append(VarNames) 
            else: 
                raise ValueError("Unknown detector type {} for creator 'Jindra'. Allowed types are 'strip' and 'quad'.".format(det_type))
    elif creator=="Daniil - sync": 
        tree_name="sync_clusters_tree" 
        for i, name in enumerate(DetNames): 
            det_type = DetTypes[i] 
            if det_type=='strip': 
                VarNames = ['cog'+str(i), 'integral'+str(i), 'length'+str(i), 'frameCounter'+str(i), name] 
                Br_Names_strip.append(VarNames) 
            else: 
                raise ValueError("Unknown detector type {} for creator 'Daniil - sync'. Allowed types are 'strip'.".format(det_type)) 
    elif creator=="Daniil - new": 
        entry_name = "eventNumber0"
        tree_name="sync_clusters_tree" 
        for i, name in enumerate(DetNames): 
            det_type = DetTypes[i] 
            if det_type=='strip': 
                VarNames = ['cog'+str(i), 'integral'+str(i), 'length'+str(i), 'frameCounter'+str(i), 'sovern'+str(i), name] 
                Br_Names_strip.append(VarNames) 
            else: 
                raise ValueError("Unknown detector type {} for creator 'Daniil - new'. Allowed types are 'strip'.".format(det_type)) 
    elif creator=="Benedikt": 
        tree_name = "pixel_data" 
        entry_name = "trigger_idx" 
        trigtime_name = "ro_start_s"
        for i, name in enumerate(DetNames): 
            det_type = DetTypes[i] 
            if det_type=='quad': 
                VarNames = ['y_pix_'+str(i+1), 'x_pix_'+str(i+1), 't_ns_'+str(i+1), 'e_keV_'+str(i+1), name] 
                Br_Names_quad.append(VarNames) 
            else: 
                raise ValueError("Unknown detector type {} for creator 'Benedikt'. Allowed types are 'strip'.".format(det_type))
    # Add a new creator if you need! 
    else: 
        raise ValueError("Unknown creator of .ROOT file {}.".format(creator))
    # ---------------------------------------------------------------------------------------------------
    
    clutree = myfile.Get(tree_name) # Extract the ROOT tree. 

    if clutree==0: # If there is no tree ... 
        raise ValueError('The tree has not been found!') # ... raise error. 

    nentries = clutree.GetEntries() # Number of entries in the ROOT tree. 
    
    # Variables to monitor progress. 
    n = int(math.ceil(nentries / 100.0))
    multiple = 0 
    
    if print_==True: # If you want to print message about the data: 
        print('The tree from {} detectors has {} entries.'.format(len(DetNames), nentries)) 
    
    Output = AllCoincidences() # Output class. 
    Output.Names = DetNames 
    Output.Types = DetTypes
    for entry in range(nentries): # Go through all entries in the clusters tree. 
        clutree.GetEntry(entry) 
        EntryData = dict.fromkeys(DetNames, None) 
        for name in DetNames: 
            EntryData[name] = []
        EntryData["trigNo"] = getattr(clutree, entry_name) 
        Output.CommonEntries.append(EntryData["trigNo"]) 
        EntryData["fCounter"] = [] 
        EntryData["trigtime"] = None 
        for j, VarNames in enumerate(Br_Names_strip): 
            name = VarNames[-1]
            vcog=getattr(clutree, VarNames[0]) # Get vector with centres of gravity of all clusters in this entry. 
            vint=getattr(clutree, VarNames[1]) # Get vector with integrals (energies) of all clusters in this entry. 
            vlen=getattr(clutree, VarNames[2]) # Get vector with lengths of all clusters in this entry. 
            vnfra=getattr(clutree, VarNames[3]) # Get nframe of this entry. 
            EntryData["fCounter"].append(vnfra) 
            if creator=='Daniil - new': 
                vsov=getattr(clutree, VarNames[4])
                for cluster in range(vint.size()): 
                    # Save information about all clusters from this entry to the detector7.AllCoincidences structure.
                    EntryData[name].append([vcog[cluster], vint[cluster], vlen[cluster], vsov[cluster]]) 
            else: 
                for cluster in range(vint.size()): 
                    # Save information about all clusters from this entry to the detector7.AllCoincidences structure.
                    EntryData[name].append([vcog[cluster], vint[cluster], vlen[cluster]])
        for j, VarNames in enumerate(Br_Names_quad): 
            name = VarNames[-1] 
            vx=getattr(clutree, VarNames[0]) 
            vy=getattr(clutree, VarNames[1])                    
            vToA=getattr(clutree, VarNames[2])
            vE=getattr(clutree, VarNames[3])
            trigtime=getattr(clutree, trigtime_name) 
            EntryData["trigtime"]=trigtime 
            if creator=="Jindra": 
                vsize=getattr(clutree, VarNames[4])
                for cluster in range(vx.size()): 
                    # Save information about all clusters from this entry to the detector7.AllCoincidences structure.
                    EntryData[name].append([vx[cluster], vy[cluster], vToA[cluster], vE[cluster], vsize[cluster]])
            else: 
                for cluster in range(vx.size()): 
                    # Save information about all clusters from this entry to the detector7.AllCoincidences structure.
                    EntryData[name].append([vx[cluster], vy[cluster], vToA[cluster], vE[cluster]])

        Output.Data.append(EntryData)

        if entry >= multiple*n: 
            multiple += 1 
            print('|'*(multiple) + ' {} % entries, {} s   '.format(multiple, round(time.time() - start, 2)), end='\r')
    
    print('|'*(100) + ' {} % entries, {} s   '.format(100, round(time.time() - start, 2)))
    return(Output)

def SaveRootCoinFile(path, AllCoin, SaveNames=None): 
    """
    This function saves .ROOT file with coincident clusters data. 
    
    In .ROOT file, there is the same number of entries as there was number of items in AllCoin.Data 
    list. Each entry contains data from all clusters in one coincidence event. 
    The branches in .ROOT file for strip detectors (let the detectors be named ["dets1", ..., "detsN"]) are: 
        "dets#"+"_cog" ... centres of gravity coordinates (vector of floats) 
        "dets#"+"_integral" ... deposited energies of the clusters (vector of floats)
        "dets#"+"_len" ... sizes of the clusters (vector of integers) 
        "dets#"+"_N" ... number of clusters in detector called "dets#" in this entry (integer) 
        "dets#"+"_fCounter" ... frame counter (long integer) 
    The branches in .ROOT file for quad detectors (let the detectors be named ["detq1", ..., "detqN"]) are: 
        "detq#"+"_y" ... x-coordinates of the cluster centres (we reverse the axes, because x-axis of quads 
            corresponds to y-axis of strip detectors) (vector of floats) 
        "detq#"+"_x" ... y-coordinates of the cluster centres (we reverse the axes, because y-axis of quads  
            corresponds to x-axis of strip detectors) (vector of floats) 
        "detq#"+"_ToA" ... first times of arrival of the clusters (in ns) (vector of floats) 
        "detq#"+"_E" ... total energies of the clusters (vector of floats) 
        "detq#"+"_size" ... sizes of the clusters (vector of integers) 
        "detq#"+"_N" ... number of clusters in detector called "detq#" in this entry (integer) 
    The .ROOT file also contains branch: 
        "entry" ... number of this trigger in strip detectors (integer) (Useful if you want to return to 
            original data and find the clusters) 
    If there was at least one quad detector, then the .ROOT file also contains a branch: 
        "trigger_time" ... time of beginning of the trigger (in ns) (double)
    
    Parameters
    ---------- 
    path : string 
        Path to the .ROOT file to be created. 
    AllCoin : detector7.AllCoincidences 
        Object with the coincident clusters data to be saved. 
    SaveNames : dictionary (optional) 
        This dictionary replaces names of the detectors in AllCoin input with new names. Sometimes 
        you want to name the detectors in .ROOT file in a standard way, not with the names you used 
        in Python analysis. Let the old names of all N detectors in AllCoin.Names be 
        ["old1", ..., "oldN"] and the new names to replace them ["new1", ..., "newN"]. Then the 
        dictionary must take form: 
        SaveNames = {
            "old1" : "new1", 
            ..... 
            "oldN" : "newN" 
        }
        All old names need to be present, even if you don't wish to change some of them. In this 
        case you must write dictionary key "oldM":"oldM". There mustn't be two same new names. 
    """
    start = time.time() 
    
    # Check if (some) of the inputs are OK. 
    # -------------------------------------------------------------------------------------------
    if type(path)!=str: 
        raise TypeError("Path to a .ROOT file must be a string! You used {}.".format(type(path))) 
    elif not path.endswith(".root"): 
        raise ValueError("Path must lead to a .ROOT file (i.e. it must finish with '.root' ending).") 
    
    if type(AllCoin)==AllCoincidences: 
        Types = AllCoin.Types  
    else: 
        raise TypeError("""Invalid input at 'AllCoin' type {}. Allowed type is detector7.AllCoincidences.""".format(type(AllCoin))) 
    
    if SaveNames!=None: 
        Checks = []
        if type(SaveNames)==dict: 
            try: 
                NewNames = []
                for name in AllCoin.Names: 
                    Checks.append(type(SaveNames[name])==str) 
                    NewNames.append(SaveNames[name])
                for j, check in enumerate(Checks): 
                    if not check: 
                        raise TypeError("Key 'SaveNames[{}]' must be a string with the new detector name!.".format(AllCoin.Names[j])) 
                for j, name1 in enumerate(NewNames): 
                    for k in range(j+1,len(NewNames)): 
                        if name1==NewNames[k]: 
                            raise ValueError("Two new detector names are equal: '{}'.".format(name1))
            except KeyError: 
                raise KeyError("Detector {} is not mentioned in 'SaveNames' dictionary. All detector names must be present.".format(name))
        else: 
            raise TypeError("Input 'SaveNames' must be either dictionary (if you want to rename detectors) or None (if not).")
    # ------------------------------------------------------------------------------------------- 
    
    ll = len(AllCoin.Data) 
    n = int(math.ceil(ll / 100))
    multiple = 0
    
    #os.remove(path)
    file = ROOT.TFile(path, "recreate") 
    #file.Close()

    Tree = ROOT.TTree('CoincidentClusters', 'CoincidentClusters')
    
    N_strip = Types.count('strip')
    N_quad = Types.count('quad') 

    Names = copy.deepcopy(AllCoin.Names)

    QuadNames = []
    StripNames = []
    for i, det_type in enumerate(Types): 
        if det_type=='quad': 
            QuadNames.append(Names[i]) 
        elif det_type=='strip': 
            StripNames.append(Names[i])
        else: 
            raise ValueError("Unknown detector type {}. Allowed detector types are 'strip' or 'quad'.".format(det_type))

    TrigTime = array.array('d', [0.0]) 
    EntryNumber = array.array('I', [0]) 
        
    try:    
        Quads = [] 
        for i in range(N_quad): 
            DataVecs = [
                ROOT.vector('float') (), # x-coordinate (will be saved as y)
                ROOT.vector('float') (), # y-coordinate (will be saved as x) 
                ROOT.vector('double') (), # times of arrival (in ns) (We need to use double due to precision.)
                ROOT.vector('float') (), # cluster energies 
                array.array('I', [0]) # number of clusters in the trigger 
            ]
            Quads.append(DataVecs) 
            
        Strips = [] 
        for i in range(N_strip): 
            DataVecs = [
                ROOT.vector('float') (), # centre of gravity
                ROOT.vector('float') (), # cluster integral (ADC count, energy)
                ROOT.vector('int') (), # cluster size
                ROOT.vector('float') (), # sovern (signal over noise)
                array.array('Q', [0]), # frame counter of this trigger
                array.array('I', [0]) # number of clusters at this trigger
            ]
            Strips.append(DataVecs)

        Tree.Branch('trigger_time', TrigTime, 'trigger_time/D')
        Tree.Branch('entry', EntryNumber, 'entry/i') 
        
        QuadVars = ['y', 'x', 'ToA', 'E'] 
        StripVars = ['cog', 'integral', 'len', 'sovern'] 
        
        for i, DataVec in enumerate(Quads): 
            name = QuadNames[i] 
            if type(SaveNames)==dict: 
                name = SaveNames[name]
            for j, var in enumerate(QuadVars): 
                Tree.Branch(name+'_'+var, Quads[i][j])
            Tree.Branch(name+'_N', Quads[i][4], 'N/s') 
        
        for i, DataVec in enumerate(Strips): 
            name = StripNames[i] 
            if type(SaveNames)==dict: 
                name = SaveNames[name]
            for j, var in enumerate(StripVars): 
                Tree.Branch(name+'_'+var, Strips[i][j]) 
            Tree.Branch(name+'_fCounter', Strips[i][4], 'fCounter/l')
            Tree.Branch(name+'_N', Strips[i][5], 'N/s')

        for i, coin in enumerate(AllCoin.Data): 
            EntryNumber[0] = coin["trigNo"] 
            TrigTime[0] = coin["trigtime"] 
                
            for j, quadname in enumerate(QuadNames): 
                Quads[j][4][0] = len(coin[quadname]) 
                for row in coin[quadname]: 
                    for m in range(4): 
                        Quads[j][m].push_back(row[m]) 
                        
            for j, stripname in enumerate(StripNames): 
                Strips[j][5][0] = len(coin[stripname])
                if type(coin["fCounter"])==int or type(coin["fCounter"])==float: 
                    Strips[j][4][0] = coin["fCounter"] 
                else: 
                    Strips[j][4][0] = coin["fCounter"][j] 
                for row in coin[stripname]: 
                    for m in range(4): 
                        Strips[j][m].push_back(row[m])       

            Tree.Fill()
            
            for j, QuadVec in enumerate(Quads): 
                for m in range(4): 
                    Quads[j][m].erase(Quads[j][m].begin(), Quads[j][m].end()) 
                    
            for j, StripVec in enumerate(Strips): 
                for m in range(4): 
                    Strips[j][m].erase(Strips[j][m].begin(), Strips[j][m].end())

            if i==multiple*n: 
                multiple += 1 
                print('|'*(multiple) + ' {} %, {} s   '.format(multiple, round(time.time() - start, 2)), end='\r')

        Tree.Write()
    finally: 
        file.Close() 
        print('|'*(100) + ' {} %, {} s   '.format(100, round(time.time() - start, 2)))