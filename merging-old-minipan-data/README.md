# Merging old MiniPAN data 

Old MiniPAN data do not have the trigger synchronization with quad data, they have to be therefore merged manually. This folder contains a Jupyter notebook `MergeStripsWithPixels.ipynb` where you can run a code that merges old MiniPAN data. 

The Jupyter notebook is meant to be run cell by cell. It executes loading of the strip and pixel files, it helps you to find the trigger shift between the files (you have to identify the exact shift number visually from a graph), then the pixel and strip files are merged, you can visually check if the merging was correct, and finally you can save a `ROOT` file with the merged data. 

Some cells require you to input numbers that you will have found from previous cells. Therefore we do not recommend using the Jupyter notebook feature "Run all cells" as your files might be merged and saved with an incorrect trigger shift. Instead run the cells one by one and check the results regularly. 

This folder also contains package `detector7.py` of Python functions that are used in the Jupyter notebook. 

## Test data files

Test data files are given in folders `Quad_Data` and `Strip_Data`. The Jupyter notebook `MergeStripsWithPixels.ipynb` in its default state contains path to these files and if you run it from start to end, it will save the merged file in the folder `Merged_Data`. 
